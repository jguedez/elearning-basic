<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('soNumber');
            $table->string('subject');
            $table->integer('contactId')->nullable()->unsigned();
            $table->string('contactName')->nullable();
            $table->string('status');
            $table->string('owner');
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('subTotal')->nullable();
            $table->string('tax')->nullable();
            $table->string('adjustment')->nullable();
            $table->string('grandTotal')->nullable();
            $table->json('productDetails');
            $table->string('description')->nullable();
            $table->string('discount')->nullable();
            $table->string('currency')->nullable();
            $table->string('exchangeRate')->nullable();
            $table->string('advance')->nullable();
            $table->string('bank')->nullable();
            $table->string('codeAdvance')->nullable();
            $table->string('typeAdvance')->nullable();
            $table->string('dateAdvance')->nullable();
            $table->string('mountAdvance')->nullable();
            $table->string('debtAdvance')->nullable();
            $table->integer('productId')->nullable()->unsigned();
            $table->integer('vendorId')->nullable()->unsigned();
            $table->string('instructor')->nullable();
            $table->string('percentageOfProfit')->nullable();
            $table->string('purchaseOrders')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
