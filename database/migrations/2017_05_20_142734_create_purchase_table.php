<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('subject')->nullable();
            $table->string('vendorId')->nullable();
            $table->string('vendorName')->nullable();
            $table->string('contactId')->nullable();
            $table->string('contactName')->nullable();
            $table->string('poDate')->nullable();
            $table->string('dueDate')->nullable();
            $table->string('carrier')->nullable();
            $table->string('exciseDuty')->nullable();
            $table->string('salesCommission')->nullable();
            $table->string('status')->nullable();
            $table->string('owner')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('subTotal')->nullable();
            $table->string('tax')->nullable();
            $table->string('adjustment')->nullable();
            $table->string('grandTotal')->nullable();
            $table->string('billingStreet')->nullable();
            $table->string('billingCity')->nullable();
            $table->string('billingState')->nullable();
            $table->string('billingCountry')->nullable();
            $table->json('productDetails');
            $table->string('description')->nullable();
            $table->string('discount')->nullable();
            $table->string('currency')->nullable();
            $table->string('exchangeRate')->nullable();
            $table->string('deltaIncome')->nullable();
            $table->string('bank')->nullable();
            $table->string('advance')->nullable();
            $table->string('dateAdvance')->nullable();
            $table->timestamps();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase');
    }
}
