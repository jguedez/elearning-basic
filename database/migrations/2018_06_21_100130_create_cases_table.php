<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('addComment')->nullable();
            $table->string('subject')->nullable();
            $table->string('noOfComments')->nullable();
            $table->string('internalComments')->nullable();
            $table->string('email')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('description')->nullable();
            $table->string('status');
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('reportedBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('currency')->nullable();
            $table->string('caseReason')->nullable();
            $table->string('accountName')->nullable();
            $table->string('dealName')->nullable();
            $table->string('productName')->nullable();
            $table->string('caseNumber')->nullable();
            $table->string('caseOrigin');
            $table->string('priority')->nullable();
            $table->string('owner')->nullable();
            $table->integer('relatedTo')->nullable()->unsigned();
            $table->string('solution')->nullable();
            $table->string('exchangeRate')->nullable();
            $table->string('phone')->nullable();
            $table->string('caseNumberText')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
