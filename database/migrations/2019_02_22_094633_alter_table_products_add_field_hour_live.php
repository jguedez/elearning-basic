<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductsAddFieldHourLive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('live_hours')->nullable();
        });
        
        Schema::table('lives', function (Blueprint $table) {
            $table->string('duration_hours')->nullable();
        });

        Schema::table('registrations', function (Blueprint $table) {
            $table->string('type')->nullable();
            $table->string('live_hours')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('live_hours');
            
        });

        Schema::table('lives', function (Blueprint $table) {
            $table->dropColumn('duration_hours');
        });

        Schema::table('registrations', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('live_hours');
        });
    }
}
