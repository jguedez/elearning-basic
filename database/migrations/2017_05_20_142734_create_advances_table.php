<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('advanceName')->nullable();
            $table->string('name')->nullable();
            $table->string('owner')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->unsignedInteger('lessonCourseId');
            $table->string('lessonCourse')->nullable();
            $table->unsignedInteger('courseId');
            $table->string('course')->nullable();
            $table->unsignedInteger('moduleCourseId');
            $table->string('moduleCourse')->nullable();
            $table->string('secondId');
            $table->unsignedInteger('contactId');
            $table->string('contact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advances');
    }
}
