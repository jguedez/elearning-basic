<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('moduleName');
            $table->string('moduleOwner');
            $table->string('order')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('lastActivity')->nullable();
            $table->unsignedInteger('parentCourseId');
            $table->string('parentCourse')->nullable();
            $table->text('description')->nullable();
            $table->string('currency')->nullable();
            $table->string('exchangeRate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
