<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesAddFieldsProfileUsersAndProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {

            $table->date('birthday')->nullable();
            $table->json('experience')->nullable();
            $table->json('education')->nullable();

        });

        Schema::table('contacts', function (Blueprint $table) {
            
            $table->date('birthday')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->dropColumn('birthday');
            $table->dropColumn('experience');
            $table->dropColumn('education');
        });

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('birthday');
        });
    }
}
