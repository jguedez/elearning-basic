<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('vendorName');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('glAccount')->nullable();
            $table->string('category')->nullable();
            $table->string('vendorOwner')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('street')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zipCode')->nullable();
            $table->string('country')->nullable();
            $table->string('description')->nullable();
            $table->string('type')->nullable();
            $table->string('biography')->nullable();
            $table->string('nameInstructor')->nullable();
            $table->text('littleDescription')->nullable();
            $table->string('interest')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->text('socialOne')->nullable();
            $table->text('socialTwo')->nullable();
            $table->text('socialThree')->nullable();
            $table->text('socialFour')->nullable();
            $table->text('socialFive')->nullable();
            $table->text('socialSix')->nullable();
            $table->text('vendorPicture')->nullable();
            $table->string('paypal')->nullable();
            $table->text('bank')->nullable();
            $table->text('advanceI')->nullable();
            $table->text('dateAdvanceI')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
