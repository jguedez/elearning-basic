<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('productOwner');
            $table->string('productName');
            $table->string('productCode')->nullable();
            $table->string('order')->nullable();
            $table->string('statusApproved')->nullable();
            $table->string('courseProgress')->nullable();
            $table->unsignedInteger('vendorId');
            $table->string('vendorName')->nullable();
            $table->string('productActive')->nullable();
            $table->string('productCategory')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('unitPrice')->nullable();
            $table->string('commissionRate')->nullable();
            $table->string('usageUnit')->nullable();
            $table->string('qtyOrdered')->nullable();
            $table->string('qtyInStock')->nullable();
            $table->string('reorderLevel')->nullable();
            $table->string('qtyInDemand')->nullable();
            $table->text('description')->nullable();
            $table->string('taxable')->nullable();
            $table->text('previous')->nullable();
            $table->string('link')->nullable();
            $table->string('image')->nullable();
            $table->string('level')->nullable();
            $table->string('video')->nullable();
            $table->string('subCategory')->nullable();
            $table->string('category')->nullable();
            $table->text('audience')->nullable();
            $table->string('subTitle')->nullable();
            $table->string('duration')->nullable();
            $table->text('location')->nullable();
            $table->string('language')->nullable();
            $table->text('document')->nullable();
            $table->string('startCourse')->nullable();
            $table->text('descriptionCourse')->nullable();
            $table->text('objectivesCourse')->nullable();
            $table->text('littleDescription')->nullable();
            $table->string('instructorEmail')->nullable();
            $table->string('instructorName')->nullable();
            $table->string('instructorWeb')->nullable();
            $table->text('biographyInstructor')->nullable();
            $table->string('phoneInstructor')->nullable();
            $table->string('socialOne')->nullable();
            $table->string('socialTwo')->nullable();
            $table->string('socialThree')->nullable();
            $table->string('socialFour')->nullable();
            $table->string('photoInstructor')->nullable();
            $table->text('education')->nullable();
            $table->string('distribution')->nullable();
            $table->string('deltaIncome')->nullable();
            $table->text('cover')->nullable();
            $table->text('linkVideo')->nullable();
            $table->string('signedUp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
