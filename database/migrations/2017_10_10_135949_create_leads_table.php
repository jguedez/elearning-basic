<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('leadOwner')->nullable();
            $table->string('name')->nullable();
            $table->string('lastName')->nullable();
            $table->string('email')->nullable();
            $table->string('leadSource')->nullable();
            $table->string('industry')->nullable();
            $table->string('noOfEmployees')->nullable();
            $table->string('annualRevenue')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('fullName')->nullable();
            $table->text('description')->nullable();
            $table->text('street')->nullable();
            $table->string('country')->nullable();
            $table->string('lastActivity')->nullable();
            $table->string('key')->nullable();
            $table->string('Type')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
