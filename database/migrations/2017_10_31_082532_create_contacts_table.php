<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('contactOwner')->nullable();
            $table->string('leadSource')->nullable();
            $table->string('name')->nullable();
            $table->string('lastName')->nullable();
            $table->string('accountId')->nullable();
            $table->string('accountName')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('fullName')->nullable();
            $table->string('mailingCountry')->nullable();
            $table->string('emailOptOut')->nullable();
            $table->string('lastActivity')->nullable();
            $table->string('tag')->nullable();
            $table->string('gender')->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->string('type')->nullable();
            $table->integer('vendorId')->nullable()->unsigned();
            $table->string('vendorName')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
