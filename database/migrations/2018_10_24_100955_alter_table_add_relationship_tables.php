<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAddRelationshipTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('users', function (Blueprint $table) {

            $table->foreign('vendorId')
            ->references('id')->on('vendors')
            ->onDelete('cascade');
            
            $table->foreign('contactId')
            ->references('id')->on('contacts')
            ->onDelete('cascade');
        });

        Schema::table('contacts', function (Blueprint $table) {

            $table->foreign('vendorId')
            ->references('id')->on('vendors')
            ->onDelete('cascade');
        });

        Schema::table('products', function (Blueprint $table) {

            $table->foreign('vendorId')
            ->references('id')->on('vendors')
            ->onDelete('cascade');
        });

        Schema::table('registrations', function (Blueprint $table) {
            $table->foreign('userId')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('parentCourseId')
            ->references('id')->on('products')
            ->onDelete('cascade');
        });

        Schema::table('modules', function (Blueprint $table) {

            $table->foreign('parentCourseId')
            ->references('id')->on('products')
            ->onDelete('cascade');
        });

        Schema::table('lessons', function (Blueprint $table) {

            $table->foreign('parentCourseId')
            ->references('id')->on('products')
            ->onDelete('cascade');

            $table->foreign('parentModuleId')
            ->references('id')->on('modules')
            ->onDelete('cascade');
        });

        Schema::table('advances', function (Blueprint $table) {

            $table->foreign('lessonCourseId')
            ->references('id')->on('lessons')
            ->onDelete('cascade');

            $table->foreign('moduleCourseId')
            ->references('id')->on('modules')
            ->onDelete('cascade');

            $table->foreign('courseId')
            ->references('id')->on('products')
            ->onDelete('cascade');

            $table->foreign('contactId')
            ->references('id')->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign(['vendorId']);
            $table->dropForeign(['contactId']);
        });

        Schema::table('contacts', function (Blueprint $table) {

            $table->dropForeign(['vendorId']);
        });

        Schema::table('products', function (Blueprint $table) {

            $table->dropForeign(['vendorId']);

        });

        Schema::table('registrations', function (Blueprint $table) {

            $table->dropForeign(['userId']);
            $table->dropForeign(['parentCourseId']);
    
        });

        Schema::table('modules', function (Blueprint $table) {

            $table->dropForeign(['parentCourseId']);


        Schema::table('lessons', function (Blueprint $table) {

            $table->dropForeign(['parentCourseId']);
            $table->dropForeign(['parentModuleId']);

        });

        Schema::table('advances', function (Blueprint $table) {

            $table->dropForeign(['lessonCourseId']);
            $table->dropForeign(['moduleCourseId']);
            $table->dropForeign(['courseId']);
            $table->dropForeign(['contactId']);

            });
        });
    }
}
