<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRegistrationsFieldsSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registrations', function (Blueprint $table) {
            $table->timestamp('start_at')->nullable();
            $table->timestamp('ends_at')->nullable();
            $table->integer('duration_lic')->nullable();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('duration_lic')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registrations', function (Blueprint $table) {
            $table->dropColumn('start_at');
            $table->dropColumn('ends_at');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('duration_lic');
        });
    }
}
