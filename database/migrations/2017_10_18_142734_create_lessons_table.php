<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('duration')->nullable();
            $table->string('resourceType')->nullable();
            $table->string('lessonName');
            $table->string('lessonOwner');
            $table->string('order')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('lastActivity')->nullable();
            $table->string('resource')->nullable();
            $table->unsignedInteger('parentModuleId');
            $table->string('parentModule')->nullable();
            $table->unsignedInteger('parentCourseId');
            $table->string('parentCourse')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
