<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFilesAlterTableLessonAndProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('file_id');
            $table->string('photo_resource');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->string('file_id')->nullable();
            $table->string('photo_cover')->nullable();
        });

        Schema::table('lessons', function (Blueprint $table) {
            $table->string('file_id')->nullable();
            $table->string('type_resource')->nullable();
            $table->string('path_resource')->nullable();
        });

        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_id')->nullable();
            $table->string('contact_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('lesson_id')->nullable();
            $table->string('folder_name')->nullable();
            $table->string('type_resource')->nullable();
            $table->string('path_resource')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');

        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('file_id');
            $table->dropColumn('photo_resource');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('file_id');
            $table->dropColumn('photo_cover');
        });

        Schema::table('lessons', function (Blueprint $table) {
            $table->dropColumn('file_id');
            $table->dropColumn('type_resource');
            $table->dropColumn('path_resource');

        });
    }
}
