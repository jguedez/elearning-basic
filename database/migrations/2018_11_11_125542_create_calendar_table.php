<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('crmId')->nullable();
            $table->unsignedInteger('contactId')->nullable();
            $table->string('crmContactId')->nullable();
            $table->unsignedInteger('vendorId')->nullable();
            $table->string('crmVendorId')->nullable();
            $table->string('startDate')->nullable();
            $table->string('endDate')->nullable();
            $table->string('rule')->nullable();
            $table->string('frequency')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });

        Schema::table('calendar', function (Blueprint $table) {
            $table->foreign('contactId')
            ->references('id')->on('contacts')
            ->onDelete('cascade');
    
            $table->foreign('vendorId')
            ->references('id')->on('vendors')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       /* Schema::table('calendar', function (Blueprint $table) {
            $table->dropForeign(['contactId','vendorId']);
        });*/

        Schema::dropIfExists('calendar');

        
    }
}
