<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wfTrigger')->default('true');
            $table->string('date')->nullable();
            $table->string('inscriptionName');
            $table->string('owner')->nullable();
            $table->string('createdBy')->nullable();
            $table->string('modifiedBy')->nullable();
            $table->string('createdTime')->nullable();
            $table->string('modifiedTime')->nullable();
            $table->string('lastActivity')->nullable();
            $table->string('currency')->nullable();
            $table->string('exchangeRate')->nullable();
            $table->unsignedInteger('parentCourseId');
            $table->string('parentCourse')->nullable();
            $table->unsignedInteger('userId');
            $table->string('user')->nullable();
            $table->string('chargeClientId')->nullable();
            $table->string('payVendorId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
