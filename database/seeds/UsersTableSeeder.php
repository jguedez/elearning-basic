<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // check if table users is empty
        if(\DB::table('users')->get()->count() == 0)
        {
            \DB::table('users')->insert([

                [
                    'id'         => 1,
                    'name'       => 'Donald Trump',
                    'lastName'   => 'Donald Trump',
                    'email'      => 'donald@kineticssolutions.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001313032',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 2,
                    'name'       => 'test1',
                    'lastName'   => 'test1',
                    'email'      => 'test1@thebiznation.com',
                    'contactId'  => '2635099000001352051',
                    'vendorId'   => null,
                    'level'      => 'Alumno',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 3,
                    'name'       => 'test2',
                    'lastName'   => 'test2',
                    'email'      => 'test2@thebiznation.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001352023',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 4,
                    'name'       => 'test3',
                    'lastName'   => 'test3',
                    'email'      => 'test3@thebiznation.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001352037',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 5,
                    'name'       => 'test4',
                    'lastName'   => 'test4',
                    'email'      => 'test42@thebiznation.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001352044',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 6,
                    'name'       => 'Leydy',
                    'lastName'   => 'Coromoto',
                    'email'      => 'lmoreno@negocioselectronicos.biz',
                    'contactId'  => '2635099000001495001',
                    'vendorId'   => null,
                    'level'      => 'Alumno',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 7,
                    'name'       => 'JF',
                    'lastName'   => 'Testing',
                    'email'      => 'testingjff@gmail.com',
                    'contactId'  => null,
                    'vendorId'   => null,
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 8,
                    'name'       => 'Kinetics',
                    'lastName'   => 'Solutions',
                    'email'      => 'info@kineticssolutions.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000000580389',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 9,
                    'name'       => 'Usuario De Prueba 2',
                    'lastName'   => 'Testing',
                    'email'      => 'test255@thebiznation.com',
                    'contactId'  =>  null,
                    'vendorId'   => '2635099000001352023',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 10,
                    'name'       => 'Usuario De Prueba 3',
                    'lastName'   => 'Testing',
                    'email'      => 'test3@negocioselectronicos.biz',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001352037',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 11,
                    'name'       => 'Usuario de Prueba 4',
                    'lastName'   => 'Testing',
                    'email'      => 'test4@thebiznation.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001352044',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 12,
                    'name'       => 'Diego',
                    'lastName'   => 'Artiles',
                    'email'      => 'dartiles@thebiznation.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001434011',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 13,
                    'name'       => 'The Biz Nation',
                    'lastName'   => 'Testing',
                    'email'      => 'admin@thebiznation.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001445086',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 14,
                    'name'       => 'Javier',
                    'lastName'   => 'El Instructor',
                    'email'      => 'javierelinstructor@negocioselectronicos.biz',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001495056',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 15,
                    'name'       => 'Instructor Demo 4',
                    'lastName'   => 'Testing',
                    'email'      => 'intructor4@negocioselectronicos.biz',
                    'contactId'  => '2635099000001499176',
                    'vendorId'   => null,
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],[

                    'id'         => 16,
                    'name'       => 'Intructor Demo 5',
                    'lastName'   => 'Testing',
                    'email'      => 'intructor5@negocioselectronicos.biz',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001526025',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 17,
                    'name'       => 'Juan David ',
                    'lastName'   => 'Martinez Mogollon',
                    'email'      => 'juandavidmartinezmogollon@gmail.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001526216',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],[

                    'id'         => 18,
                    'name'       => 'Instructor Test 7',
                    'lastName'   => 'Testing',
                    'email'      => 'intructor7@negocioselectronicos.biz',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001527076',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [

                    'id'         => 19,
                    'name'       => 'JF',
                    'lastName'   => 'Testing',
                    'email'      => 'testingjf@gmail.com',
                    'contactId'  => '2635099000001499176',
                    'vendorId'   => null,
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 20,
                    'name'       => 'lina',
                    'lastName'   => 'mill',
                    'email'      => 'instructores@thebiznation.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001528216',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 21,
                    'name'       => 'Gina',
                    'lastName'   => 'Perez',
                    'email'      => 'administradorcis@cartagenainternationalschool.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001528313',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 22,
                    'name'       => 'Luis',
                    'lastName'   => 'Berrio',
                    'email'      => 'luisberrio08@hotmail.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001529353',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 23,
                    'name'       => 'Gina',
                    'lastName'   => 'Perez',
                    'email'      => 'ginaperezmendoz@outlook.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001530268',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 24,
                    'name'       => 'Instructor Test 6',
                    'lastName'   => 'Testing',
                    'email'      => 'intructor6@negocioselectronicos.biz',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001528216',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 25,
                    'name'       => 'Instructor  Test 8',
                    'lastName'   => 'Testing',
                    'email'      => 'instructor8@negocioselecteonicos.biz',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001532125',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 26,
                    'name'       => 'Instructor Test 10',
                    'lastName'   => 'Testing',
                    'email'      => 'instructor10@negocioselectronicos.biz',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001532261',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 27,
                    'name'       => 'Carlos',
                    'lastName'   => 'Velez',
                    'email'      => 'carlosvelezromero@gmail.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001532364',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 28,
                    'name'       => 'irina',
                    'lastName'   => 'hernandez',
                    'email'      => 'wonderaniri@hotmail.com',
                    'contactId'  => null,
                    'vendorId'   => '2635099000001532589',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],
                [
                    'id'         => 29,
                    'name'       => 'Alumno',
                    'lastName'   => 'Instructor',
                    'email'      => 'alumnoainstructor2@negocioselectronicos.biz',
                    'contactId'  => '2635099000001579017',
                    'vendorId'   => '2635099000001577011',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('Ks123456789@')
                ],
                [
                    'id'         => 30,
                    'name'       => 'Leydy',
                    'lastName'   => 'Moreno',
                    'email'      => 'leydykm93@gmail.com',
                    'contactId'  => '2635099000001686021',
                    'vendorId'   => '2635099000001689010',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('Ks123456789@')
                ],
                [
                    'id'         => 31,
                    'name'       => 'Cafeteria',
                    'lastName'   => 'Cis',
                    'email'      => 'cafeteriacis@gmail.com',
                    'contactId'  => '2635099000001766028',
                    'vendorId'   => '2635099000001770022',
                    'level'      => 'Instructor',
                    'password'   => bcrypt('123456789')
                ],

            ]);

        }
        
    }

}