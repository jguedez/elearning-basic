<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reset Password</title>
</head>
<body>
    <p>Hello!, {{ $user->name }},</p>
    <p>Access the following link to recovery your <a href="{{ url('forget-password-token/{{ $user->tokenPassword }}')}}">
        Password</a>
    </a></p>

</body>