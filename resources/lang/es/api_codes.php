<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API codes Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during api transactions for matching the
    | the API codes with the specify message
    |
    */

    // 2xx: Peticiones correctas:

    200 => 'Operación completada',
    201 => 'El objecto se ha creado satisfactoriamente',
    204 => 'Sin contenido',

    // 4xx: Errores del cliente

    400 => 'Parámetro obligatorio omitido o con valor incorrecto',
    401 => 'Autenticación no permitida, credenciales invalidas',
    403 => 'Usted no posee los permisos necesario para esta funcion',
    404 => 'Recurso no encontrado',
    405 => 'HTTP exception, method not allowed',
    409 => 'La solicitud no pudo ser procesada',

    // 5xx Errores de servidor
    500 => 'Error interno del servidor',
];