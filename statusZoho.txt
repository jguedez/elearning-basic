
    $array   = [];
    $zoho    = new \CristianPontes\ZohoCRMClient\ZohoCRMClient('Products','fe4d0ce38c31c7d47eee02e4b9731b20');
    $records = $zoho->insertRecords()
        ->setRecords([
            [
                "Product Owner" => "JF",
                "Product Name" => "CrearAPi",
                "Product Code" => "courses-10",
                "Product Active" => "true",
                "Product Category" => "Cursos Online",
                "Created By" => "The Biz Nation",
                "MODIFIEDBY" => "2635099000000121009",
                "Modified By" => "The Biz Nation",
                "Created Time" => "2017-10-23 09:47:52",
                "Modified Time" => "2017-10-23 11:25:31",
                "Unit Price" => "100",
                "Commission Rate" => "0",
                "Usage Unit" => "Caja",
                "Qty Ordered" => "0",
                "Qty in Stock" => "0",
                "Reorder Level" => "0",
                "Qty in Demand" => "0",
                "Taxable" => "true",
                "¿Aprobado?" => "true",
                "¿Aprobado para publicar?" => "false"
            ]
        ])
        ->onDuplicateError()
        ->triggerWorkflow()
        ->request();

    foreach ($records as $record)
    {
       $array[] = $record;
    }











    Route::get('fromZoho', function()
    {
        $array   = [];
        $zoho    = new \CristianPontes\ZohoCRMClient\ZohoCRMClient('Contacts','fe4d0ce38c31c7d47eee02e4b9731b20');
        $records = $zoho->insertRecords()
            ->setRecords([
                [
                      "CONTACTID" => "",
                      "Contact Owner" => "The Biz Nation",
                      "Lead Source" => "",
                      "First Name" => "JF",
                      "Last Name" => "Cordova",
                      "ACCOUNTID" => "",
                      "Account Name" => "",
                      "email" => "ll@gmail.com",
                      "Created By" => "",
                      "Modified By" => "",
                      "Created Time" => "",
                      "Modified Time" => "",
                      "Full Name" => "",
                      "Mailing Country" => "",
                      "Email Opt Out" => "",
                      "Last Activity Time" => "",
                      "Tag" => ""
                ]
            ])
            ->onDuplicateError()
            ->triggerWorkflow()
            ->request();

        foreach ($records as $record)
        {
            $array[] = $record;
        }

        return response()->json(['response' => $array]);

    });
