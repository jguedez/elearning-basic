<?php

namespace App\Policies;

use App\Entities\User;
use App\Helpers\PermissionHelper;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\Entities\User  $user
     * @param  bool|\App\Entities\User $record
     * @return mixed
     */
    public function view($level)
    {
        $permission = $this->HasPermissionTo($level);
        
        return $permission['contacts']['index'];
    }

    /**
     * Determine whether the user can show the user.
     *
     * @param  \App\Entities\User  $user
     * @param  bool|\App\Entities\User $record
     * @return mixed
     */
    public function show($level)
    {
        $permission = $this->HasPermissionTo($level);
        
        return $permission['contacts']['show'];
    }
    
    /**
     * Determine whether the user can create users.
     *
     * @param  \App\Entities\User  $user
     * @return mixed
     */
    public function store($level)
    {
        $permission = $this->HasPermissionTo($level);
        
        return $permission['contacts']['store'];
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\User  $record
     * @return mixed
     */
    public function update($level)
    {
        $permission = $this->HasPermissionTo($level);
        
        return $permission['contacts']['update'];
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\User  $record
     * @return mixed
     */
    public function delete($level)
    {
        $permission = $this->HasPermissionTo($level);
        
        return $permission['contacts']['destroy'];
    }

    public function HasPermissionTo($level)
    {
        if ($level == 'Administrador')
        {   
            $permission = PermissionHelper::Administrador();
            
            return $permission;

        }elseif ($level == 'Instructor')
        {   
            $permission = PermissionHelper::Instructor();
            
            return $permission;

        }elseif ($level == 'Alumno')
        {   
            $permission = PermissionHelper::Alumno();
            
            return $permission;
        }else
        {
            return false;
        }
    }
}
