<?php

namespace App\Services;

use App\Helpers\Curl;
/**
 * Class Moodle
 */
class Moodle
{

    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var
     */
    private $token;
    /**
     * @var string
     */
    private $endPoint = 'https://moodle.thebiznation.com/webservice/rest/server.php';

    /**
     * Moodle constructor.
     * @param $token
     */
    public function __construct($token)
    {
        $this->curl = new Curl();
        $this->token = $token;
    }

    /**
     * @param $user_id
     * @param bool $course_id
     * @return array|mixed
     */
    public function getCourses($user_id, $course_id = false)
    {
        $params = $course_id ? ['field' => 'id', 'value' => $course_id] : [];
        $courses = $this->call('core_course_get_courses_by_field', $params);
        $userCourses = $this->call('core_enrol_get_users_courses', ['userid' => $user_id]);
        $response = [];

        $userCourses = empty($userCourses) ? [] : array_column($userCourses, 'id');

        foreach ($courses['courses'] as $course) {
            if ($course['visible'] != 1 || $course['id'] == 1) {
                continue;
            }
            $course['price'] = null;
            $course['picture_url'] = null;
            $course['enrolled'] = in_array($course['id'], $userCourses);
            $response[] = $course;
        }
        return $course_id ? array_shift($response) : $response;
    }

    /**
     * @param $user_id
     * @param $course_id
     * @return array
     */
    public function getCourseModules($user_id, $course_id)
    {
        $course = $this->getCourses($user_id, $course_id);
        //$cache      = json_decode(file_get_contents($this->cacheFolder . $course_id . '.json'), true);

        return ['course' => $course];
    }

    /**
     * @param $method
     * @param $params
     * @return array
     */
    private function call($method, $params = [])
    {
        return json_decode($this->curl->post($this->endPoint, array_merge($params, [
            'moodlewsrestformat' => 'json',
            'wstoken' => $this->token,
            'wsfunction' => $method,
        ])), true);
    }

}