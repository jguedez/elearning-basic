<?php

namespace App\Transformers;


class LessonsTransformer extends Transformer
{
    /**
     * @param $lessons
     * @return array
     */
    public function schema($lessons)
    {
        return [

            'id'              =>  $lessons['id'],
            'duration'        =>  $lessons['duration'],
            'resourceType'    =>  $lessons['resourceType'],
            'lessonName'      =>  $lessons['lessonName'],
            'lessonOwner'     =>  $lessons['lessonOwner'],
            'order'           =>  $lessons['order'],
            'createdBy'       =>  $lessons['createdBy'],
            'modifiedBy'      =>  $lessons['modifiedBy'],
            'createdTime'     =>  $lessons['createdTime'],
            'modifiedTime'    =>  $lessons['modifiedTime'],
            'lastActivity'    =>  $lessons['lastActivity'],
            'parentModuleId'  =>  $lessons['parentModuleId'],
            'parentModule'    =>  $lessons['parentModule'],
            'parentCourseId'  =>  $lessons['parentCourseId'],
            'parentCourse'    =>  $lessons['parentCourse'],
            'file_id'         =>  $lessons['file_id']
        ];
    }
}