<?php

namespace App\Transformers;

abstract class Transformer
{
    /**
     * All child classes should declare the schema for their resources
     * @param $item
     * @return array
     */
    public abstract function schema($item);


    /**
     * Single transforming using the resource schema
     * @param $item
     * @return array
     */
    public function transform($item)
    {
        $item = is_array($item) ? $item : $item->toArray();
        return $this->schema($item);
    }

    /**
     * Wrapper for the single transform method, used for batch data
     * @param $collection
     * @return array
     */
    public function transformCollection($collection)
    {
        if(isset($collection['transforming_disable'])){
            return $collection['data'];
        }

        return [
            'paginator' => [
                'total'         => $collection->total(),
                'per_page'      => $collection->perPage(),
                'current_page'  => $collection->currentPage(),
                'last_page'     => $collection->lastPage(),
                'next_page_url' => $collection->nextPageUrl(),
                'prev_page_url' => $collection->previousPageUrl(),
            ],
            'data'      => array_map([$this, 'transform'], $collection->all())
        ];
    }
}