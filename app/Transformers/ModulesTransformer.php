<?php

namespace App\Transformers;


class ModulesTransformer extends Transformer
{
    /**
     * @param $module
     * @return array
     */
    public function schema($module)
    {
        return [

            'id'                =>  $module['id'],
            'moduleName'        =>  $module['moduleName'],
            'moduleOwner'       =>  $module['moduleOwner'],
            'order'             =>  $module['order'],
            'createdBy'         =>  $module['createdBy'],
            'modifiedBy'        =>  $module['modifiedBy'],
            'createdTime'       =>  $module['createdTime'],
            'modifiedTime'      =>  $module['modifiedTime'],
            'lastActivity'      =>  $module['lastActivity'],
            'parentCourseId'    =>  $module['parentCourseId'],
            'parentCourse'      =>  $module['parentCourse'],

        ];
    }
}