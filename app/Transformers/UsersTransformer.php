<?php

namespace App\Transformers;


class UsersTransformer extends Transformer
{
    /**
     * @param $user
     * @return array
     */
    public function schema($user)
    {
        return [

            'id'          => $user['id'],
            'name'        => $user['name'],
            'lastName'    => $user['lastName'],
            'email'       => $user['email'],
            'level'       => $user['level'],
            'vendorId'    => $user['vendorId'],
            'contactId'   => $user['contactId'],
            'status'      => $user['status']
        ];
    }
}