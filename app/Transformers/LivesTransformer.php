<?php

namespace App\Transformers;


class LivesTransformer extends Transformer
{
    /**
     * @param $lessons
     * @return array
     */
    public function schema($lives)
    {
        return [
            'id'          =>  $lives['id'],
            'name'        =>  $lives['name'],
            'start_at'    =>  $lives['start_at'],
            'ends_at'     =>  $lives['ends_at'],
            'draggble'   =>   $lives['draggble'],
            'resizable'   =>  $lives['resizable'],
            'product_id'  =>  $lives['product_id'],
            'lesson_id'   =>  $lives['lesson_id'],
            'user_id'     =>  $lives['user_id'],
        ];
    }
}