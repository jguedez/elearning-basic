<?php

namespace App\Transformers;


class SalesOrdersTransformer extends Transformer
{
    /**
     * @param $sales
     * @return array
     */
    public function schema($sales)
    {
        return [

            'id'                => $sales['id'],
            'soNumber'          => $sales['soNumber'],
            'subject'           => $sales['subject'],
            'contactId'         => $sales['contactId'],
            'contactName'       => $sales['contactName'],
            'status'            => $sales['status'],
            'owner'             => $sales['owner'],
            'createdBy'         => $sales['createdBy'],
            'modifiedBy'        => $sales['modifiedBy'],
            'createdTime'       => $sales['createdTime'],
            'modifiedTime'      => $sales['modifiedTime'],
            'subTotal'          => $sales['subTotal'],
            'tax'               => $sales['tax'],
            'adjustment'        => $sales['adjustment'],
            'grandTotal'        => $sales['grandTotal'],
            'productDetailed'   => $sales['productDetailed'],
            'description'       => $sales['description'],
            'discount'          => $sales['discount'],
            'currency'          => $sales['currency'],
            'exchangeRate'      => $sales['exchangeRate'],
            'advance'           => $sales['advance'],
            'bank'              => $sales['bank'],
            'codeAdvance'       => $sales['codeAdvance'],
            'typeAdvance'       => $sales['typeAdvance'],
            'dateAdvance'       => $sales['dateAdvance'],
            'mountAdvance'      => $sales['mountAdvance'],
            'debtAdvance'       => $sales['debtAdvance'],
            'productId'         => $sales['productId'],
            'vendorId'          => $sales['vendorId'],
            'instructor'        => $sales['instructor'],
            'purchaseOrders'    => $sales['purchaseOrders'],
        ];
    }
}