<?php

namespace App\Transformers;


class CaseTransformer extends Transformer
{
    /**
     * @cases
     * @return array
     */
    public function schema($cases)
    {
        return [

            'id'                =>  $cases['id'],
            'addComment'        =>  $cases['addComment'],
            'subject'           =>  $cases['subject'],
            'noOfComments'      =>  $cases['noOfComments'],
            'internalComments'  =>  $cases['internalComments'],
            'email'             =>  $cases['email'],
            'createdBy'         =>  $cases['createdBy'],
            'description'       =>  $cases['description'],
            'status'            =>  $cases['status'],
            'createdTime'       =>  $cases['createdTime'],
            'modifiedTime'      =>  $cases['modifiedTime'],
            'reportedBy'        =>  $cases['reportedBy'],
            'modifiedBy'        =>  $cases['modifiedBy'],
            'currency'          =>  $cases['currency'],
            'caseReason'        =>  $cases['caseReason'],
            'accountName'       =>  $cases['accountName'],
            'dealName'          =>  $cases['dealName'],
            'productName'       =>  $cases['productName'],
            'caseNumber'        =>  $cases['caseNumber'],
            'caseOrigin'        =>  $cases['caseOrigin'],
            'priority'          =>  $cases['priority'],
            'owner'             =>  $cases['owner'],
            'relatedTo'         =>  $cases['relatedTo'],
            'solution'          =>  $cases['solution'],
            'exchangeRate'      =>  $cases['exchangeRate'],
            'phone'             =>  $cases['phone'],
            'caseNumberText'    =>  $cases['caseNumberText'],
            'type'              =>  $cases['type'],
        ];
    }
}