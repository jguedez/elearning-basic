<?php

namespace App\Transformers;


class AdvancesTransformer extends Transformer
{

    /**
     * @param $advance
     * @return array
     */
    public function schema($advance)
    {
        return [

            'id'                => $advance['id'],
            'advanceName'       => $advance['advanceName'],
            'name'              => $advance['name'],
            'owner'             => $advance['owner'],
            'createdBy'         => $advance['createdBy'],
            'modifiedBy'        => $advance['modifiedBy'],
            'createdTime'       => $advance['createdTime'],
            'modifiedTime'      => $advance['modifiedTime'],
            'lessonCourseId'    => $advance['lessonCourseId'],
            'lessonCourse'      => $advance['lessonCourse'],
            'courseId'          => $advance['courseId'],
            'course'            => $advance['course'],
            'moduleCourseId'    => $advance['moduleCourseId'],
            'moduleCourse'      => $advance['moduleCourse'],
            'secondId'          => $advance['secondId'],
            'contactId'         => $advance['contactId'],
            'contact'           => $advance['contact'],

        ];

    }
}