<?php

namespace App\Transformers;


class ContactTransformer extends Transformer
{
    /**
     * @param $contact
     * @return array
     */
    public function schema($contact)
    {
        return [

            'id'              => $contact['id'],
            'contactOwner'    => $contact['contactOwner'],
            'leadSource'      => $contact['leadSource'],
            'name'            => $contact['name'],
            'lastName'        => $contact['lastName'],
            'accountId'       => $contact['accountId'],
            'accountName'     => $contact['accountName'],
            'email'           => $contact['email'],
            'phone'           => $contact['phone'],
            'createdBy'       => $contact['createdBy'],
            'modifiedBy'      => $contact['modifiedBy'],
            'createdTime'     => $contact['createdTime'],
            'modifiedTime'    => $contact['modifiedTime'],
            'fullName'        => $contact['fullName'],
            'mailingCountry'  => $contact['mailingCountry'],
            'emailOptOut'     => $contact['emailOptOut'],
            'lastActivity'    => $contact['lastActivity'],
            'tag'             => $contact['tag'],
            'gender'          => $contact['gender'],
            'dateOfBirth'     => $contact['dateOfBirth'],
            'birthday'        => $contact['birthday']
        ];
    }
}