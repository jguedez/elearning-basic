<?php

namespace App\Transformers;


class ProductsTransformer extends Transformer
{
    /**
     * @param $product
     * @return array
     */
    public function schema($product)
    {
        return [

            'id'                    =>  $product['id'],
            'productOwner'          =>  $product['productOwner'],
            'productName'           =>  $product['productName'],
            'productCode'           =>  $product['productCode'],
            'order'                 =>  $product['order'],
            'statusApproved'        =>  $product['statusApproved'],
            'courseProgress'        =>  $product['courseProgress'],
            'vendorId'              =>  $product['vendorId'],
            'vendorName'            =>  $product['vendorName'],
            'productActive'         =>  $product['productActive'],
            'productCategory'       =>  $product['productCategory'],
            'createdBy'             =>  $product['createdBy'],
            'modifiedBy'            =>  $product['modifiedBy'],
            'createdTime'           =>  $product['createdTime'],
            'modifiedTime'          =>  $product['modifiedTime'],
            'unitPrice'             =>  $product['unitPrice'],
            'commissionRate'        =>  $product['commissionRate'],
            'usageUnit'             =>  $product['usageUnit'],
            'qtyOrdered'            =>  $product['qtyOrdered'],
            'qtyInStock'            =>  $product['qtyInStock'],
            'reorderLevel'          =>  $product['reorderLevel'],
            'qtyInDemand'           =>  $product['qtyInDemand'],
            'description'           =>  $product['description'],
            'taxable'               =>  $product['taxable'],
            'link'                  =>  $product['link'],
            'image'                 =>  $product['image'],
            'level'                 =>  $product['level'],
            'video'                 =>  $product['video'],
            'subCategory'           =>  $product['subCategory'],
            'category'              =>  $product['category'],
            'audience'              =>  $product['audience'],
            'subTitle'              =>  $product['subTitle'],
            'duration'              =>  $product['duration'],
            'location'              =>  $product['location'],
            'language'              =>  $product['language'],
            'document'              =>  $product['document'],
            'startCourse'           =>  $product['startCourse'],
            'descriptionCourse'     =>  $product['descriptionCourse'],
            'littleDescription'     =>  $product['littleDescription'],
            'instructorEmail'       =>  $product['instructorEmail'],
            'instructorName'        =>  $product['instructorName'],
            'instructorWeb'         =>  $product['instructorWeb'],
            'biographyInstructor'   =>  $product['instructorEmail'],
            'phoneInstructor'       =>  $product['phoneInstructor'],
            'socialOne'             =>  $product['socialOne'],
            'socialTwo'             =>  $product['socialTwo'],
            'socialThree'           =>  $product['socialThree'],
            'socialFour'            =>  $product['socialFour'],
            'photoInstructor'       =>  $product['photoInstructor'],
            'status'                =>  $product['status'] ? $product['status'] : null,
            'deltaIncome'           =>  $product['deltaIncome'],
            'cover'                 =>  $product['cover'],
            'linkVideo'             =>  $product['linkVideo'],
            'signedUp'              =>  $product['signedUp'],
            'type'                  =>  $product['type'],
        ];
    }


    /**
     * @param $products
     * @return array
     */
    public function getProducts($products)
    {
        $array = [];

        foreach($products as $product)
        {
            $array[] = [

                'id'                    =>  $product['id'],
                'productOwner'          =>  $product['productOwner'],
                'productName'           =>  $product['productName'],
                'vendorId'              =>  $product['vendorId'],
                'vendorName'            =>  $product['vendorName'],
                'productActive'         =>  $product['productActive'],
                'productCategory'       =>  $product['productCategory'],
                'createdBy'             =>  $product['createdBy'],
                'modifiedBy'            =>  $product['modifiedBy'],
                'createdTime'           =>  $product['createdTime'],
                'modifiedTime'          =>  $product['modifiedTime'],
                'unitPrice'             =>  $product['unitPrice'],
                'link'                  =>  $product['link'],
                'image'                 =>  $product['image'],
                'level'                 =>  $product['level'],
                'video'                 =>  $product['video'],
                'subCategory'           =>  $product['subCategory'],
                'category'              =>  $product['category'],
                'status'                =>  $product['status'],
                'indicator'             =>  $product['indicator'],
                'gain'                  =>  $product['gain'],
                'deltaIncome'           =>  $product['deltaIncome'],
                'cover'                 =>  $product['cover'],
                'linkVideo'             =>  $product['linkVideo'],
                'signedUp'              =>  $product['signedUp'],
                'order'                 =>  $product['order'],
                'statusApproved'        =>  $product['statusApproved'],
                'courseProgress'        =>  $product['courseProgress'],
                'type'                  =>  $product['type'],
                'created_at'            =>  $product['created_at'],
                'updated_at'            =>  $product['updated_at']
            ];
        }

        return $array;

    }


}
