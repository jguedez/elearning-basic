<?php

namespace App\Transformers;


class PurchaseOrdersTransformer extends Transformer
{
    /**
     * @param $purchase
     * @return array
     */
    public function schema($purchase)
    {
        return [

            'id'                => $purchase['id'],
            'subject'           => $purchase['subject'],
            'vendorId'          => $purchase['vendorId'],
            'vendorName'        => $purchase['vendorName'],
            'contactId'         => $purchase['contactId'],
            'contactName'       => $purchase['contactName'],
            'poDate'            => $purchase['poDate'],
            'dueDate'           => $purchase['dueDate'],
            'Carrier'           => $purchase['carrier'],
            'exciseDuty'        => $purchase['exciseDuty'],
            'salesCommission'   => $purchase['salesCommission'],
            'status'            => $purchase['status'],
            'owner'             => $purchase['owner'],
            'createdBy'         => $purchase['createdBy'],
            'modifiedBy'        => $purchase['modifiedBy'],
            'createdTime'       => $purchase['createdTime'],
            'modifiedTime'      => $purchase['modifiedTime'],
            'subTotal'          => $purchase['subTotal'],
            'tax'               => $purchase['tax'],
            'adjustment'        => $purchase['adjustment'],
            'grandTotal'        => $purchase['grandTotal'],
            'billingStreet'     => $purchase['billingStreet'],
            'billingCity'       => $purchase['billingCity'],
            'billingState'      => $purchase['billingState'],
            'billingCountry'    => $purchase['billingCountry'],
            'productDetails'    => $purchase['productDetails'],
            'discount'          => $purchase['discount'],
            'currency'          => $purchase['currency'],
            'exchangeRate'      => $purchase['exchangeRate'],
            'bank'              => $purchase['bank'],
            'advance'           => $purchase['advance'],
            'paidStatus'        => $purchase['advance'],
            'dateAdvance'       => $purchase['dateAdvance'],

        ];

    }
}