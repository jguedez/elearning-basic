<?php

namespace App\Transformers;


class VendorTransformer extends Transformer
{
    /**
     * @param $vendor
     * @return array
     */
    public function schema($vendor)
    {
        return [

            'id'                  => $vendor['id'],
            'phone'               => $vendor['phone'],
            'email'               => $vendor['email'],
            'website'             => $vendor['website'],
            'glAccount'           => $vendor['glAccount'],
            'category'            => $vendor['category'],
            'vendorOwner'         => $vendor['vendorOwner'],
            'createdBy'           => $vendor['createdBy'],
            'modifiedBy'          => $vendor['modifiedBy'],
            'createdTime'         => $vendor['createdTime'],
            'modifiedTime'        => $vendor['modifiedTime'],
            'street'              => $vendor['street'],
            'city'                => $vendor['city'],
            'state'               => $vendor['state'],
            'zipCode'             => $vendor['zipCode'],
            'country'             => $vendor['country'],
            'description'         => $vendor['description'],
            'type'                => $vendor['type'],
            'biography'           => $vendor['biography'],
            'nameInstructor'      => $vendor['nameInstructor'],
            'littleDescription'   => $vendor['littleDescription'],
            'interest'            => $vendor['interest'],
            'age'                 => $vendor['age'],
            'gender'              => $vendor['gender'],
            'socialOne'           => $vendor['socialOne'],
            'socialTwo'           => $vendor['socialTwo'],
            'socialThree'         => $vendor['socialThree'],
            'socialFour'          => $vendor['socialFour'],
            'socialFive'          => $vendor['socialFive'],
            'socialSix'           => $vendor['socialSix'],
            'birthday'            => $vendor['birthday'],
            'experience'          => $vendor['experience'],
            'education'           => $vendor['education'],
            'created_at'          => $vendor['created_at'],
            'updated_at'          => $vendor['updated_at']
        ];
    }

    public function getVendors($vendors)
    {
        $array = [];
        foreach($vendors as $vendor)
        {
            $array[] = [

                'id'                  =>  $vendor['id'],
                'phone'               =>  $vendor['phone'],
                'email'               =>  $vendor['email'],
                'country'             =>  $vendor['country'],
                'experience'          =>  $vendor['experience'],
                'education'           =>  $vendor['education'],
                'biography'           =>  $vendor['biography'],
                'nameInstructor'      =>  $vendor['nameInstructor'],
                'vendorPicture'       =>  $vendor['vendorPicture'],
                'age'                 =>  $vendor['age'],
                'gender'              =>  $vendor['gender'],
                'socialOne'           =>  $vendor['socialOne'],
                'socialTwo'           =>  $vendor['socialTwo'],
                'socialThree'         =>  $vendor['socialThree'],
                'socialFour'          =>  $vendor['socialFour'],
                'socialFive'          =>  $vendor['socialFive'],
                'socialSix'           =>  $vendor['socialSix'],
                'littleDescription'   =>  $vendor['littleDescription'],
                'interest'            =>  $vendor['interest'],
            ];
        }

        return $array;
    }
}