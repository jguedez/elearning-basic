<?php

namespace App\Transformers;


class LeadsTransformer extends Transformer
{
    /**
     * @param $lead
     * @return array
     */
    public function schema($lead)
    {
        return [

            'id'           => $lead['id'],
            'leadOwner'    => $lead['leadOwner'],
            'lastName'     => $lead['lastName'],
            'email'        => $lead['email'],
            'leadSource'   => $lead['leadSource'],
            'createdTime'  => $lead['createdTime'],
            'modifiedTime' => $lead['modifiedTime'],
            'fullName'     => $lead['fullName'],
            'lastActivity' => $lead['lastActivity'],
            'description'  => $lead['description'],
            'country'      => $lead['country'],
            'created_at'   => $lead['created_at'],
            'key'          => $lead['key'],
            'Type'         => $lead['Type'],
            'updated_at'   => $lead['updated_at'],
        ];

    }
}