<?php

namespace App\Transformers;


class RegistrationsTransformer extends Transformer
{
    /**
     * @param $registration
     * @return array
     */
    public function schema($registration)
    {
        return [

            'id'                =>  $registration['id'],
            'date'              =>  $registration['date'],
            'inscriptionName'   =>  $registration['inscriptionName'],
            'owner'             =>  $registration['owner'],
            'createdBy'         =>  $registration['createdBy'],
            'modifiedBy'        =>  $registration['modifiedBy'],
            'createdTime'       =>  $registration['createdTime'],
            'modifiedTime'      =>  $registration['modifiedTime'],
            'lastActivity'      =>  $registration['lastActivity'],
            'currency'          =>  $registration['currency'],
            'exchangeRate'      =>  $registration['exchangeRate'],
            'parentCourseId'    =>  $registration['parentCourseId'],
            'parentCourse'      =>  $registration['parentCourse'],
            'userId'            =>  $registration[ 'userId' ],
            'user'              =>  $registration['user' ],
            'chargeClientId'    =>  $registration['chargeClientId'],
            'chargeClient'      =>  $registration['chargeClient' ],
            'start_at'          =>  $registration['start_at'],
            'ends_at'           =>  $registration['ends_at']
        ];
    }
}