<?php
namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Response;

class ProcedureHelpers {

    public static function GetMyCourses()
    {
        return $procedure = "CREATE PROCEDURE `get_my_courses`(IN contact BIGINT)
            BEGIN
	        SELECT 
	            productName AS CourseName,
                d.advanceName as LessonName,
                vendorName AS Instructor,
                selected.counted AS TotalAdvances,
                COUNT(selected.courseId) AS TotalLessons,
                selected.counted * 100 / COUNT(selected.courseId) AS PercentView,
                cover AS CoverCourse,
                CAST(d.moduleCourseId AS CHAR) as ModuleId,
                CAST(d.lessonCourseId AS CHAR) as LessonId,
                CAST(d.courseId AS CHAR) as CourseId,
                vendorId AS InstructorId
            FROM
            advances d
                INNER JOIN
            ((SELECT 
                MAX(a.id) AS advanceId,
                    COUNT(a.id) AS counted,
                    MAX(a.lessonCourseId) AS lessonCourseId,
                    a.courseId AS courseId
            FROM
                advances a
            INNER JOIN ((SELECT 
            MAX(advances.id) AS id,
                advances.lessonCourseId AS lessonCourseId
            FROM
            advances
            WHERE
                advances.contactId = contact
            GROUP BY advances.lessonCourseId) AS b) ON b.id = a.id
            GROUP BY a.courseId) AS selected) ON selected.advanceId = d.id
                JOIN
            lessons ON lessons.parentCourseId = d.courseId
            inner join products on products.id = d.courseId
            GROUP BY selected.courseId;
            END";
    }
}
