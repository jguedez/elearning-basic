<?php
namespace App\Helpers;

class PermissionHelper {

    public static function Administrador()
    {
        $permission = array(
            'products' => array (
                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),

            'contacts' => array (
                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'leads' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'users' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'modules' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'lessons' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'advances' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'purchase' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'registrations' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'sales' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'vendors' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'cases' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
        );

      return $permission;
    }

    public static function Instructor()
    {
        $permission = array(
            'products' => array (
                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),

            'contacts' => array (
                'index'   => true,
                'show'    => true,
                'store'   => false,
                'update'  => true,
                'destroy' => false
            ),
            'leads' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
            'users' => array (

                'index'   => false,
                'show'    => true,
                'store'   => false,
                'update'  => true,
                'destroy' => false
            ),
            'modules' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'lessons' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'advances' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'purchase' => array (

                'index'   => true,
                'show'    => true,
                'store'   => false,
                'update'  => false,
                'destroy' => false
            ),
            'registrations' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
            'sales' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
            'vendors' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => true,
                'destroy' => true
            ),
            'cases' => array (

                'index'   => true,
                'show'    => true,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
        );

      return $permission;
    }

    public static function Alumno()
    {
        $permission = array(
            'products' => array (
                'index'   => true,
                'show'    => true,
                'store'   => false,
                'update'  => false,
                'destroy' => false
            ),

            'contacts' => array (
                'index'   => true,
                'show'    => true,
                'store'   => false,
                'update'  => true,
                'destroy' => false
            ),
            'leads' => array (

                'index'   => false,
                'show'    => false,
                'store'   => false,
                'update'  => false,
                'destroy' => false
            ),
            'users' => array (

                'index'   => false,
                'show'    => true,
                'store'   => false,
                'update'  => true,
                'destroy' => false
            ),
            'modules' => array (

                'index'   => false,
                'show'    => true,
                'store'   => false,
                'update'  => false,
                'destroy' => false
            ),
            'lessons' => array (

                'index'   => false,
                'show'    => true,
                'store'   => false,
                'update'  => false,
                'destroy' => false
            ),
            'advances' => array (

                'index'   => false,
                'show'    => true,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
            'purchase' => array (

                'index'   => false,
                'show'    => false,
                'store'   => false,
                'update'  => false,
                'destroy' => false
            ),
            'registrations' => array (

                'index'   => false,
                'show'    => true,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
            'sales' => array (

                'index'   => false,
                'show'    => false,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
            'vendors' => array (

                'index'   => false,
                'show'    => true,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
            'cases' => array (

                'index'   => false,
                'show'    => false,
                'store'   => true,
                'update'  => false,
                'destroy' => false
            ),
        );

      return $permission;
    }

}