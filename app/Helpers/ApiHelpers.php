<?php
namespace App\Helpers;

use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Http\Response;

class ApiHelpers {

    /**
     * Generic API response structure for all requests
     *
     * @param $code string
     * @param $response mixed
     * @return \Illuminate\Http\JsonResponse
     */
    public static function ApiResponse($code, $response)
    {
        ///Trifase code menol
        $messageCode = 1;

        switch($code)
        {
            case 200:
            case 201:
            case 204:
                $messageCode = 1;
            break;
            case 400:
                $messageCode = 2;
            break;
            case 401:
                $messageCode = 103;
            break;
            case 403:
                $messageCode = 103;
            break;
            case 404:
                $messageCode = 303;
            break;
            case 407:
                $messageCode = 102;
            break;
            case 409:
                $messageCode = 2;
            break;
            case 500:
                $messageCode = 3;
            break;
            default:
                $messageCode = 1;
            break;

        }
            
        return response()->json([
            'code'     => $messageCode,
            'message'  => trans('api_codes.' . $code),
            'response' => $response
        ], $code);
    }
}