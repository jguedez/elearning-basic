<?php
namespace App\Zoho;

use Carbon\Carbon;
use CristianPontes\ZohoCRMClient\ZohoCRMClient;
use Illuminate\Support\Facades\Auth;

class Zoho
{
    /**
     * @var mixed
     */
    private $token;

    /**
     * ZohoSetter constructor.
     */
    public function __construct()
    {
        $this->token = config('zoho.token');
    }

    /**
     * @param $module
     * @return \CristianPontes\ZohoCRMClient\Request\Field[]|\CristianPontes\ZohoCRMClient\Response\Record[]
     */
    public function index($module)
    {
        $client = new ZohoCRMClient($module, $this->token);
        return $client->getRecords()
            ->request();
    }

    /**
     * @param $module
     * @param $id
     * @return \CristianPontes\ZohoCRMClient\Request\Field[]|\CristianPontes\ZohoCRMClient\Response\Record[]
     */
    public function getRecordsById($module, $id)
    {
        $client = new ZohoCRMClient($module, $this->token);
        return $client->getRecordById()
            ->id($id)
            ->request();
    }

    /**
     * @param $module
     * @param $fields
     * @return \CristianPontes\ZohoCRMClient\Response\MutationResult[]
     */
    public function create($module, $fields)
    {
        $client = new ZohoCRMClient($module, $this->token);
        return $client->insertRecords()
            ->addRecord($fields)
            ->triggerWorkflow()
            ->request();
    }

    /**
     * @param $module
     * @param $id
     * @param $fields
     * @return \CristianPontes\ZohoCRMClient\Response\MutationResult[]
     */
    public function update($module, $id, $fields)
    {
        $client = new ZohoCRMClient($module, $this->token);
        $fields['Id'] = $id;
        return $client->updateRecords()
            ->addRecord($fields)
            ->triggerWorkflow()
            ->request();
    }


    /**
     * @return string
     */
    private function getUserStamp()
    {
        if(empty(Auth::user())){ return ''; }
        return '# ' . Auth::user()->id . ' - ' . Auth::user()->name
        . ' ' . Auth::user()->last_name . ' @ ' . Carbon::now()->format('Y-m-d H:i');
    }


    /**
     * @param $module
     * @param $id
     * @return \CristianPontes\ZohoCRMClient\Response\MutationResult
     */
    public function delete($module, $id)
    {
        $client = new ZohoCRMClient($module, $this->token);
        return $client->deleteRecords()
            ->id($id)
            ->request();
    }


}