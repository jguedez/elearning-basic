<?php
namespace App\Zoho;

use App\Entities\Advances;
use App\Entities\Contact;
use App\Entities\Leads;
use App\Entities\Lessons;
use App\Entities\Modules;
use App\Entities\Products;
use App\Entities\PurchaseOrders;
use App\Entities\Registration;
use App\Entities\SalesOrders;
use App\Entities\Vendor;
use App\Entities\Cases;
use CristianPontes\ZohoCRMClient\Exception\UnexpectedValueException;
use CristianPontes\ZohoCRMClient\ZohoCRMClient;

class ZohoSync
{
    private $token;
    private $service;
    /* @var \Illuminate\Database\Eloquent\Model */
    private $model;
    private $inserts;
    private $client;
    private $serviceModels = [

        'Products'          => Products::class,
        'Contacts'          => Contact::class,
        'Vendors'           => Vendor::class,
        'Leads'             => Leads::class,
        'CustomModule2'     => Modules::class,
        'CustomModule3'     => Lessons::class,
        'CustomModule4'     => Registration::class,
        'SalesOrders'       => SalesOrders::class,
        'PurchaseOrders'    => PurchaseOrders::class,
        'CustomModule1'     => Advances::class,
        'Cases'             => Cases::class,

    ];

    /**
     * ZohoSync constructor.
     * @param $service
     */
    public function __construct($service)
    {
        $this->token        = config('zoho.token');
        $this->service      = $service;
        $this->inserts      = [];
        $this->model        = new $this->serviceModels[$service];
        $this->client       = new ZohoCRMClient($this->service, $this->token);
    }


    public function run()
    {
        $start = 1;
        $end   = 200;

        while(!isset($no_data))
        {

            $data = $this->client->getRecords()
                ->fromIndex($start)
                ->toIndex($end)
                //->since((new Carbon())->subDay(100))
                ->request();    
             

            if (!empty($data))
            {
                foreach ($data as $record)
                {
                    $record = $this->model->parseFromZoho($record);
                    $this->updateOrInsert($record);
                }

                $start = $end + 1;
                $end += 200;

            } else {
                $no_data = true;
            }
        }

        if (!empty($this->inserts))
        {
            foreach ($this->inserts as $insert)
            {
                $this->model->create($insert);
            }
        }
    }

    /**
     * @param $record
     */
    private function updateOrInsert($record)
    {
        $search = $this->model->where('id', $record['id'])->first();

        if(!empty($search))
        {
            $search->update($record);
        }
        else {
            $this->inserts[] = $record;
        }
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        //return $id;
        $this->model->where('id', $id)->delete();
        
    }
}