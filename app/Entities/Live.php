<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Live extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'lives';

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'resizable'  => 'json'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'start_at', 'ends_at', 'draggable', 'resizable',
        'product_id', 'lesson_id', 'user_id', 'duration_hours','channel'
    ];
}

