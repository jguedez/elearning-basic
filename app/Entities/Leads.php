<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class Leads extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'leads';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','leadOwner','lastName','email','leadSource','createdTime','modifiedTime','fullName',
        'description','country','lastActivity','key','Type','wfTrigger'
    ];

    public $zoho_fields = [

        'id'                => 'LEADID',
        'name'              => 'First Name',
        'leadOwner'         => 'Lead Owner',
        'lastName'          => 'Last Name',
        'email'             => 'Email',
        'leadSource'        => 'Lead Source',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'fullName'          => 'Full Name',
        'lastActivity'      => 'Last Activity Time',
        'description'       => 'Description',
        'country'           => 'Country',
        'key'               => 'Clave',
        'Type'              => 'Tipo',
        'wfTrigger'         => 'wfTrigger', 
        ];




}
