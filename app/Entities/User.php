<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use CristianPontes\ZohoCRMClient\Response\Record;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name','lastName','email','contactId', 'vendorId', 'level','password','tokenPassword','dateExpired', 'status'

    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'tokenPassword',
        'dateExpired'
    ];

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return Auth::user()->level == "Administrador";
    }

    /**
     * @return bool
     */
    public function isInstructor()
    {
        return Auth::user()->level == "Instructor";
    }

    /**
     * @return bool
     */
    public function isStudent()
    {
        return Auth::user()->level === "Alumno";
    }




}
