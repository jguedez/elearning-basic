<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class PurchaseOrders extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'purchase';


    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id'                => 'string',
        'productDetails'    => 'json'
    ];
      

 public function setWfTriggerAttribute($value)
    {
        $this->fillable['wfTrigger'] = 'true';
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','subject','vendorId','vendorName','contactId','contactName','poDate','dueDate','carrier','exciseDuty',
        'salesCommission','status','owner','createdBy','modifiedBy','createdTime','modifiedTime','subTotal','tax','adjustment',
        'grandTotal','billingStreet','billingCity','billingState','billingCountry','productDetails','discount','currency',
        'exchangeRate','wfTrigger','deltaIncome','bank','advance','dateAdvance','paidStatus'
    ];
//
    public $zoho_fields = [

        'id'                => 'PURCHASEORDERID',
        'subject'           => 'Subject',
        'vendorId'          => 'VENDORID',
        'vendorName'        => 'Vendor Name',
        'contactId'         => 'CONTACTID',
        'contactName'       => 'Contact Name',
        'poDate'            => 'PO Date',
        'dueDate'           => 'Due Date',
        'carrier'           => 'carrier',
        'exciseDuty'        => 'Excise Duty',
        'salesCommission'   => 'Sales Commission',
        'status'            => 'Status',
        'owner'             => 'Purchase Order Owner',
        'createdBy'         => 'Created By',
        'modifiedBy'        => 'Modified By',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'subTotal'          => 'Sub Total',
        'tax'               => 'Tax',
        'adjustment'        => 'Adjustment',
        'grandTotal'        => 'Grand Total',
        'billingStreet'     => 'Billing Street',
        'billingCity'       => 'Billing City',
        'billingState'      => 'Billing State',
        'billingCountry'    => 'Billing Country',
        'productDetails'    => 'Product Details',
        'discount'          => 'Description',
        'currency'          => 'Currency',
        'exchangeRate'      => 'Exchange Rate',
        'wfTrigger'         => 'wfTrigger',
        'deltaIncome'       => 'Porcentaje de ingresos',
        'bank'              => 'Banco 1',
        'advance'           =>  'Anticipo No 1.',
        'dateAdvance'       =>  'Fecha Anticipo No 1.',
        ];

}
