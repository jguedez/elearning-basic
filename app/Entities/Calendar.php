<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;

class Calendar extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'calendar';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','crmId','contactId','crmContactId','vendorId','crmVendorId','startDate','endDate','type','frequency','rule'
    ];

}
