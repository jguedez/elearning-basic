<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class Modules extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'modules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'moduleName','moduleOwner','order','createdBy','modifiedBy','createdTime',
        'modifiedTime','lastActivity','parentCourseId','parentCourse','description','currency','exchangeRate','wfTrigger'
    ];

    public function setWfTriggerAttribute($value)
    {
        $this->fillable['wfTrigger'] = 'true';
    }

    public $zoho_fields = [

        'id'                => 'CUSTOMMODULE2_ID',
        'moduleName'        => 'CustomModule2 Name',
        'moduleOwner'       => 'CustomModule2 Owner',
        'order'             => 'Orden',
        'createdBy'         => 'Created By',
        'modifiedBy'        => 'Modified By',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'lastActivity'      => 'Last Activity Time',
        'parentCourseId'    => 'Curso relacionado_ID',
        'parentCourse'      => 'Curso relacionado',
        'description'       => 'Descripción',
        'currency'          => 'Currency',
        'exchangeRate'      => 'Exchange Rate',
        'wfTrigger'         => 'wfTrigger',
        ];


}
