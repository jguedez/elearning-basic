<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;
use CristianPontes\ZohoCRMClient\Response\Record;

class Entity extends Model
{

    protected $zoho_fields = [];

    public function parseFromZoho(Record $record)
    {
        $parsed = [];

        foreach ($this->zoho_fields as $app => $zoho)
        {
            if(is_array($zoho))
            {
                foreach ($zoho as $sub_app => $sub_zoho)
                {
                    $parsed[$app][$sub_app] = $record->get($sub_zoho);
                }
            }
            else
            {
                $parsed[$app] = $record->get($zoho);
            }
        }

        return $parsed;
    }

    /**
     * @return array
     */
    public function parseToZoho()
    {
        $parsed = [];

        foreach ($this->zoho_fields as $app => $zoho)
        {
            if(is_array($zoho))
            {
                foreach ($zoho as $sub_app => $sub_zoho) {
                    $parsed[$sub_zoho] = isset($this->$app[$sub_app]) ? $this->$app[$sub_app] : '';
                }
            }
            else
            {
                $parsed[$zoho] = isset($this->$app) ? $this->$app : '';
            }
        }

       return $parsed;
    }

}