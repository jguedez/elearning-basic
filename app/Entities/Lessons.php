<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class Lessons extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'lessons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'duration','resourceType','lessonName','lessonOwner','order','createdBy','modifiedBy','createdTime',
        'modifiedTime','lastActivity','resource','parentModuleId','parentModule','parentCourseId','parentCourse','description',
        'wfTrigger', 'file_id'
    ];

    public function setWfTriggerAttribute($value)
    {
        $this->fillable['wfTrigger'] = 'true';
    }
    
    public $zoho_fields = [

        'id'                => 'CUSTOMMODULE3_ID',
        'duration'          => 'Duración',
        'resourceType'      => 'Tipo de recurso',
        'lessonName'        => 'CustomModule3 Name',
        'lessonOwner'       => 'CustomModule3 Owner',
        'order'             => 'Orden',
        'createdBy'         => 'Created By',
        'modifiedBy'        => 'Modified By',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'lastActivity'      => 'Last Activity Time',
        'resource'          => 'Recurso',
        'parentModuleId'    => 'Módulo del curso_ID',
        'parentModule'      => 'Módulo del curso',
        'parentCourseId'    => 'Curso_ID',
        'parentCourse'      => 'Curso',
        'description'       => 'Descripción',
        'wfTrigger'         => 'wfTrigger',
        ];


}
