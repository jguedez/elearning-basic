<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class SalesOrders extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'sales';


    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id'                => 'string',
        'productDetails'    => 'json'
    ];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $appends = ['productDetailed','productTotal'];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $hidden = ['productDetails'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','soNumber','subject','contactId','contactName','status','owner','createdBy','modifiedBy','createdTime',
        'modifiedTime','subTotal','tax','adjustment','grandTotal','productDetails','description',
        'discount','currency','exchangeRate','advance','bank','codeAdvance','typeAdvance','dateAdvance',
        'mountAdvance','debtAdvance','productId','vendorId','instructor','wfTrigger','percentageOfProfit','purchaseOrders'        
    ];

    public function setWfTriggerAttribute($value)
    {
        $this->fillable['wfTrigger'] = 'true';
    }


    /**
     * @return mixed
     */
    public function getProductDetailedAttribute()
    {
        return isset($this->productDetails) ? $this->productDetails[1] : null;
    }

    /**
     * @return mixed
     */
    public function getProductTotalAttribute()
    {
        return $this->productDetailed['Total'];
    }


    public $zoho_fields = [

        'id'                => 'SALESORDERID',
        'soNumber'          => 'SO Number',
        'subject'           => 'Subject',
        'contactId'         => 'CONTACTID',
        'contactName'       => 'Contact Name',
        'status'            => 'Status',
        'owner'             => 'Sales Order Owner',
        'createdBy'         => 'Created By',
        'modifiedBy'        => 'Modified By',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'subTotal'          => 'Sub Total',
        'tax'               => 'Tax',
        'adjustment'        => 'Adjustment',
        'grandTotal'        => 'Grand Total',
        'productDetails'    => 'Product Details',
        'description'       => 'Description',
        'discount'          => 'Discount',
        'currency'          => 'Currency',
        'exchangeRate'      => 'Exchange Rate',
        'advance'           => 'Anticipo No 1.',
        'bank'              => 'Banco 1',
        'codeAdvance'       => 'Código Anticipo No 1.',
        'typeAdvance'       => 'Tipo de pago Anticipo No 1.',
        'dateAdvance'       => 'Fecha Anticipo No 1.',
        'mountAdvance'      => 'Monto Anticipo',
        'debtAdvance'       => 'Deuda restante',
        'vendorId'          => 'Instructor_ID',
        'instructor'        => 'Instructor',
        'wfTrigger'         => 'wfTrigger',
        'percentageOfProfit' => 'Porcentaje de Ganancia',
        'purchaseOrders'    => 'Purchase Order',
    ];

}
