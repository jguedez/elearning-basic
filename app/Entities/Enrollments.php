<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;

class Enrollments extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'enrollments';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'advanceName','name','owner','createdBy','modifiedBy','createdTime','modifiedTime','lessonCourseId','lessonCourse',
        'courseId','course','moduleCourseId','moduleCourse','secondId','contactId','contact'
    ];

    public $zoho_fields = [
p
        ];

}
