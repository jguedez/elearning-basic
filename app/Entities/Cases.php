<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Cases extends Entity
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table= 'cases';
    //
    
    protected $fillable = [
        'addComment','subject','noOfComments','internalComments','email','createdBy','description',
        'status','createdTime','modifiedTime','reportedBy','modifiedBy','currency','caseReason','accountName','dealName'
        ,'productName','caseNumber','caseOrigin','priority','owner','relatedTo','solution','exchangeRate','phone','caseNumberText','type'
    ];

    public $zoho_fields = [

        'id'                    => 'CASEID',
        'status'                => 'Status',
        'caseOrigin'            => 'Case Origin',
        'subject'               => 'Subject',
        'relatedTo'             => 'WHOID',
        'description'           => 'Description',
        'internalComments'      => 'Internal Comments',
        'noOfComments'          => 'No of comments',
        'email'                 => 'Email',
        'createdBy'             => 'SMCREATORID',
        'modifiedBy'            => 'MODIFIEDBY',
        'reportedBy'            => 'Reported By',
        'currency'              => 'Currency',
        'caseReason'            => 'Case Reason',
        'accountName'           => 'ACCOUNTID',
        'dealName'              => 'POTENTIALID',
        'productName'           => 'Product Name',
        'caseNumber'            => 'Case Number',
        'priority'              => 'Priority',
        'solution'              => 'Solution',
        'exchangeRate'          => 'Exchange Rate',
        'phone'                 => 'Phone',
        'type'                  => 'Type'

    ];
}
