<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class Products extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'productOwner','productName','productCode','order','statusApproved','courseProgress','vendorId','vendorName',
        'productActive','productCategory','createdBy','modifiedBy','createdTime',
        'modifiedTime','unitPrice','commissionRate','usageUnit','qtyOrdered','qtyInStock','reorderLevel',
        'qtyInDemand','description','taxable','previous','link', 'image','level','video','subCategory','category','audience',
        'subTitle','duration','location','language','document','startCourse','descriptionCourse','objectivesCourse',
        'littleDescription','instructorEmail','instructorWeb','instructorName',
        'socialOne','biographyInstructor','socialTwo','phoneInstructor','socialFour','socialThree','photoInstructor',
        'education','distribution','deltaIncome','cover', 'linkVideo','signedUp','wfTrigger', 'duration_lic','type','aditionalInstructors',
        'live_hours'

    ];

    public function setWfTriggerAttribute($value)
    {
        $this->fillable['wfTrigger'] = 'true';
    }

    public $zoho_fields = [

        'id'                    => 'PRODUCTID',
        'productOwner'          => 'Product Owner',
        'productName'           => 'Product Name',
        'productCode'           => 'Product Code',
        'order'                 => 'Orden',
        'statusApproved'        => 'Status de aprobación',
        'courseProgress'        => 'Fase del curso',
        'vendorId'              => 'VENDORID',
        'vendorName'            => 'Vendor Name',
        'productActive'         => 'Product Active',
        'productCategory'       => 'Product Category',
        'createdBy'             => 'Created By',
        'modifiedBy'            => 'Modified By',
        'createdTime'           => 'Created Time',
        'modifiedTime'          => 'Modified Time',
        'unitPrice'             => 'Unit Price',
        'commissionRate'        => 'Commission Rate',
        'usageUnit'             => 'Usage Unit',
        'qtyOrdered'            => 'Qty Ordered',
        'qtyInStock'            => 'Qty in Stock',
        'reorderLevel'          => 'Reorder Level',
        'qtyInDemand'           => 'Qty in Demand',
        'description'           => 'Description',
        'taxable'               => 'Taxable',
        'previous'              => 'Requisitos previos',
        'link'                  => 'Link del registro',
        'image'                 => 'Imagen del curso',
        'level'                 => 'Nivel del curso',
        'video'                 => 'Video Promocional',
        'subCategory'           => 'Sub categoria',
        'category'              => 'Categoria',
        'audience'              => 'Audiencia',
        'subTitle'              => 'Sub Título',
        'duration'              => 'Duracion',
        'location'              => 'Ubicación',
        'language'              => 'Idioma',
        'document'              => 'Experiencia',
        'startCourse'           => 'Inicio del curso',
        'descriptionCourse'     => 'Beneficios',
        'objectivesCourse'      => 'Objetivos del curso',
        'littleDescription'     => 'Descripción corta del curso',
        'instructorEmail'       => 'Email del instructor',
        'instructorWeb'         => 'Sitio web del instructor',
        'instructorName'        => 'Nombre del instructor',
        'socialOne'             => 'Perfil social 1',
        'biographyInstructor'   => 'Biografía del instructor',
        'socialTwo'             => 'Perfil social 2',
        'phoneInstructor'       => 'Teléfono del instructor',
        'socialFour'            => 'Perfil social 4',
        'socialThree'           => 'Perfil social 3',
        'photoInstructor'       => 'Foto del instructor',
        'education'             => 'Educación',
        'distribution'          => 'Reparto de ingresos',
        'deltaIncome'           => 'Porcentaje de ingesos',
        'cover'                 => 'Foto portada del curso',
        'linkVideo'             => 'Link video promocional',
        'wfTrigger'             => 'wfTrigger',
        //Se agrego el campo type
        'type'                  => 'type',
        ];


}
