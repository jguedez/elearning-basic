<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class Registration extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'registrations';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'date','inscriptionName','owner','createdBy','modifiedBy','createdTime',
        'modifiedTime','lastActivity','currency','exchangeRate','parentCourseId','parentCourse',
        'userId','user', 'chargeClientId','payVendorId', 'start_at', 'ends_at', 'duration_lic',
        'status'
    ];

    public $zoho_fields = [

        'id'                    => 'CUSTOMMODULE4_ID',
        'date'                  => 'Fecha y hora inicio',
        'inscriptionName'       => 'CustomModule4 Name',
        'owner'                 => 'CustomModule4 Owner',
        'createdBy'             => 'Created By',
        'modifiedBy'            => 'Modified By',
        'createdTime'           => 'Created Time',
        'modifiedTime'          => 'Modified Time',
        'lastActivity'          => 'Last Activity Time',
        'currency'              => 'Currency',
        'exchangeRate'          => 'Exchange Rate',
        'parentCourseId'        => 'Curso relacionado_ID',
        'parentCourse'          => 'Curso relacionado',
        'userId'                => 'Usuario_ID',
        'user'                  => 'Usuario',
        'chargeClientId'        => 'Cobro al cliente_ID',
        'payVendorId'           => 'Pago a proveedor_ID',
        'wfTrigger'             => 'wfTrigger',        
    ];


}
