<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class Advances extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'advances';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'advanceName','name','owner','createdBy','modifiedBy','createdTime','modifiedTime','lessonCourseId','lessonCourse',
        'courseId','course','moduleCourseId','moduleCourse','secondId','contactId','contact'
    ];

    public $zoho_fields = [

        'id'                => 'CUSTOMMODULE1_ID',
        'advanceName'       => 'Avance en el curso Name',
        'name'              => 'CustomModule1 Name',
        'owner'             => 'Sales Order Owner',
        'createdBy'         => 'Created By',
        'modifiedBy'        => 'Modified By',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'lessonCourseId'    => 'Lección del curso_ID',
        'lessonCourse'      => 'Lección del curso',
        'courseId'          => 'Curso_ID',
        'course'            => 'Curso',
        'moduleCourseId'    => 'Módulo del curso_ID',
        'moduleCourse'      => 'Módulo del curso',
        'secondId'          => 'ID',
        'contactId'         => 'Contacto_ID',
        'contact'           => 'Contacto',
        ];

}
