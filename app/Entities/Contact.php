<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;

class Contact extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contactOwner','leadSource','name','lastName','accountId','accountName','email','phone','createdBy',
        'modifiedBy','createdTime','modifiedTime','fullName','mailingCountry','emailOptOut','gender','dateOfBirth',
        'lastActivity','tag','type','vendorId','vendorName', 'birthday'
    ];

    public $zoho_fields = [

        'id'                => 'CONTACTID',
        'contactOwner'      => 'Contact Owner',
        'leadSource'        => 'Lead Source',
        'name'              => 'First Name',
        'lastName'          => 'Last Name',
        'accountId'         => 'ACCOUNTID',
        'accountName'       => 'Account Name',
        'email'             => 'Email',
        'createdBy'         => 'Created By',
        'modifiedBy'        => 'Modified By',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'fullName'          => 'Full Name',
        'mailingCountry'    => 'Mailing Country',
        'emailOptOut'       => 'Email Opt Out',
        'lastActivity'      => 'Last Activity Time',
        'tag'               => 'Tag',
        'wfTrigger'         => 'wfTrigger',
        'gender'            => 'Género',
        'dateOfBirth'       => 'dateOfBirth',
        'type'              => 'Tipo de usuario',
        'vendorId'          => 'VENDORID', 
        'VendorName'        => 'Vendor Name',
        'phone'             =>  'Phone',
        ];




}
