<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
class Vendor extends Entity
{
    use Notifiable;

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'vendors';


    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'experience' => 'json',
        'education'  => 'json'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'vendorName','phone','email','website','glAccount','category','vendorOwner','createdBy','modifiedBy','createdTime','modifiedTime',
        'street','city','state','zipCode','country','description','type','biography','nameInstructor', 'littleDescription', 'interest', 'age',
        'gender','socialOne','socialTwo', 'socialThree', 'socialFour', 'socialFive', 'socialSix','vendorPicture','education','paypal','bank','wfTrigger',
        'birthday', 'experience', 'education','aditionalInstructors'
    ];

    public function setWfTriggerAttribute($value)
    {
        $this->fillable['wfTrigger'] = 'true';
    }

    public $zoho_fields = [

        'id'                => 'VENDORID',
        'vendorName'        => 'Vendor Name',
        'phone'             => 'Phone',
        'email'             => 'Email',
        'website'           => 'Website',
        'glAccount'         => 'GL Account',
        'category'          => 'Category',
        'vendorOwner'       => 'Vendor Owner',
        'createdBy'         => 'Created By',
        'modifiedBy'        => 'Modified By',
        'createdTime'       => 'Created Time',
        'modifiedTime'      => 'Modified Time',
        'street'            => 'Street',
        'city'              => 'City',
        'state'             => 'State',
        'zipCode'           => 'Zip Code',
        'country'           => 'Country',
        'description'       => 'Description',
        'type'              => 'Tipo de Proveedor',
        'biography'         => 'Biografía',
        'nameInstructor'    => 'Nombre de instructor',
        'littleDescription' => 'Descripción corta',
        'interest'          => 'Intereses',
        'age'               => 'Edad',
        'gender'            => 'Género',
        'socialOne'         => 'Perfil Social 1',
        'socialTwo'         => 'Perfil Social 2',
        'socialThree'       => 'Perfil Social 3',
        'socialFour'        => 'Perfil Social 4',
        'socialFive'        => 'Perfil Social 5',
        'socialSix'         => 'Perfil Social 6',
        'vendorPicture'     => 'Foto del Vendor',
        'education'         => 'Educación',
        'paypal'            => 'Paypal',
        'bank'              => 'Cuenta bancaria',
        'wfTrigger'         => 'wfTrigger',
    ];

}
