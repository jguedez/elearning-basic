<?php

namespace App\Providers;

use App\Entities\User;
use App\Entities\Cases;
use App\Entities\Leads;
use App\Entities\Vendor;
use App\Entities\Contact;
use App\Entities\Lessons;
use App\Entities\Modules;
use App\Entities\Advances;
use App\Entities\Products;
use App\Entities\SalesOrders;
use App\Policies\CasesPolicy;
use App\Policies\LeadsPolicy;
use App\Policies\UsersPolicy;
use App\Entities\Registration;
use App\Policies\LessonsPolicy;
use App\Policies\ModulesPolicy;
use App\Policies\VendorsPolicy;
use App\Entities\PurchaseOrders;
use App\Policies\AdvancesPolicy;
use App\Policies\ContactsPolicy;
use App\Policies\ProductsPolicy;
use App\Policies\SalesOrdersPolicy;
use App\Policies\RegistrationsPolicy;
use App\Policies\PurchaseOrdersPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class           =>  UsersPolicy::class,
        Products::class       =>  ProductsPolicy::class,
        Contact::class        =>  ContactsPolicy::class,
        Cases::class          =>  CasesPolicy::class,
        PurchaseOrders::class =>  PurchaseOrdersPolicy::class,
        SalesOrders::class    =>  SalesOrdersPolicy::class,
        Lessons::class        =>  LessonsPolicy::class,
        Modules::class        =>  ModulesPolicy::class,
        Advances::class       =>  AdvancesPolicy::class,
        Vendor::class         =>  VendorsPolicy::class,
        Leads::class          =>  LeadsPolicy::class,
        Registration::class   =>  RegistrationsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
