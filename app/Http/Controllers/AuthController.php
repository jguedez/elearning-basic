<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use App\Entities\User;
use App\Entities\Vendor;
use App\Entities\Contact;
use App\Entities\Advances;
use App\Helpers\ApiHelpers;
use Illuminate\Http\Request;
use App\Entities\SalesOrders;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Transformers\UsersTransformer;
use App\Http\Requests\Auth\UserRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RefreshToken;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\Auth\RegisterRequest;

class AuthController extends ApiController {

	/**
	 * @var UsersTransformer
	 */
	protected $userTransformer;

	/**
	 * @var Zoho
	 */
	protected $zoho;

	/**
	 * AuthController constructor.
	 * @param UsersTransformer $userTransformer
	 * @param Zoho $zoho
	 */
	public function __construct(UsersTransformer $userTransformer, Zoho $zoho) {
		$this->userTransformer = $userTransformer;
		$this->zoho = $zoho;
	}

	public function getUser(UserRequest $request) {

		try {
			$user = JWTAuth::parseToken()->toUser();
		} catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
			return $this->ApiResponse(401, "Token invalido");
		}

		return $this->ApiResponse(200, $user);
	}

	/**
	 * This method is used for login
	 *
	 * @param LoginRequest $request
	 * @return string
	 */
	public function login(LoginRequest $request) {

		$credentials = $request->only('email', 'password');
		$user = User::where('email', '=', $request->email)->first();

		$token = JWTAuth::attempt($credentials);
		#dd($token);
		
		if (!$token)
		{
			return $this->ApiResponse(400, 'Las credenciales son invalidas');

		}elseif($user['status'] == 1) {
			
			$user = JWTAuth::setToken($token)->toUser();
			return $this->ApiResponse(200, [
				'token' => $token,
				'refresh_ttl' => config('jwt.refresh_ttl'),
				'user' => $this->userTransformer->transform($user),
			]);

		}else{
			return $this->ApiResponse(400, 'Tu cuenta ha sido inhabilitada o las credenciales son invalidas');
		}
	}

	/**
	 * @param RegisterRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function register(RegisterRequest $request) {
		$data = $request;

		$resource_contact = Contact::select('id')
			->where('email', '=', $data->email)->first();

		$resource_vendor = Vendor::select('id')
		->where('email', '=', $data->email)->first();

		if ($data->level == 'Alumno') {
			return $message = $this->CreateStudent($data, $resource_contact);

		} elseif ($data->level == 'Instructor') {
			return $this->CreateInstructor($data, $resource_contact, $resource_vendor);
		
		} elseif ($data->level == 'Administrador') {
			return $this->ApiResponse(409, 'Oops');
			
		} else {
			return $this->ApiResponse(400, 'Oops');
		}
	}

	public function refresh(RefreshToken $request) {
		try
		{
			$token = $request->token;
			$newToken = JWTAuth::refresh($token);
		} catch (JWTException $e) {
			return ApiHelpers::ApiResponse(401, null);
		}

		return $this->ApiResponse(200, [
			'new_token' => $newToken,
			'expire in' => config('jwt.refresh_ttl'),
		]);
	}

	public function CreateStudent($data, $resource_contact) 
	{
		if (!empty($resource_contact)) {

			$new_user = [
				'email' => $data->email,
				'password' => bcrypt($data->password),
				'name' => $data->name,
				'lastName' => $data->lastName,
				'level' => $data->level,
				'contactId' => $resource_contact->id,
				'vendorId' => null,
			];

			$user = new User();
			$user->fill($new_user);

			$user->save();
			$token = JWTAuth::attempt(['email' => $data->email, 'password' => $data->password]);

			return $this->ApiResponse(200, [
				'token' => $token,
				'refresh_ttl' => config('jwt.refresh_ttl'),
				'user' => $this->userTransformer->transform($user->fresh()),
			]);

		} elseif (empty($resource_contact)) {
			$contact = new Contact();

			$new_contact = [
				'name' => $data->name,
				'lastName' => $data->lastName,
				'contactOwner' => 'Elearning B&D',
				'type' => $data->level,
				'gender' => $data->gender,
				'dateOfBirth' => $data->dateOfBirth,
				'mailingCountry' => $data->mailingCountry,
				'phone' => $data->phone,
				'leadSource' => 'Registro Principal desde el Menu',
				'vendorId' => null,
			];

			$contact->fill($new_contact);
			$contact->save();

			$new_user = [
				'email' => $data->email,
				'password' => bcrypt($data->password),
				'name' => $data->name,
				'lastName' => $data->lastName,
				'level' => $data->level,
				'contactId' => $contact->id,
				'vendorId' => null,
			];

			$user = new User();
			$user->fill($new_user);
			$user->save();

			$token = JWTAuth::attempt(['email' => $data->email, 'password' => $data->password]);

			return $this->ApiResponse(200, [
				'token' => $token,
				'refresh_ttl' => config('jwt.refresh_ttl'),
				'user' => $this->userTransformer->transform($user->fresh()),
			]);

			} else {
				return $this->ApiResponse(409, 'Este email ya se encuentra registrado');
			}
	}

	public function CreateInstructor($data, $resource_contact, $resource_vendor) {

		$instructor = $data->name . ' ' . $data->lastName;

		$vendor = new Vendor();

		$new_vendor = [
				'vendorOwner' => 'Elearning B&D',
				'vendorName' => $instructor,
				'nameInstructor' => $instructor,
				'email' => $data->email,
				'age' => $data->age,
				'country' => $data->country,
				'gender' => $data->gender,
				'city' => $data->city,
				'website' => $data->website,
				'street' => $data->street,
				'state' => $data->state,
				'zipCode' => $data->zipCode,
				'description' => $data->description,
				'biography' => $data->biography,
				'littleDescription' => $data->littleDescription,
				'interest' => $data->interest,
				'socialOne' => $data->socialOne,
				'socialTwo' => $data->socialTwo,
				'socialThree' => $data->socialThree,
				'socialFour' => $data->socialFour,
				'socialFive' => $data->socialFive,
				'socialSix' => $data->socialSix,
				'vendorPicture' => $data->vendorPicture,
				'education' => $data->education,
				'phone' => $data->phone,
				'type' => $data->level,
			];

			$vendor->fill($new_vendor);
			$vendor->save();

			$contact = new Contact();
			$new_contact = [
				'name' => $data->name,
				'lastName' => $data->lastName,
				'contactOwner' => 'Elearning B&D',
				'type' => 'Instructor',
				'vendorName' => $instructor,
				'vendorId' => $vendor->id,
				'leadSource' => 'Registro Principal desde el Menu',
			];

			$contact->fill($new_contact);
			$contact->save();
			
			$new_user = [
				'email' => $data->email,
				'password' => bcrypt($data->password),
				'name' => $data->name,
				'lastName' => $data->lastName,
				'level' => $data->level,
				'vendorId' => $vendor->id,
				'level' => 'Instructor',
				'contactId' => $contact->id,
			];

			$user = new User();
			$user->fill($new_user);
			$user->save();

			$token = JWTAuth::attempt(['email' => $data->email, 'password' => $data->password]);
					
			return $this->ApiResponse(200, [
					'token' => $token,
					'refresh_ttl' => config('jwt.refresh_ttl'),
					'user' => $this->userTransformer->transform($user->fresh()),
				]);
		}
}
