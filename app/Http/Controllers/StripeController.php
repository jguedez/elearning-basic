<?php

namespace App\Http\Controllers;

use Exception;
use App\Zoho\Zoho;
use Carbon\Carbon;
use PayPal\Api\{Item, Payer, Amount, Payment, ItemList, Transaction, RedirectUrls};
use App\Entities\Advances;
use Illuminate\Http\Request;
use App\Entities\Registration;
use Illuminate\Support\Facades\Auth;
use App\Entities\{Products, SalesOrders};
use App\Http\Requests\Stripe\StoreRequest;
use Stripe\{Stripe, Charge, Token, Customer};
use Stripe\Error\{Base, RateLimit, ApiConnection, InvalidRequest};

class StripeController extends ApiController
{
    /**
     * @var Zoho
     */
    protected $zoho;

   /**
     * StripeController constructor.
     */
    public function __construct(Zoho $zoho)
    {
        \Stripe\Stripe::setApiKey('sk_test_qPLcsvl0ODCMJJ5Uce0yDM3s');
        $this->zoho = $zoho;
    }

    public function payment(StoreRequest $request)
    {
        $data = $request;

        $product = Products::where('id', '=', $data->productId)->get();
        $price = $product[0]->unitPrice * 100;

        if ($price == 0)
        {
            return $this->CourseFree($product[0], $price, $data);

        }elseif ($data->method == 'stripe')
        {
            return $this->PaymentStripe($product[0], $price, $data);

        }else 
        {
            return $this->ApiResponse(400, 'Ha ocurrido un error durante la operacion');
        }
    }

    public function PaymentStripe($product, $price, $data)
    {
        $customer = \Stripe\Customer::create(array(
            'email' => Auth::user()->email,
            'source' => $data->token,
        ));

        try{
            $charge = \Stripe\Charge::create(
                array(
                    'amount' => $price,
                    'currency' => 'usd',
                    'customer' => $customer            )
            );
        } catch (\Stripe\Error\RateLimit $e) {
            return $this->ApiResponse(400, $e->getMessage());
          } catch (\Stripe\Error\InvalidRequest $e) {
            return $this->ApiResponse(400, $e->getMessage());
          } catch (\Stripe\Error\ApiConnection $e) {
            return $this->ApiResponse(400, $e->getMessage());
          } catch (\Stripe\Error\Base $e) {
            return $this->ApiResponse(400, $e->getMessage());
          } catch (Exception $e) {
            return $this->ApiResponse(400, $e->getMessage());
	  }
          if ($charge['status'] == 'succeeded')
          {
            //Generate Sales Orders with price > 0.5.
            $sale = new SalesOrders();
            $sales_order = [
                'soNumber'              =>  $data->productId,
                'subject'               =>  'Inscripción automatica en el curso '.$product->productName,
                'contactId'             =>  Auth::user()->contactId,
                'contactName'           =>  Auth::user()->name.' '.Auth::user()->lastName,
                'vendorId'              =>  $product->vendorId,
                'instructor'            =>  $product->vendorName,
                'owner'                 =>  'Elearning Nemobile',
                'status'                =>  'Creada',
                'subTotal'              =>  $price,
                'tax'                   =>  '0',
                'adjustment'            =>  '0',
                'grandTotal'            =>  $price,
                'bank'                  => 'Stripe',
                'codeAdvance'           =>  $data->token,
                'typeAdvance'           => 'TDC'
            ];
    
            $sales_order['productDetails'] = [

                1 => [
            
                    '@type'         => 'product',
                    'Product Id'    =>  $data->productId,
                    'Unit Price'    =>  $price,
                    'Quantity'      =>  '1',
                    'Total'         =>  '1',
                    'List Price'    =>  '0',
                    'Net Total'     =>  $price
                ]
            ];
    
            $sale->fill($sales_order);
            $sale->save();

           //Inscription Course
           $register = [
               'date'            => Carbon::now(),
               'inscriptionName' => $product->productName,
               'userId'          => Auth::user()->contactId,
               'user'            => Auth::user()->name.' '.Auth::user()->lastName,
               'parentCourseId'  => $product->id,
               'parentCourse'    => $product->productName,
               'chargeClientId'  => $sale->id,
               'owner'           => 'Elearning Nemobile',
               'createdBy'       => 'Elearning Nemobile',
               'createdTime'     => Carbon::now(),
               'currency'        => 'USD'
           ];
           
           $registration = new Registration();
           $registration->fill($register);
           $registration->save();

           $this->AdvancesCourse($data);
           return $this->ApiResponse(200, $registration);
        }
    }

    public function CourseFree($product, $price, $data)
    {
        //Generate Sales Orders price 0.
        $sale = new SalesOrders();
        $sales_order = [
            'soNumber'              =>  $data->productId,
            'subject'               =>  'Inscripción automatica en el curso '.$product->productName,
            'contactId'             =>  Auth::user()->contactId,
            'contactName'           =>  Auth::user()->name.' '.Auth::user()->lastName,
            'vendorId'              =>  $product->vendorId,
            'instructor'            =>  $product->vendorName,
            'owner'                 =>  'Elearning Nemobile',
            'status'                =>  'Creada',
            'subTotal'              =>  '0',
            'tax'                   =>  '0',
            'adjustment'            =>  '0',
            'grandTotal'            =>  '0',
            'bank'                  => 'Elearning Nemobile',
            'codeAdvance'           => 'Free',
            'typeAdvance'           => 'Free'
        ];

        $sales_order['productDetails'] = [
            1 => [
        
                '@type'         => 'product',
                'Product Id'    =>  $data->productId,
                'Unit Price'    =>  '0',
                'Quantity'      =>  '0',
                'Total'         =>  '0',
                'List Price'    =>  '0',
                'Net Total'     =>  '0'
            ]
        ];

        $sale->fill($sales_order);
        $sale->save();

       //Inscription Course
       $register = [
           'date'            => Carbon::now(),
           'inscriptionName' => $product->productName,
           'userId'          => Auth::user()->contactId,
           'user'            => Auth::user()->name.' '.Auth::user()->lastName,
           'parentCourseId'  => $product->id,
           'parentCourse'    => $product->productName,
           'chargeClientId'  => $sale->id,
           'owner'           => 'Elearning Nemobile',
           'createdBy'       => 'Elearning Nemobile',
           'createdTime'     => Carbon::now(),
           'currency'        => 'USD'
       ];
       
       $registration = new Registration();
       $registration->fill($register);
       $registration->save();

       $this->AdvancesCourse($data);
       
       return $this->ApiResponse(200, $registration);

    }

    public function AdvancesCourse($data)
    {   
        $advance = new Advances();
        $resource_data = 
        [
            'advanceName'    => $data->lessonCourse,
            'owner'          => 'Elearning Nemobile',
            'courseId'       => $data->productId,
            'moduleCourseId' => $data->moduleCourseId,
            'lessonCourseId' => $data->lessonCourseId,
            'lessonCourse'   => $data->lessonCourse,
            'contactId'      => Auth::user()->contactId
        ];

        $advance->fill($resource_data);
        $advance->save();
    }
}
