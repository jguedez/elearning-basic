<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use Carbon\Carbon;
use App\Entities\File;
use App\Entities\User;
use App\Entities\Lessons;
use App\Entities\Modules;
use App\Entities\Advances;
use App\Entities\Products;
use Illuminate\Http\Request;
use App\Entities\SalesOrders;
use App\Entities\Registration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Transformers\ProductsTransformer;
use App\Http\Requests\Products\ShowRequest;
use App\Http\Requests\Products\IndexRequest;
use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;
use App\Http\Requests\Products\DestroyRequest;


class ProductsController extends ApiController
{

    /**
     * @var ProductsTransformer
     */
    protected $productsTransformer;

    /**
     * @var Zoho
     */
    protected $zoho;

    /**
     * ProductsController constructor.
     * @param ProductsTransformer $productsTransformer
     * @param Zoho $zoho
     */
    public function __construct(ProductsTransformer $productsTransformer, Zoho $zoho)
    {
        $this->productsTransformer = $productsTransformer;
        $this->zoho = $zoho;
        $this->middleware('VerifyAuthToken')->except('index', 'alternativesProducts', 'show', 'searchCourse', 'ListToCourse');
    }

    public function index(IndexRequest $request)
    {
        $prod_products = Products::where('statusApproved','Aprobado')
        ->where('courseProgress','En producción')
        ->where('productCategory','Cursos Online')->get()->toArray();

        return $this->ApiResponse(200, $prod_products);
    }
    /**
     * @return null
     */
    private function getProductId()
    {
        $getProductId = Registration::where('userId', Auth::user()->contactId)->get(['parentCourseId']) ? true : false;
        $evaluate = isset($getProductId[0]['parentCourseId']) ? $getProductId[0]['parentCourseId'] : null;
        return $evaluate;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsAuth()
    {
        return $this->ApiResponse(200, $this->canIn());
    }

    /**
     * @param $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductAuthById($product)
    {
        $product = Products::findOrFail($product)->toArray();
        $product['status'] = $product['id'] == $this->getProductId() ? 'active' : 'no-active';
        
        return $this->ApiResponse(200, $this->productsTransformer->transform($product));
    }


    /**
     * Display a listing of the resource.
     */
    public function alternativesProducts()
    {
        $products = Products::all()->toArray();
        return $this->ApiResponse(200, $this->productsTransformer->getProducts($this->setter($products)));
        
      
    }
    
    /**
     * @param $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $product)
    {
        
        $products = Products::findOrFail($product)->toArray();

        $products['status']     = 'no-active';
        $products['indicator']  = $this->usersWithProducts($product);
        $products['gain']       = (int)$products['deltaIncome'] * (int)$products['unitPrice'] * $this->usersWithProducts($product) / 100;

        $modules = $this->join(new Modules(), 'parentCourseId', $product);
        $lessons = $this->join(new Lessons(), 'parentCourseId', $product);

        if ($modules && $lessons) {
            foreach ($modules as $parser => $key) {
                foreach ($lessons as $secondParser => $secondKey) {
                    if ($secondKey['parentModuleId'] == $key['id']) {
                        $modules[$parser]['Lessons'][] = $secondKey;
                    }

                }
            }

        }

        return $this->ApiResponse(200,
            ["Product" => $products, "Modules" => is_null($modules) ? $modules = [['lessons' => []]] : $modules]);
   

    }

    public function show_product_auth($product)
    {

        $products = Products::findOrFail($product)->toArray();

        $products['status']     = 'no-active';
        $products['indicator']  = $this->usersWithProducts($product);
        $products['gain']       = (int)$products['deltaIncome'] * (int)$products['unitPrice'] * $this->usersWithProducts($product) / 100;

        $modules = $this->join(new Modules(),  'parentCourseId', $product);
        $lessons = $this->join(new Lessons(),  'parentCourseId', $product);
        $advances= Advances::where('courseId', $product)
                            ->where('contactId',Auth::user()->contactId)
                            ->get();
        //

        if ($modules && $lessons) {
            foreach ($modules as $parser => $key) {
                foreach ($lessons as $secondParser => $secondKey) {
                    if ($secondKey['parentModuleId'] == $key['id']) {
                        $secondKey['viewed']="false";
                        foreach($advances as $thirdParsers=> $thirdKey){
                                if($thirdKey['lessonCourseId']==$secondKey['id']){
                                    $secondKey['viewed']="true";
                                    break;
                                }
                        }
                        $modules[$parser]['Lessons'][] = $secondKey;
                    }

                }
            }

        }
        $products['Modules']=is_null($modules) ? $modules = [['lessons' => []]] : $modules;
        
        return $this->ApiResponse(200,$products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {   
        $duration = Carbon::now('utc')->diffInDays(Carbon::now('utc')->addYear(12));

        $vendor = DB::table('vendors')
            ->select('vendorName')
            ->where('id', '=', $request->vendorId)->first();

        $resource_data = $request->all();

        $product = new Products();
        $resource_data['createdBy'] = Auth::user()->name.' '. Auth::user()->lastName;
        $resource_data['createdTime'] = Carbon::now();
        $resource_data['modifiedTime'] = Carbon::now();
        $resource_data['vendorName'] = $vendor->vendorName;
        $product->fill($resource_data);
        $product->save();

        $sale = new SalesOrders();
        $sales_order = [
            'soNumber'              =>  $product->id,
            'subject'               =>  'Inscripción en el curso '.$product->productName,
            'contactId'             =>  Auth::user()->contactId,
            'contactName'           =>  Auth::user()->name.' '.Auth::user()->lastName,
            'vendorId'              =>  Auth::user()->vendorId,
            'instructor'            =>  Auth::user()->name.' '.Auth::user()->lastName,
            'owner'                 =>  'Elearning Nemobile',
            'status'                =>  'Creada',
            'subTotal'              =>  $product->unitPrice,
            'tax'                   =>  '0',
            'adjustment'            =>  '0',
            'grandTotal'            =>  $product->unitPrice,
            'bank'                  =>  'Default',
            'codeAdvance'           =>  str_random(5),
            'typeAdvance'           =>  'Instructor'
        ];

        $sales_order['productDetails'] = [

            1 => [

                '@type'         => 'product',
                'Product Id'    =>  $product->id,
                'Unit Price'    =>  $product->unitPrice,
                'Quantity'      =>  '1',
                'Total'         =>  '1',
                'List Price'    =>  '0',
                'Net Total'     =>  $product->unitPrice
            ]
        ];

        $sale->fill($sales_order);
        $sale->save();

        $register = [
            'wfTrigger'       => 'true',
            'date'            => Carbon::now('utc'),
            'inscriptionName' => $product->productName,
            'userId'          => Auth::user()->id,
            'user'            => Auth::user()->name.' '.Auth::user()->lastName,
            'parentCourseId'  => $product->id,
            'parentCourse'    => $product->productName,
            'chargeClientId'  => $sale->id,
            'owner'           => 'Elearning Nemobile',
            'createdBy'       => 'Elearning Nemobile',
            'createdTime'     => Carbon::now('utc'),
            'currency'        => 'USD',
            'start_at'        => Carbon::now('utc'),
            'ends_at'         => Carbon::now('utc')->addYear(12),
            'duration_lic'    => $duration,
            'status'          => true,
        ];

        $registration = new Registration();
        $registration->fill($register);
        $registration->save();

        return $this->AssignOrUpdateFile($request->file_id, $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $product = Products::findOrFail($id);
        $resource_data = $request->onlyWith([
            'productOwner', 'productName', 'productCode', 'order', 'statusApproved', 'courseProgress', 'vendorId', 'vendorName',
            'productActive', 'productCategory', 'createdBy', 'modifiedBy', 'createdTime',
            'modifiedTime', 'unitPrice', 'commissionRate', 'usageUnit', 'qtyOrdered', 'qtyInStock', 'reorderLevel',
            'qtyInDemand', 'description', 'taxable', 'previous', 'link', 'image', 'level', 'video', 'subCategory', 'category', 'audience',
            'subTitle', 'duration', 'location', 'language', 'document', 'startCourse', 'descriptionCourse', 'objectivesCourse',
            'littleDescription', 'instructorEmail', 'instructorWeb', 'instructorName',
            'socialOne', 'biographyInstructor', 'socialTwo', 'phoneInstructor', 'socialFour', 'socialThree', 'photoInstructor',
            'education', 'distribution', 'deltaIncome', 'cover', 'linkVideo','signedUp','type', 'duration_lic','aditionalInstructors',
            'live_hours'
        
        ]);

        $product->fill($resource_data);
        $product->save();
        $product['modifiedTime'] = Carbon::now();  
        $product['modifiedBy'] = Auth::user()->name.' '. Auth::user()->lastName;
        
        return $this->AssignOrUpdateFile($request->file_id, $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        $students = $this->CheckAlumn($id);
        $course =  Products::where('id', '=', $id)->first();

        if ($students > 1)
        {
            return $this->ApiResponse(400, 'Oops, este curso posee usuarios matriculados');
        }

        if (Auth::user()->level != 'Instructor' && Auth::user()->level != 'Administrador')
        {
            return $this->ApiResponse(401, 'Usted no posee permisos suficientes para realizar esta accion');
        }

        if ($course->statusApproved == 'Por aprobar')
        {
            Advances::where('courseId', '=', $id)->delete();
            Lessons::where('parentCourseId', '=', $id)->delete();
            Modules::where('parentCourseId', '=', $id)->delete();
            Products::findOrFail($id)->delete();

            return $this->ApiResponse(200, 'El curso ha sido borrado satisfactoriamente');

        }
        if (Auth::user()->level == 'Instructor' && $course->statusApproved == 'Aprobado')
        {
            return $this->ApiResponse(400, 'Oops, usted no puede eliminar un curso aprobado');

        }
        
        if (Auth::user()->level == 'Administrador')
        {   
            Advances::where('courseId', '=', $id)->delete();
            Lessons::where('parentCourseId', '=', $id)->delete();
            Modules::where('parentCourseId', '=', $id)->delete();
            Products::findOrFail($id)->delete();

            return $this->ApiResponse(200, 'El curso ha sido borrado satisfactoriamente');    
        }

    }

    /**
     * @return null
     */
    private function canIn()
    {
        if ((new User())->isInstructor())
        {
            return Products::where('vendorId', Auth::user()->vendorId)->get()->toArray();
        }

        $getCourseId = Registration::where('userId', Auth::user()->contactId)->get()->toArray() ?
            Registration::where('userId', Auth::user()->contactId)->get()->toArray() : null;

        return Products::findOrFail($getCourseId[0]['parentCourseId']);
    }


    /**
     * @param $products
     * @return \Illuminate\Http\JsonResponse
     */
    private function setter($products)
    {
        foreach ($products as $parser => $product)
        {
            $pIncome = (int)is_null($product['deltaIncome']) ? 1 : $product['deltaIncome'];
            $output = function ($pIncome, $unitPrice, $callback)
            {
                if ($pIncome == 1) {
                    return $pIncome * (int)$unitPrice * $callback;
                }

                return $pIncome * (int)$unitPrice * $callback / 100;
            };

            $products[$parser]['status'] = 'no-active';
            $products[$parser]['indicator'] = $this->usersWithProductsAdmin($product['id']);
            if($products[$parser]['indicator']!=0 || $products[$parser]['indicator']!=null){
                $products[$parser]['indicator']=$products[$parser]['indicator'];
            }
            $products[$parser]['gain'] = $output($pIncome, $product['unitPrice'], $products[$parser]['indicator']);

        }

        return $products;
    }

    /**
     * @return mixed
     */
    public function gettingParameters()
    {
        if (((new User())->isInstructor()) || (new User())->isAdmin())
        {
            $getProducts = Products::where('vendorId', Auth::user()->vendorId)
            ->orderBy('updated_at', 'desc')->get()->toArray();
            
            return $this->ApiResponse(200,
                $this->productsTransformer->getProducts($this->setter($getProducts)));
        }

        return $this->ApiResponse(403, 'Only Instructors or Administrator');
    }

    public function searchCourse(Request $request)
    {

        $vendorName = $request->query('instructor');
        $courseName = $request->query('course');
        
        $result = $this->CheckSearch($vendorName, $courseName);

        return $this->ApiResponse(200, $result);
    }


    private function CheckSearch($vendorName, $courseName)
    {
        if (!empty($courseName) && empty($vendorName))
        {
            return $query = DB::table('products')
            ->join('vendors', 'products.vendorId', '=', 'vendors.id')
            ->select(DB::raw('products.id AS productId, products.productName, vendors.id AS vendorId, vendors.nameInstructor'))
            ->where('products.productName', 'like', '%'.$courseName.'%')->get();

        }elseif (!empty($vendorName) && empty($courseName))
        {
            return DB::table('products')
                    ->join('vendors', 'products.vendorId', '=', 'vendors.id')
                    ->select(DB::raw('products.id AS productId, products.productName, vendors.id AS vendorId, vendors.nameInstructor'))
                    ->where('vendors.nameInstructor', 'like', '%'.$vendorName.'%')->get();

        }elseif (!empty($vendorName) && !empty($courseName))
        {
            return DB::table('products')
                    ->join('vendors', 'products.vendorId', '=', 'vendors.id')
                    ->select(DB::raw('products.id AS productId, products.productName, vendors.id AS vendorId, vendors.nameInstructor'))
                    ->where('vendors.nameInstructor', 'like', '%'.$vendorName.'%')
                    ->Orwhere('products.productName', 'like', '%'.$courseName.'%')->get();
        }
    }
    
    public function ListToCourse(Request $request)
    {
         $status = $request->query('status');
        
        if($status == 'dev')
        {
            $data = $this->CourseToDev();

            return $this->ApiResponse(200, $data);
            
        }elseif($status == 'review')
        {
            $data = $this->CourseToReview();
            
            return $this->ApiResponse(200, $data);

        }else{
            return $this->ApiResponse(404, 'Ha seleccionado una opcion invalida');
        }
        
    }
    
    private function CourseToReview()
    {
        return Products::where('statusApproved','Por aprobar')
        ->Orwhere('statusApproved','Aprobado')
        ->where('courseProgress','En producción')
        ->where('productCategory','Cursos Online')->get(); 
        
    }
    
    private function CourseToDev()
    {
        return Products::where('statusApproved','En creación')
        ->where('courseProgress','En desarrollo')
        ->where('productCategory','Cursos Online')->get(); 
    }

        /**
     * @param $id
     * @return int
     */
    public function usersWithProductsAdmin($id)
    {
        $registrations = DB::table('registrations')
                ->whereDate('registrations.ends_at', '>=', Carbon::now('utc')->toDateTimeString())
                ->where('registrations.status', '=', true)
                ->where('registrations.userId', '!=', Auth::user()->id)
                ->get()->toArray();

        $array = [];
        #dd($registrations);
        foreach ($registrations as $parser => $registration)
        {
            if($registration->parentCourseId == $id)
            {
                $array[] = $registration->userId;
            }
        }

        return count($array);
    }

    public function CheckAlumn($course)
    {
        return DB::table('registrations')
            ->where('parentCourseId', '=', $course)
            ->where('userId', '!=', null)
            ->whereDate('ends_at', '>=', Carbon::now('utc')->toDateTimeString())
            ->where('status', '=', true)
            ->count();
    }

    public function AssignOrUpdateFile($file_id, $model)
    {
         $files = File::find($file_id);
        
         if (empty($files))
         {
            $model->save();
            return $this->ApiResponse(200, $model['id']);
         }

         $resource_file = [
            'product_id' => $model['id'],
         ];

         $files->fill($resource_file);
         $files->save();

         $model['file_id'] = $files['id'];
         $model['photo_cover'] = $files['path_resource'];
         $model->save();

         return $this->ApiResponse(200, $model['id']);
    }
}
