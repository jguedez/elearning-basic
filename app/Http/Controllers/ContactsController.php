<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use Dotenv\Validator;

use App\Entities\File;
use App\Entities\User;
use App\Entities\Contact;
use Illuminate\Http\Request;
use App\Helpers\PermissionHelper;
use Illuminate\Support\Facades\Auth;
use App\Transformers\ContactTransformer;
use App\Http\Requests\Contacts\ShowRequest;
use App\Http\Requests\Contacts\IndexRequest;
use App\Http\Requests\Contacts\StoreRequest;
use App\Http\Requests\Contacts\UpdateRequest;
use App\Http\Requests\Contacts\DestroyRequest;


class ContactsController extends ApiController
{

    /**
     * @var contactTransformer
     */
    protected $contactTransformer;

    /**
     * ContactController constructor.
     * @param ContactTransformer $contactTransformer
     * @param Zoho $zoho
     */
    public function __construct(ContactTransformer $contactTransformer,Zoho $zoho)
    {
        $this->contactTransformer  = $contactTransformer;
        $this->zoho = $zoho;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $contacts = $this->paginateResource($request, new Contact(), [
            'id','contactOwner','leadSource','name','lastName','accountId','accountName','email','phone','createdBy',
            'modifiedBy','createdTime','modifiedTime','fullName','mailingCountry','emailOptOut','gender','dateOfBirth',
            'lastActivity','tag','type','wfTrigger','vendorId','vendorName', 'birthday']);

        return $this->ApiResponse(200, $contacts);
    }

    public function show(ShowRequest $request, $id)
    {
        $contact = Contact::findOrFail($id);

        return $this->ApiResponse(200,
            $this->contactTransformer->transform($contact));
    }

    public function HasPermissionTo($user)
    {
        if ($user->level == 'Administrador')
        {
            $hola = PermissionHelper::Administrador();

            return $hola;
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $resource_data = $request->onlyWith([
            'contactOwner','leadSource','name','lastName','accountId','accountName','email','phone','createdBy',
            'modifiedBy','createdTime','modifiedTime','fullName','mailingCountry','emailOptOut','gender','dateOfBirth',
            'lastActivity','tag','type','wfTrigger','vendorId','vendorName', 'birthday', 'file_id', 
            'photo_resource'
        ]);

        $contact = new Contact();
        $contact->fill($resource_data);
        $contact->save();

        return $this->AssignOrUpdateFile($request->file_id, $contact);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  UpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $contact = Contact::findOrFail($id);

        $resource_data = $request->onlyWith([
            'contactOwner','leadSource','name','lastName','accountId','accountName','email','phone','createdBy',
            'modifiedBy','createdTime','modifiedTime','fullName','mailingCountry','emailOptOut','gender','dateOfBirth',
            'lastActivity','tag','type','wfTrigger','vendorId','vendorName', 'birthday', 'file_id', 
            'photo_resource'
        ]);

        $contact->fill($resource_data);
        $contact->save();

        return $this->AssignOrUpdateFile($request->file_id, $contact);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        Contact::findOrFail($id)->delete();
            
        return $this->ApiResponse(200, null);
    }


    public function AssignOrUpdateFile($file_id, $model)
    {
         $files = File::find($file_id);
        
         if (empty($files))
         {
            $model->save();
            return $this->ApiResponse(200, $model['id']);
         }

         $resource_file = [
            'contact_id' => $model['id'],
         ];

         $files->fill($resource_file);
         $files->save();

         $model['file_id'] = $files['id'];
         $model['photo_resource'] = $files['path_resource'];
         $model->save();

         return $this->ApiResponse(200, $model['id']);
    }
}
