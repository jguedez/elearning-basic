<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Calendar;
use App\Entities\Live;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\Calendar\ShowRequest;
use App\Http\Requests\Calendar\IndexRequest;
use App\Http\Requests\Calendar\StoreRequest;
use App\Http\Requests\Calendar\UpdateRequest;
use App\Http\Requests\Calendar\DestroyRequest;
use DateTime;

class CalendarController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $calendar= Calendar::where("vendorId",Auth::user()->vendorId)->get()->toArray();
        $events= Live::where("user_id",Auth::user()->id)->get();

       return $this->ApiResponse(200,["Availability"=>$calendar,"Inscriptions"=>$events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      /*Recibe un objeto de tipo [{"start_date":"2018-11-28 1:00:00","end_date":"2018-11-28 2:00:00"},{"start_date":"2018-11-28 2:00:00","end_date":"2018-11-28 3:00:00"}] */
            $data= json_decode($request["rules"]);
            
           // $this->verifyshock();
            foreach($data as $item){
                
                //$this->verifyshock($item);
                Calendar::create(["rule"=>$item->rule,
                                  "type"=>$item->type,
                                  "frequency"=>$item->frequency,
                                  "contactId"=>Auth::user()->contactId,
                                  "vendorId"=>Auth::user()->vendorId
                                 ]);
            }
            //$d=new DateTime($data[0]->start_date);
            //dd($d);
            return $this->ApiResponse(200,"Operacion Completada");
    }

    /**
     * Display the specified resource.a
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $calendar = Calendar::findOrFail($id);

        return $this->ApiResponse(200,$calendar);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $calendar = Calendar::findOrFail($id);

        $resource_data = $request->onlyWith([
            'type','frequency','rule'
        ]);

        dd($resource_data);

        $calendar->fill($resource_data);
        $calendar->save();

        return $this->ApiResponse(200, null);
    
        //
        $calendar= Calendar::findOrFail($id);

        $resource_data = $request->onlyWith([
            'crmId','contactId','crmContactId','vendorId','crmVendorId','startDate','endDate','duration','rule','type'
        ]);

        $calendar->fill($resource_data);
        $calendar->save();

        return $this->ApiResponse(200, null);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Calendar::findOrFail($id)->delete();
            
        return $this->ApiResponse(200, null);
    }
}
