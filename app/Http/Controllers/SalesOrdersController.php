<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use App\Entities\User;
use App\Entities\Lessons;
use App\Entities\Advances;
use App\Entities\Products;
use Illuminate\Http\Request;
use App\Entities\SalesOrders;
use App\Entities\Registration;
use App\Entities\PurchaseOrders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Transformers\SalesOrdersTransformer;
use App\Http\Requests\SalesOrders\ShowRequest;
use App\Http\Requests\SalesOrders\IndexRequest;
use App\Http\Requests\SalesOrders\StoreRequest;
use App\Http\Requests\SalesOrders\UpdateRequest;
use App\Http\Requests\SalesOrders\DestroyRequest;


/**
 * Class SalesOrdersController
 * @package App\Http\Controllers
 */
class SalesOrdersController extends ApiController
{

    /**
     * @var $this->salesOrdersTransformer
     */
    protected $salesOrdersTransformer;

    /**
     * @var Zoho
     */
    protected $zoho;

    /**
     * ModulesController constructor.
     * @param SalesOrdersTransformer $salesOrdersTransformer
     * @param Zoho $zoho
     */
    public function __construct(SalesOrdersTransformer $salesOrdersTransformer, Zoho $zoho)
    {
        $this->salesOrdersTransformer   = $salesOrdersTransformer;
        $this->zoho                     = $zoho;
    }

    /**
     * Display a listing of the resource.
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $sales = $this->paginateResource($request, new SalesOrders(), [
            'id','soNumber','subject','contactId','contactName','status','owner','createdBy','modifiedBy','createdTime',
            'modifiedTime','subTotal','tax','adjustment','grandTotal','productDetails','description',
            'discount','currency','exchangeRate','advance','bank','codeAdvance','typeAdvance','dateAdvance',
            'mountAdvance','debtAdvance','productId','vendorId', 'instructor','purchaseOrders']);

        return $this->ApiResponse(200, $sales);
    }


    /**
     * @param string $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function incomeForTotal($value = '1d')
    {
        $output  = SalesOrders::where('vendorId', Auth::user()->id);
        $output  = $this->gettingDate($output,$value)->get()->toArray();

        $all = array_map(function($output)
        {
            return ['date' => $output['created_at'], 'total' => $output['grandTotal']];

        }, $output);

        return $this->ApiResponse(200,
            ['data' => $all ? $all : null, 'sum' => array_sum(array_column($all,'total'))]);
    }

    /**
     * @param string $value
     * @return \Illuminate\Http\JsonResponse
     */
    public function incomeForPercent($value = '1d')
    {
        $output  = SalesOrders::where('vendorId',Auth::user()->id);
        $output  = $this->gettingDate($output,$value)->get()->toArray();
        $percent = array_map(function($getter)
        {
          return [ 'date' => $getter['created_at'],
              'percent' => (int) $getter['grandTotal'] * $getter['percentageOfProfit']];

        },$output);

        return $this->ApiResponse(200,
            ['data' =>  $percent ?  $percent : null, 'sum' => array_sum(array_column($percent,'percent'))]);
    }

    /**
     * @param Builder $response
     * @param $value
     * @return Builder
     */
    private function gettingDate(Builder $response, $value)
    {

        if(!$value){ return $response;}

        switch ($value)
        {
            case '1d':
                $response->where('createdTime', '>=', $this->timeCalculate('subDay',1));
                break;
            case '1m':
                $response->where('createdTime', '>=',$this->timeCalculate('subMonths',1));
                break;
            case '1y':
                $response->where('createdTime','>=', $this->timeCalculate('subYears',1));
        }

        return $response;

    }

    /**
     * @param $value
     * @return mixed
     */
    protected function otto($value)
    {
        return explode(" ", $value)[0];
    }

    /**
     * @param $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
        $product = SalesOrders::findOrFail($id);

        return $this->ApiResponse(200,
            $this->salesOrdersTransformer->transform($product));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $resource_data = $request->onlyWith([
            'soNumber','subject','contactId','contactName','status','owner','createdBy','modifiedBy','createdTime',
            'modifiedTime','subTotal','tax','adjustment','grandTotal','productDetails','description',
            'discount','currency','exchangeRate','advance','bank','codeAdvance','typeAdvance','dateAdvance',
            'mountAdvance','debtAdvance','productId','vendorId','instructor','wfTrigger','percentageOfProfit'
        ]);


        $resource_data['productDetails'] = [

            1 => [

                '@type'         => 'product',
                'Product Id'    => $request->input('productId'),
                'Unit Price'    => $request->input('unitPrice'),
                'Quantity'      => $request->input('quantity'),
                'Total'         => $request->input('unitPrice'),
                'List Price'    => $request->input('listPrice'),
                'Net Total'     => $request->input('netTotal')
            ]

        ];

        $sales = new SalesOrders();
        $sales->fill($resource_data);
        $sales->save();

        $count = Products::findOrFail($request->input('productId'))['deltaIncome'] / 100;

        $resource_data['subTotal'] *=  $count;
        $resource_data['grandTotal'] *= $count;

        $this->sendToPurchaseOrders($request->input('productId'), $resource_data, $response[1]->id);

       return  $this->ApiResponse(201,
           ['Courses' => Products::where('vendorId', '=', $resource_data['vendorId'])->get(), $response[1]->id]);

    }

    /**
     * @param $id
     * @param array $collection
     * @param $sales
     * @return $this
     */
    private function sendToPurchaseOrders($id, $collection = [], $sales)
    {
        $product = Products::findOrFail($id)['deltaIncome'];

        $po = new PurchaseOrders();
        $this->array_push_assoc($collection, 'deltaIncome', $product);

        foreach($collection['productDetails'] as $parser => $key)
        {
            $collection['productDetails'] = $this->transform($key, $product);
            $collection['bank']="Paypal";
        }

        $po->deltaIncome = $product;
        $po->fill($collection);
        $po->save();

        $this->sendToInscriptions($collection,$sales, $response[1]->id);

        return $this;

    }

    /**
     * @param $collection
     * @param $salesOrders
     * @param $purchaseOrders
     * @return $this
     */
    private function sendToInscriptions($collection, $salesOrders, $purchaseOrders)
    {
        $register = new Registration();

        $resource_data = [

            'inscriptionName'       => $collection['subject'],
            'owner'                 => 'The Biz Nation',
            'currency'              => $collection['currency'],
            'parentCourseId'        => $collection['productId'],
            'userId'                => $collection['contactId'],
            'user'                  => $collection['contactName'],
            'chargeClientId'        => $salesOrders,
            'payVendorId'           => $purchaseOrders

        ];

        $register->fill($resource_data);
        $register->save();

        return $this;
    }

    /**
     * @param $value
     * @param $product
     * @return array
     */
    private function transform($value, $product)
    {
        return [

            1 => [
                '@type'       => $value['@type'],
                'Product Id'  => (int) $value['Product Id'],
                'Unit Price'  => $value['Unit Price']   * $product / 100,
                'Quantity'    => $value['Quantity'],
                'Total'       => $value['Total']        * $product / 100,
                'List Price'  => $value['List Price']   * $product / 100,
                'Net Total'   => $value['Net Total']    * $product / 100,
            ]

        ];

    }

    /**
     * @param $array
     * @param $key
     * @param $value
     * @return mixed
     */
    private function array_push_assoc($array, $key, $value)
    {
        $array[$key] = $value;
        return $array;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  UpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $sales = SalesOrders::findOrFail($id);

        $resource_data = $request->onlyWith([
            'soNumber','subject','contactId','contactName','status','owner','createdBy','modifiedBy','createdTime',
            'modifiedTime','subTotal','tax','adjustment','grandTotal','productDetails','description',
            'discount','currency','exchangeRate','advance','bank','codeAdvance','typeAdvance','dateAdvance',
            'mountAdvance','debtAdvance','productId','vendorId','instructor','wfTrigger','percentageOfProfit'
        ]);

        $sales->fill($resource_data);
        $sales->save();

        return $this->ApiResponse(200, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        SalesOrders::findOrFail($id)->delete();

        return $this->ApiResponse(200, null);
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     */

     public function get()
    {
            return $this->ApiResponse(200, $this->authResource(New SalesOrders(), 'vendorId'));
    }

    public function getSalesOrders()
    {
        $array = $this->authResource(New SalesOrders());
        $output = [];

        foreach($array->toArray() as $item => $value)
        {
            $so = Lessons::where('parentCourseId', $value['productDetailed']['Product Id'])->get();
            $output[$item] = array_merge($value, ['nroLeccion' => count($so)]);
        }
  
        return $this->ApiResponse(200,$output);
    }
    
    public function MyCoursesAuth()
    {
       $user = Auth::user()->contactId;
       $courses = DB::select('call get_my_courses(?)', array($user));

        return $this->ApiResponse(200, $courses);
    }

    public function getSalesOrdersAuth()
    {

        $sales_orders  =  SalesOrders::where('contactId', Auth::user()->contactId)->get()->toArray();
        return $this->ApiResponse(200, $sales_orders);
 
    }

    public function gettingSalesParameters()
    { 
        if ((new User())->isInstructor())
        {
           
            $output["Products"]=[];
            $getSales = SalesOrders::where('vendorId',Auth::user()->vendorId)->get()->toArray();
            $getProducts = Products::where('vendorId',Auth::user()->vendorId)->get()->toArray();
            $contador=0;
            $suma=0;
            $total_sum=0;
            $total_count=0;
            
                    
            foreach($getProducts as $product){
                foreach($getSales as $sale){
                    if($product["id"]==$sale["productDetailed"]["Product Id"] && $sale["contactId"]!=Auth::user()->contactId){
                        $contador=$contador+1;
                        $suma=$suma+($sale["productDetailed"]["Unit Price"]*$product["deltaIncome"]/100);
                        
                        $total_count=$total_count+1;
                        $total_sum=$total_sum+($sale["productDetailed"]["Unit Price"]*$product["deltaIncome"]/100);
                    }
                }
                $product["Indicator"]=$contador;
                $product["gain"]=$suma;
                $contador=0;
                $suma=0;
                array_push($output["Products"],$product);
            }          
            $object= (object) ['Total_Count'=>$total_count, 'Total_Sum'=>$total_sum];
            $output["Totals"]=$object;
            
            return $this->ApiResponse(200,$output);
         }

        return $this->ApiResponse(200, $bandera);
    }

}
