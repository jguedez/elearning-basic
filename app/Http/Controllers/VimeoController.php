<?php

namespace App\Http\Controllers;

use App\Vimeo\Vimeo;
use App\Entities\File;
use App\Entities\Lessons;
use Illuminate\Http\Request;
 

class VimeoController extends ApiController
{
    //
    public function __construct(){
        $this->vimeo = new Vimeo(
            '7be6dd9144d981ca164e2a9240e19fbb7b956d68',
            '3C5LKbEHW0cLmP0ScqxSJGUR9TJDTwjqoV3yZGa5jRIF4u9fKCfraaWxxNrOprSoau3mJrVcBwU06mEUd6LNSYO0UWEZA6RTSgfDbC1iLD4TUjMHIOwgkjvHiXhZM9Rw',
            '7eae703aa88e571efdbfd6ba448fb917'
        );
    }
    
    public function getVideo(){
        
        $uri="/me/videos/";
        $response=$this->vimeo->request($uri, ['per_page' => 1], 'GET');
        return $response;

    }
    public function getLessonsWithoutDuration(){

        $lessons=Lessons::where("resourceType","Video Vimeo")->where("duration",null)->get();
        $lesson_ids=[];
        $cont=0;
        $counter=0;
        $video_info=[];
        $resp="";

        $videos=[ ];
        //$videos[2]="245979372";
        //$videos[1]="245979504";
        $videos[0]=["id"=>"2635099000001917575",
                    "resource"=>"245979372",
        ];

        foreach ($lessons as $lesson) {
            //$lesson_ids[$cont]=$lesson->resource;
            $vec=explode("/",$lesson->resource);
            $lesson_ids[$cont]=[
                                "id"=>$lesson->id,
                                "resource"=>end($vec)
                               ];
            $cont++;
        }

        $durations=[];
        $responses = [];
        $i=0;///
        foreach ($lesson_ids as $video) {
            $resp=$this->getVideo($video["resource"]);
            if(isset($resp["body"]["duration"]) ){  
                $durations[$i] = gmdate("H:i:s",$resp["body"]["duration"]);
                $lesson = Lessons::findOrFail($video["id"]);
                $lesson->duration=$durations[$i];
                $responses[$i]=[
                    "id"=>$video["id"],
                    'duration'=>$durations[$i]
                ];
                $lesson->save();
            } else {
                $durations[$i] = "None";
            }
            $i++;
            if($i==20){
                break;
            }
        }
 
        return $resp;

    }

    public function VimeoFile(Request $request)
    {
        $account = $this->GetKeyAccountConfig();
        $state = $this->GetCodeVimeo();
        $url = 'https://files.nemobile.app/api/';
        $route = 'vimeo/upload_file';
        $rest = 'POST';

        $body = array(
            'json' => array(
                'state' => $state,
                'user'  => $account['key'],
                'subscription_id' => $account['id'],
                'upload_type' => 'post_form',
                'file_name' => $request->name,
                'redirect_url' => 'https://www.example.com'
            )
        );
        $upload = $this->ClientHttpRequest($url, $route, $rest, $body);

        return response()->json($upload);
    }

    /**
     * Service code status Vimeo
     * @return $code 
     */
    protected function GetCodeVimeo()
    {
        $rest = 'GET';
        $url = 'https://files.nemobile.app/api/';
        $route = 'get_state';
        $data = $this->ClientHttpRequest($url, $route, $rest, $body = null);
        
        $code = $data->state;

        return $code;
    }

    public function UriVimeo(Request $request)
    {
        $account = $this->GetKeyAccountConfig();
        $state = $this->GetCodeVimeo();
        $url = 'https://files.nemobile.app/api/';
        $route = 'vimeo/video/version';
        $rest = 'POST';

        $body = array(
            'json' => array(
                'state' => $state,
                'user'  => $account['key'],
                'subscription_id' => $account['id'],
                'video_id' => $request->video_id
            )
        );
        $uri = $this->ClientHttpRequest($url, $route, $rest, $body);

        $files = new File();
        
        $resource_file = [
            'file_id' => $request->video_id,
            'type_resource' => 'video',
            'path_resource' => $uri->uri,
        ];
        
        $files->fill($resource_file);
        $files->save();

        return response()->json($files);
    }
}
