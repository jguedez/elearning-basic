<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use App\Entities\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Transformers\VendorTransformer;
use App\Http\Requests\Vendors\ShowRequest;
use App\Http\Requests\Vendors\IndexRequest;
use App\Http\Requests\Vendors\StoreRequest;
use App\Http\Requests\Vendors\UpdateRequest;


class VendorsController extends ApiController
{

    /**
     * @var Zoho
     */
    protected $zoho;

    /**
     * @var $vendorTransformer
     */
    protected $vendorTransformer;


    /**
     * VendorsController constructor.
     * @param VendorTransformer $vendorTransformer
     * @param Zoho $zoho
     */
    public function __construct(VendorTransformer $vendorTransformer, Zoho $zoho)
    {
        $this->vendorTransformer = $vendorTransformer;
        $this->zoho = $zoho;

    }

    /**
    
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
       
        $vendors = Vendor::all();

        return $this->ApiResponse(200, $vendors);
    }

    public function InstructorsList(IndexRequest $request)
    {
        $vendors = Vendor::all();

        return $this->ApiResponse(200, $this->vendorTransformer->getVendors($vendors));
    }
    
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
       $vendor = Vendor::findOrFail($id);
       return $this->ApiResponse(200, $vendor);

    }

    public function test(ShowRequest $request, $id)
    {
        $vendor = Vendor::findOrFail($id);

        return $this->ApiResponse(200, $vendor);

    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $resource_data = $request->onlyWith([
            'vendorName','phone','email','website','glAccount','category','vendorOwner','createdBy','modifiedBy','createdTime','modifiedTime',
            'street','city','state','zipCode','country','description','type','biography','nameInstructor', 'littleDescription', 'interest', 'age',
            'gender','socialOne','socialTwo', 'socialThree', 'socialFour', 'socialFive', 'socialSix','vendorPicture','education','paypal','bank',
            'birthday', 'experience', 'education'
        ]);

        $vendor = new Vendor();
        $vendor->fill($resource_data);
        $vendor->save();

        return $this->ApiResponse(201, $vendor->id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  UpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $vendor = Vendor::findOrFail($id);

        $resource_data = $request->onlyWith([
            'vendorName','phone','email','website','glAccount','category','vendorOwner','createdBy','modifiedBy','createdTime','modifiedTime',
            'street','city','state','zipCode','country','description','type','biography','nameInstructor', 'littleDescription', 'interest', 'age',
            'gender','socialOne','socialTwo', 'socialThree', 'socialFour', 'socialFive', 'socialSix','vendorPicture','education','paypal','bank',
            'birthday', 'experience', 'education'
        ]);

        $vendor->fill($resource_data);
        $vendor->save();

        return $this->ApiResponse(200, null);
    }

    public function AdvancesByStudent(Request $request)
    {   
        $instructor = $request->query('instructor');
        
        $progress = DB::select('call progress_per_course(?)', array($instructor));

        return $this->ApiResponse(200, $progress);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        Vendor::findOrFail($id)->delete();
        
        return $this->ApiResponse(200, null);
    }
}
