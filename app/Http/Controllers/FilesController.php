<?php

namespace App\Http\Controllers;

use Exception;
use App\Entities\File;
use GuzzleHttp\Client;
use App\Entities\Contact;
use App\Entities\Lessons;
use App\Entities\Products;
use GuzzleHttp\Psr7\Request;
use function GuzzleHttp\json_decode;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Files\StoreRequest;
use Psr\Http\Message\ServerRequestInterface;
use App\Http\Requests\Files\DeleteFileRequest;
use App\Http\Requests\Files\UploadFileRequest;

class FilesController extends ApiController
{

    /**
     * FilesController constructor.
     */
    public function __construct()
    {
       
    }

    /**
    * Create Google Drive folders 
    * @return $code 
    */
    public function UploadFiles(UploadFileRequest $request)
    {
       $account = $this->GetKeyAccount();
       $state = $this->GetCodeFiles();
       $url = 'drive/upload_file';
       $rest = 'POST';
       $file = $request->file('file_path');
       
       if (empty($request->folder_name))
       {
            $body = [
                'multipart' => [
                [
                    'name'     => 'state',
                    'contents' => $state
                ],
                [
                    'name'     => 'user',
                    'contents' => $account['key']
                ],
                [
                    'name'     => 'subscription_id',
                    'contents' => $account['id']
                ],
                [
                    'name'     => 'folder_name',
                    'contents' => 'nemobile'
                ],
                [
                    'name'     => 'file_path',
                    'contents' => fopen($file, 'r'),
                ],
                [
                    'name'     => 'file_name',
                    'contents' => $file->getClientOriginalName(),
                ]
            ]];
       }else
       {
        $body = [
            'multipart' => [
            [
                'name'     => 'state',
                'contents' => $state
            ],
            [
                'name'     => 'user',
                'contents' => $account['key']
            ],
            [
                'name'     => 'subscription_id',
                'contents' => $account['id']
            ],
            [
                'name'     => 'folder_name',
                'contents' => $request->folder_name,
            ],
            [
                'name'     => 'file_path',
                'contents' => fopen($file, 'r'),
            ],
            [
                'name'     => 'file_name',
                'contents' => $file->getClientOriginalName(),
            ]
        ]];

       }
        $upload = $this->ClientHttp($url, $rest, $body);
        $files = new File();
        
        switch ($request->type_resource)
        {
           case 'img':
               $resource_file = [
                   'file_id' => $upload->file_id,
                   'type_resource' => 'img',
                   'path_resource' => 'http://drive.google.com/uc?export=view&id='.$upload->file_id,
               ];
           break;
           case 'pdf':
               $resource_file = [
                   'file_id' => $upload->file_id,
                   'type_resource' => 'pdf',
                   'path_resource' => 'https://drive.google.com/file/d/'.$upload->file_id.'/view',
               ];
           break;
           case 'doc':
           $resource_file = [
               'file_id' => $upload->file_id,
               'type_resource' => 'doc',
               'path_resource' => 'https://docs.google.com/document/d/'.$upload->file_id.'/view',
           ];
           break;
           case 'pptx':
           $resource_file = [
               'file_id' => $upload->file_id,
               'type_resource' => 'pptx',
               'path_resource' => 'https://docs.google.com/presentation/d/'.$upload->file_id.'/view',
           ];
           break;
        }
        
        $files->fill($resource_file);
        $files->save();

        return response()->json($files);
    }

    /**
    * Delete Google Drive files 
    * @return $code 
    */
    public function DeleteFile(DeleteFileRequest $request)
    {
       $account = $this->GetKeyAccount();
       $state = $this->GetCodeFiles();
       $url = 'drive/delete';
       $rest = 'DELETE';
       $files = File::find($request->file_id);

       $body = [
           'json' => [
            'state' => $state,
            'user'  => $account['key'],
            'subscription_id' => $account['id'],
            'file_id' => $files->file_id
           ]
       ];

       $file = $this->ClientHttp($url, $rest, $body);

       return $this->CheckDelete($request->file_id, $file);
    }

    /**
    * Create Google Drive folders 
    * @return $code 
    */
    public function Folders(StoreRequest $request)
    {
       $account = $this->GetKeyAccount();
       $state = $this->GetCodeFiles();
       $url = 'drive/create_folder';
       $rest = 'POST';
    
       $body = array(
            'json' => array(
                'state' => $state,
                'user'  => $account['key'],
                'subscription_id' => $account['id'],
                'folder_name' => $request->name
            )
        );
    
        $folder = $this->ClientHttp($url, $rest, $body);

       return response()->json($folder);
    }

    /**
     * Service code status files
     * @return $code 
     */
    protected function GetCodeFiles()
    {
        $rest = 'GET';
        $url = 'get_state';
        $data = $this->ClientHttp($url, $rest, $body = null);
        
        $code = $data->state;

        return $code;
    }
    
     /**
     * Service Key Client Accounts
     * @return $code 
     */
    protected function GetKeyAccount()
    {
        return [
            'key' => $key = Config::get('Account.secret_key'),
            'id'  => $id = Config::get('Account.id_subscription'),
        ];
    }

    /**
     * Get http service
     * @param $route 
     */
    protected function ClientHttp($route, $rest, $body)
    {
        try {
            $client = new Client([
                'base_uri' => 'https://files.nemobile.app/api/',
                'timeout'  => 100.0
            ]);
            
            if (!empty($body))
            {
                $response = $client->request($rest, $route, $body);
                
                $contents = (string) $response->getBody();
                $data = json_decode($contents);
                
                return $data;

            }else
            {
                $response = $client->request($rest, $route);
                $contents = (string) $response->getBody();
                $data = json_decode($contents);
                
                return $data;
            }

        } catch (Exception $e) {
            return $this->ApiResponse(400, $e->getMessage());
        }
    }

    public function CheckDelete($id, $file)
    {
        $files_obj = File::find($id);
        
        if (!empty($files_obj['contact_id']) && $file->response == true)
       {
            $contact = Contact::find($files_obj['contact_id']);
            $contact['file_id'] = null;
            $contact['photo_resource'] = null;
            $contact->save();

            $files_obj->delete();
       }

       if (!empty($files_obj['product_id']) && $file->response == true)
       {
            $product = Products::find($files_obj['product_id']);    
            $product['file_id'] = null;
            $product['photo_cover'] = null;
            $product->save();

            $files_obj->delete();
       }

       if (!empty($files_obj['lesson_id']) && $file->response == true)
       {
            $lesson = Lessons::find($files_obj['lesson_id']);
            $lesson['file_id'] = null;
            $lesson['type_resource'] = null;
            $lesson['path_resource'] = null;
            $lesson->save();

            $files_obj->delete();
       }

        return response()->json($file);
    }
}
