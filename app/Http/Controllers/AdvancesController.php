<?php

namespace App\Http\Controllers;

use App\Entities\Lessons;
use App\Entities\Advances;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Transformers\AdvancesTransformer;
use App\Http\Requests\Advances\ShowRequest;
use App\Http\Requests\Advances\StoreRequest;
use App\Http\Requests\Advances\UpdateRequest;
use App\Http\Requests\Advances\DestroyRequest;

class AdvancesController extends ApiController
{

    /**
     * @var $this->advancesTransformer
     */
    protected $advancesTransformer;

    /**
     * ModulesController constructor.
     * @param advancesTransformer $advancesTransformer
     */
    public function __construct(AdvancesTransformer $advancesTransformer)
    {
        $this->advancesTransformer  = $advancesTransformer;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $advances = Advances::all();

        return $this->ApiResponse(200, $advances);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexAuth()
    {
        $data = Advances::where('contactId', Auth::user()->contactId)->get()->toArray();
        return $this->ApiResponse(200, $data);
    }

    /**
     * @param $course
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexWithParameter($course)
    {
        $data = Advances::where('contactId', Auth::user()->contactId)->get()->toArray();
        $output = [];

        foreach($data as $value => $item)
        {
            if($item['courseId'] == $course)
            {
                $output[] = $item;
            }
        }

        return $this->ApiResponse(200, $data);
    }

    /**
     * @param $advance
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
        $advance = Advances::findOrFail($id);

        return $this->ApiResponse(200,
            $this->advancesTransformer->transform($advance));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $resource_data = $request->onlyWith([
            'advanceName','name','owner','createdBy','modifiedBy','createdTime','modifiedTime','lessonCourseId','lessonCourse',
            'courseId','course','moduleCourseId','moduleCourse','secondId','contactId','contact'
        ]);
        
        $product = new advances();
        $product->fill($resource_data);
        $product['contactId'] = Auth::user()->id;
        $product->save();

       return $this->ApiResponse(200, $product);
    }


    /**
     * @param UpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $product = Advances::findOrFail($id);

        $resource_data = $request->onlyWith([
            'id','advanceName','name','owner','createdBy','modifiedBy','createdTime','modifiedTime','lessonCourseId','lessonCourse',
            'courseId','course','moduleCourseId','moduleCourse','secondId','contactId','contact'
        ]);

        $product->fill($resource_data);        
        $product->save();

        return $this->ApiResponse(200, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        Advances::findOrFail($id)->delete();

        return $this->ApiResponse(200, null);
    }

    public function GetLastLesson()
    {
        $user = Auth::user()->contactId;

        $last_lesson = DB::table('advances')
        ->join('products', 'advances.courseId', '=', 'products.id')
        ->select(DB::raw('MAX(advanceName) AS LessonName'), 'courseId', 'products.ProductName', DB::raw('MAX(advances.updated_at) as last_view'))
        ->where('contactId', '=', array($user))
        ->groupBy('courseId')
        ->get();
        return $this->ApiResponse(200, $last_lesson);
    }

}
