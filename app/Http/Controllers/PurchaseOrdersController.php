<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use Illuminate\Http\Request;
use App\Entities\SalesOrders;
use App\Entities\PurchaseOrders;
use Illuminate\Support\Facades\Auth;
use App\Transformers\PurchaseOrdersTransformer;
use App\Http\Requests\PurchaseOrders\ShowRequest;
use App\Http\Requests\PurchaseOrders\IndexRequest;
use App\Http\Requests\PurchaseOrders\StoreRequest;
use App\Http\Requests\PurchaseOrders\UpdateRequest;
use App\Http\Requests\PurchaseOrders\DestroyRequest;

class PurchaseOrdersController extends ApiController
{

    /**
     * @var $this->purchaseOrdersTransformer
     */
    protected $purchaseOrdersTransformer;

    /**
     * @var Zoho
     */
    protected $zoho;

    /**
     * ModulesController constructor.
     * @param purchaseOrdersTransformer $purchaseOrdersTransformer
     * @param Zoho $zoho
     */
    public function __construct(PurchaseOrdersTransformer $purchaseOrdersTransformer, Zoho $zoho)
    {
        $this->purchaseOrdersTransformer   = $purchaseOrdersTransformer;
        $this->zoho                         = $zoho;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(IndexRequest $request)
    {
        $output = [];

        $output = PurchaseOrders::all()->toArray() ;

       return $this->ApiResponse(200, $output);

    }

    /**
     * @param $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
        $product = PurchaseOrders::findOrFail($id);
        foreach($product as $prod){
            $prod["paid_stutus"]=$prod["advance"];
        }

        return $this->ApiResponse(200,
            $this->purchaseOrdersTransformer->transform($product));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $resource_data = $request->onlyWith([
            'subject','vendorId','vendorName','contactId','contactName','poDate','dueDate','carrier','exciseDuty',
            'salesCommission','status','owner','createdBy','modifiedBy','createdTime','modifiedTime','subTotal','tax','adjustment',
            'grandTotal','billingStreet','billingCity','billingState','billingCountry','productDetails','discount','currency',
            'exchangeRate','bank','advance','dateAdvance',
        ]);

        $purchase = new PurchaseOrders();
        $purchase->fill($resource_data);
        $purchase->save();

        return $this->ApiResponse(201, $response[1]->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  UpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {

        $purchase = PurchaseOrders::findOrFail($id);

        $resource_data = $request->onlyWith([
            'subject','vendorId','vendorName','contactId','contactName','poDate','dueDate','carrier','exciseDuty',
            'salesCommission','status','owner','createdBy','modifiedBy','createdTime','modifiedTime','subTotal','tax','adjustment',
            'grandTotal','billingStreet','billingCity','billingState','billingCountry','productDetails','discount','currency',
            'exchangeRate','bank','advance','dateAdvance','paidStatus'
        ]);

        $purchase->fill($resource_data);
        $purchase->save();
        
        return $this->ApiResponse(200, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        PurchaseOrders::findOrFail($id)->delete();
        
        return $this->ApiResponse(200, null);
    }

    public function GetPushesOrdersAuth()
    {
        $user_id = Auth::user()->vendorId;
        $data = PurchaseOrders::where('vendorId', $user_id)->get()->toArray();

        return $this->ApiResponse(200, $data);
    }


}
