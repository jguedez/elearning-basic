<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Entities\User;
use GuzzleHttp\Client;
use App\Helpers\ApiHelpers;
use Illuminate\Http\Request;
use App\Entities\Registration;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class ApiController extends Controller
{

    /**
     * @var int
     */
    protected $paginationSize = 1000;

    /**
     * Each resource request should define this property
     * @var $resource string
     */
    protected $resource;

    /**
     * @param Request $request
     * @param Model $resource
     * @param array $fields
     * @return mixed
     */
    protected function paginateResource(Request $request, Model $resource, $fields = ['*'])
    {
        $pageSize      = (int) request('pagination_size',$this->paginationSize);
        $modifiedTime  = request('update_time','2017-11-20');
        $sort          = request('selected_field', false);
        $output        = $resource::whereDate('updated_at', '>', $modifiedTime)->orderBy('updated_at',$sort);
        $output        = $this->theCriteria($output, request('criteria',false));

        $output->select($fields);

        return $output->paginate($pageSize);
    }

    /**
     * @param Builder $response
     * @param $criteria
     * @return Model
     */
    private function theCriteria(Builder $response, $criteria)
    {
        if(!$criteria){ return $response; }
        foreach ($criteria as $criterion)
        {
            $criterion['value'] = !isset($criterion['value']) || empty($criterion['value']) ? null : $criterion['value'];
            switch ($criterion['operator'])
            {
                case 'equals':
                    $response->where($criterion['field'], $criterion['value']);
                    break;
                case 'not_equals':
                    $response->where($criterion['field'], '!=', $criterion['value']);
                    break;
                case 'empty':
                    $response->whereNull($criterion['field']);
                    break;
                case 'not_empty':
                    $response->whereNotNull($criterion['field']);
                    break;
                case 'contains':
                    $response->where($criterion['field'], 'LIKE', '%'.$criterion['value'].'%');
                    break;
                case 'not_contains':
                    $response->where($criterion['field'], 'NOT LIKE', '%'.$criterion['value'].'%');
                    break;
                case 'starts_with':
                    $response->where($criterion['field'], 'LIKE', $criterion['value'].'%');
                    break;
                case 'ends_with':
                    $response->where($criterion['field'], 'LIKE', '%'.$criterion['value']);
                    break;
                case 'greater_than':
                    $response->where($criterion['field'], '>', $criterion['value']);
                    break;
                case 'less_than':
                    $response->where($criterion['field'], '<', $criterion['value']);
                    break;
                case 'date_equals':
                    $response->where($criterion['field'], Carbon::parse($criterion['value'])->toDateString());
                    break;
            }
        }
        return $response;
    }

    /**
     * Wrapper for the ApiResponse Helper
     *
     * @param string $code
     * @param array $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function ApiResponse($code, $response)
    {
        return ApiHelpers::ApiResponse($code, $response);
    }

    /**
     * @return mixed
     */
    protected function authJWT()
    {
        return JWTAuth::parseToken()->authenticate();
    }

    /**
     * @param $id
     * @return int
     */
    protected function usersWithProducts($id)
    {
        $registrations = Registration::all()->toArray();
        $array         = [];

        foreach ($registrations as $parser => $registration)
        {
            if($registration['parentCourseId'] == $id)
            {
                $array[] = $registration['userId'];
            }
        }

        return count($array);
    }


    /**
     * @param Model $resource
     * @param bool $attribute
     * @param $value
     * @return null
     */
    protected function join(Model $resource, $attribute = false, $value)
    {
        return $resource::where($attribute, $value)->get()->toArray();
    }

    /**
     * @param $time
     * @param $value
     * @return string
     */
    protected function timeCalculate($time, $value)
    {
        $current  = Carbon::now('America/New_York');
        $sub      = $current->$time($value);
        $output   = $sub.'.000000';

        return $output;
    }

    /**
     * @param Model $resource
     * @return bool
     */
    public function authResource(Model $resource)
    {
       // if ((new User())->isInstructor()) {
       //     return $resource::where('vendorId', Auth::user()->vendorId)->get();
       // }

        return $resource::where('contactId', Auth::user()->contactId)->get();

    }


    public function ClientHttpRequest($url, $route, $rest, $body)
    {
        try {
            $client = new Client([
                'base_uri' => $url,
                'timeout'  => 100.0
            ]);
            
            if (!empty($body))
            {
                $response = $client->request($rest, $route, $body);
                
                $contents = (string) $response->getBody();
                $data = json_decode($contents);
                
                return $data;

            }else
            {
                $response = $client->request($rest, $route);
                $contents = (string) $response->getBody();
                $data = json_decode($contents);
                
                return $data;
            }

        } catch (Exception $e) {
            return $this->ApiResponse(2, $e->getMessage());
        }
    }

    /**
     * Service Key Client Accounts
     * @return $code 
     */
    public function GetKeyAccountConfig()
    {
        return [
            'key' => $key = Config::get('Account.secret_key'),
            'id'  => $id = Config::get('Account.id_subscription'),
        ];
    }
}