<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Entities\Registration;
use App\Entities\User;
use App\Entities\Products;
use App\Entities\SalesOrders;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Transformers\ModulesTransformer;
use App\Transformers\RegistrationsTransformer;
use App\Http\Requests\Registrations\ShowRequest;
use App\Http\Requests\Registrations\IndexRequest;
use App\Http\Requests\Registrations\StoreRequest;
use App\Http\Requests\Registrations\UpdateRequest;
use App\Http\Requests\Registrations\DestroyRequest;
use App\Http\Requests\Registrations\InscriptionRequest;

class RegistrationController extends ApiController
{

    /**
     * @var ModulesTransformer
     */
    protected $registrationsTransformer;

    /**
     * @var Zoho
     */
    protected $zoho;

    /**
     * RegistrationsTransformer constructor.
     * @param RegistrationsTransformer $registrationsTransformer
     * @param Zoho $zoho
     */
    public function __construct(RegistrationsTransformer $registrationsTransformer, Zoho $zoho)
    {
        $this->registrationsTransformer   = $registrationsTransformer;
        $this->zoho                       = $zoho;
    }

    /**
     * Display a listing of the resource.
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {   
        $registration = DB::table('registrations')
        ->join('products', 'products.id', 'registrations.parentCourseId')
        ->select('registrations.id', 'registrations.parentCourseId', 'products.productName', 'registrations.start_at', 'registrations.ends_at', 'registrations.duration_lic',
        'registrations.status', 'products.cover', 'products.vendorId', 'products.unitPrice', 'products.description',
        'products.category', 'products.duration')
        ->whereDate('ends_at', '>=', Carbon::now('utc')->toDateTimeString())
        ->where('userId', '=', Auth::user()->id)
        ->where('status', '=', true)
        ->get();

        return $this->ApiResponse(200, $registration);
    }

    /**
     * @param $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
        $product = Registration::findOrFail($id);

        return $this->ApiResponse(200,
            $this->registrationsTransformer->transform($product));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  UpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $registration = Registration::findOrFail($id);

        $resource_data = $request->onlyWith([
            'date','inscriptionName','owner','createdBy','modifiedBy','createdTime',
            'modifiedTime','lastActivity','currency','exchangeRate','parentCourseId','parentCourse',
            'userId','user', 'chargeClientId','payVendorId', 'start_at', 'ends_at', 'duration_lic', 'status'
        ]);

        $registration->fill($resource_data);
        $registration->save();

        return $this->ApiResponse(200, $registration['status']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        Registration::findOrFail($id)->delete();

        return $this->ApiResponse(200, null);
    }

    public function InscriptionCourse(InscriptionRequest $request)
    {
        $client = User::where('email', '=', $request->email)->first();
        $product = Products::where('id', '=', $request->productId)->first();


        $sale = new SalesOrders();
        $sales_order = [
            'soNumber'              =>  $product->id,
            'subject'               =>  'Inscripción en el curso '.$product->productName,
            'contactId'             =>  $client->contactId,
            'contactName'           =>  $client->name.' '.$client->lastName,
            'vendorId'              =>  $product->vendorId,
            'instructor'            =>  $product->vendorName,
            'owner'                 =>  'Elearning Nemobile',
            'status'                =>  'Creada',
            'subTotal'              =>  $request->amount,
            'tax'                   =>  '0',
            'adjustment'            =>  '0',
            'grandTotal'            =>  $request->amount,
            'bank'                  =>  $request->method,
            'codeAdvance'           =>  $request->code,
            'typeAdvance'           =>  $request->methodPay
        ];

        $sales_order['productDetails'] = [

            1 => [

                '@type'         => 'product',
                'Product Id'    =>  $product->id,
                'Unit Price'    =>  $product->unitPrice,
                'Quantity'      =>  '1',
                'Total'         =>  '1',
                'List Price'    =>  '0',
                'Net Total'     =>  $product->unitPrice
            ]
        ];

        $sale->fill($sales_order);
        $sale->save();

        if($request->duration == 0) { // if not send duration
            $duration = Carbon::now('utc')->addYear(12);
        
        } else { // if send duration
            $duration = Carbon::now('utc')->addDay($request->duration);
        }

        $register = [
            'wfTrigger'       => 'true',
            'date'            => Carbon::now(),
            'inscriptionName' => $product->productName,
            'userId'          => $client->id,
            'user'            => $client->name.' '.$client->lastName,
            'parentCourseId'  => $product->id,
            'parentCourse'    => $product->productName,
            'chargeClientId'  => $sale->id,
            'owner'           => 'Elearning Nemobile',
            'createdBy'       => 'Elearning Nemobile',
            'createdTime'     => Carbon::now('utc'),
            'currency'        => 'USD',
            'start_at'        => Carbon::now('utc'),
            'ends_at'         => $duration,
            'duration_lic'    => $request->duration,
            'status'          => true,
        ];

        $registration = new Registration();
        $registration->fill($register);
        $registration->save();

        return $this->ApiResponse(200, 'El usuario ha sido inscrito satisfactoriamente');
    }

    public function CourseNotEnrolledByUser(Request $request, $userid)
    {
        $query = DB::table('products')
                    ->join('registrations', 'products.id', 'registrations.parentCourseId')
                    ->selectRaw(DB::raw('products.id AS productId, products.productName, products.unitPrice, products.duration_lic AS duration_default'))
                    ->where('registrations.userId', '!=', $userid)
                    ->where('products.statusApproved','Aprobado')
                    ->where('products.courseProgress','En producción')
                    ->where('productCategory','Cursos Online')
                    ->groupBy('products.id')->get();

        return $this->ApiResponse(200, $query);
    }

    public function CourseEnrolledByUser(Request $request, $userid)
    {
        $query = DB::table('registrations')
            ->select('id', 'parentCourseId', 'inscriptionName', 'start_at', 'ends_at', 'duration_lic', 'status')
            ->whereDate('ends_at', '>=', Carbon::now('utc')->toDateTimeString())
            ->where('userId', '=', $userid)
            ->where('status', '=', true)
            ->get();

        return $this->ApiResponse(200, $query);
    }

    public function CourseEnrolledByCourse(Request $request, $courseid)
    {
        $user = Auth::user()->level;
        
        if ($user == 'Administrador')
        {
            $query = DB::table('registrations')
            ->join('users', 'users.id', 'registrations.userId')
            ->select('registrations.id', 'registrations.parentCourseId', 'registrations.inscriptionName', 
            'registrations.start_at', 'registrations.ends_at', 'registrations.duration_lic', 
            'registrations.status', 'registrations.userId', 'users.name', 'users.lastName', 'users.email')
            ->whereDate('registrations.ends_at', '>=', Carbon::now('utc')->toDateTimeString())
            ->where('registrations.parentCourseId', '=', $courseid)
            ->where('registrations.status', '=', true)
            ->where('registrations.userId', '!=', Auth::user()->id)
            ->get();
        }else
        {
            $query = DB::table('registrations')
            ->join('users', 'users.id', 'registrations.userId')
            ->select('registrations.id', 'registrations.parentCourseId', 'registrations.inscriptionName', 
            'registrations.start_at', 'registrations.ends_at', 'registrations.duration_lic', 
            'registrations.status', 'registrations.userId', 'users.name', 'users.lastName', 'users.email')
            ->whereDate('registrations.ends_at', '>=', Carbon::now('utc')->toDateTimeString())
            ->where('registrations.parentCourseId', '=', $courseid)
            ->where('registrations.status', '=', true)
            ->where('registrations.userId', '!=', Auth::user()->id)
            ->get();
        }

        return $this->ApiResponse(200, $query);
    }

    public function GetInscriptions()
    {
        if(Auth()->check() && Auth::user()->level == 'Administrador')
        {   
            $query = DB::table('registrations')
            ->join('users', 'users.id', 'registrations.userId')
            ->join('products', 'products.id', 'registrations.parentCourseId')
            ->join('sales', 'sales.id', 'registrations.chargeClientId')
            ->select('registrations.id', 'registrations.parentCourseId', 'products.productName',
            'registrations.start_at', 'registrations.ends_at', 'registrations.duration_lic', 
            'registrations.status', 'registrations.userId', 'users.name', 'users.lastName', 'users.email',
            'sales.grandTotal', 'sales.bank', 'sales.typeAdvance')
            ->whereDate('registrations.ends_at', '>=', Carbon::now('utc')->toDateTimeString())
            ->where('registrations.status', '=', true)
            ->get();

            return $this->ApiResponse(200, $query);
            
        }elseif(Auth()->check() && Auth::user()->level == 'Instructor')
        {   
            $query = DB::table('registrations')
            ->join('users', 'users.id', 'registrations.userId')
            ->join('products', 'products.id', 'registrations.parentCourseId')
            ->join('sales', 'sales.id', 'registrations.chargeClientId')
            ->select('registrations.id', 'registrations.parentCourseId', 'products.productName',
            'registrations.start_at', 'registrations.ends_at', 'registrations.duration_lic', 
            'registrations.status', 'registrations.userId', 'users.name', 'users.lastName', 'users.email',
            'sales.grandTotal', 'sales.bank', 'sales.typeAdvance')
            ->whereDate('registrations.ends_at', '>=', Carbon::now('utc')->toDateTimeString())
            ->where('registrations.status', '=', true)
            ->where('products.vendorId', '=', Auth::user()->vendorId)
            ->get();

            return $this->ApiResponse(200, $query);
        }
    }
}