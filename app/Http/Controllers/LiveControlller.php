<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Entities\Live;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Transformers\LivesTransformer;
use App\Http\Requests\Live\IndexRequest;
use App\Http\Requests\Live\StoreRequest;
use App\Http\Requests\Live\UpdateRequest;

class LiveControlller extends ApiController
{
    /**
     * @var LiveTransformer
     */
    protected $liveTransformer;

    /**
     * LeadsController constructor.
     * @param LeadsTransformer $leadsTransformer
     */
    public function __construct(LivesTransformer $liveTransformer)
    {
        $this->liveTransformer = $liveTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $event = DB::table('lives') 
        ->join('products', 'products.id', 'lives.product_id')
        ->join('lessons', 'lessons.parentCourseId', 'lives.product_id')
        ->join('users', 'users.id', 'lives.user_id')
        ->select(DB::raw('products.productName AS course, lessons.LessonName AS lesson'),
            'lives.start_at', 'lives.ends_at', 'lives.resizable', 'lives.duration_hours',
            DB::raw('products.vendorId AS teacher_id,
            products.vendorName AS teacher, lives.id AS student_id', 'users.name'))
        ->whereDate('ends_at', '>=', Carbon::now('utc')->toDateTimeString())
        ->get();

        return $this->ApiResponse(200, $event);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
      $resource_data = $request->onlyWith([
        'name', 'start_at', 'ends_at', 'draggble',
        'rezisable', 'product_id', 'lesson_id', 'user_id', 'duration_hours','channel'
      ]);
        
      $live = new Live();
      $live->fill($resource_data);
      $live->save();

      return $this->ApiResponse(201, $live);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $live = Live::findOrFail($id);

        return $this->ApiResponse(200, $this->liveTransformer->transform($live));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $live = Live::findOrFail($id);

        $resource_data = $request->onlyWith([
            'name', 'start_at', 'ends_at', 'draggble',
            'rezisable', 'product_id', 'lesson_id', 'user_id', 'duration_hours','channel'
          ]);
            
          $live->fill($resource_data);
          $live->save();
    
          return $this->ApiResponse(200, $live);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Live::findOrFail($id)->delete();

        return $this->ApiResponse(200, 'El evento ha sido eliminado');
    }

    public function nextInstructorClasses(){

        $event = DB::table('lives') 
        ->join('products', 'products.id', 'lives.product_id')
        ->join('lessons', 'lessons.parentCourseId', 'lives.product_id')
        ->join('users', 'users.id', 'lives.user_id')
        ->select(DB::raw('products.productName AS course, lessons.LessonName AS lesson'),
            'lives.start_at', 'lives.ends_at', 'lives.resizable',
            DB::raw('products.vendorId AS teacher_id,
            products.vendorName AS teacher, lives.id AS student_id', 'users.name'))
        ->whereDate('ends_at', '>=', Carbon::now('utc')->toDateTimeString())
        ->get();

        return $this->ApiResponse(200, $event);
    }
}
