<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Entities\Modules;
use App\Entities\Products;
use Illuminate\Http\Request;
use App\Transformers\ModulesTransformer;
use App\Http\Requests\Modules\ShowRequest;
use App\Http\Requests\Modules\IndexRequest;
use App\Http\Requests\Modules\StoreRequest;
use App\Http\Requests\Modules\UpdateRequest;
use App\Http\Requests\Modules\DestroyRequest;


class ModulesController extends ApiController
{

    /**
     * @var ModulesTransformer
     */
    protected $modulesTransformer;

    /**
     * ModulesController constructor.
     * @param ModulesTransformer $modulesTransformer
     */
    public function __construct(ModulesTransformer $modulesTransformer)
    {
        $this->modulesTransformer   = $modulesTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $modules = $this->paginateResource($request, new Modules(), [
            'id','moduleName','moduleOwner','order','createdBy','modifiedBy','createdTime',
            'modifiedTime','lastActivity','parentCourseId','parentCourse','description','currency','exchangeRate']);

        return $this->ApiResponse(200, $modules);
    }

    /**
     * @param $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
        $product = Modules::findOrFail($id);

        return $this->ApiResponse(200,
            $this->modulesTransformer->transform($product));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $resource_data = $request->onlyWith([
            'moduleName','moduleOwner','order','createdBy','modifiedBy','createdTime',
            'modifiedTime','lastActivity','parentCourseId','parentCourse','description','currency','exchangeRate'
        ]);
        
        $product = new Modules();
        $product->fill($resource_data);
        $product->save();

        return $this->ApiResponse(201, $product->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  UpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $product = Modules::findOrFail($id);

        $resource_data = $request->onlyWith([
            'moduleName','moduleOwner','order','createdBy','modifiedBy','createdTime',
            'modifiedTime','lastActivity','parentCourseId','parentCourse','description','currency','exchangeRate'
        ]);

        $product->fill($resource_data);
        $product->save();

        return $this->ApiResponse(200, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        Modules::findOrFail($id)->delete();
        
        return $this->ApiResponse(200, null);
    }


}
