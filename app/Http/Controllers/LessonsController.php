<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Entities\File;
use App\Entities\Lessons;
use App\Entities\Advances;
use App\Entities\Products;
use Illuminate\Http\Request;
use App\Transformers\LessonsTransformer;
use App\Http\Requests\Lessons\ShowRequest;
use App\Http\Requests\Lessons\IndexRequest;
use App\Http\Requests\Lessons\StoreRequest;
use App\Http\Requests\Lessons\UpdateRequest;
use App\Http\Requests\Lessons\DestroyRequest;
use Mavinoo\LaravelBatch\LaravelBatchFacade as Batch;

class LessonsController extends ApiController
{

    /**
     * @var LessonsTransformer
     */
    protected $lessonsTransformer;

    /**
     * LessonsController constructor.
     * @param LessonsTransformer $lessonsTransformer
     */
    public function __construct(LessonsTransformer $lessonsTransformer)
    {
        $this->lessonsTransformer  = $lessonsTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $lessons = $this->paginateResource($request, new Lessons(), [
            'id','duration','resourceType','lessonName','lessonOwner','order','createdBy','modifiedBy','createdTime',
            'modifiedTime','lastActivity','resource','parentModuleId','parentModule','parentCourseId','parentCourse','description', 'file_id']);

        return $this->ApiResponse(200, $lessons);
    }

    /**
     * @param $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
        $product = Lessons::findOrFail($id);

        return $this->ApiResponse(200,
            $this->lessonsTransformer->transform($product));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $resource_data = $request->onlyWith([
            'duration','order','resourceType','lessonName','lessonOwner','createdBy','modifiedBy','createdTime',
            'modifiedTime','lastActivity','resource','parentModuleId','parentModule','parentCourseId','parentCourse',
            'description', 'file_id', 'path_resource', 'type_resource'
        ]);

        $product = new Lessons();
        $product->fill($resource_data);
        $product->save();
        
        return $this->AssignOrUpdateFile($request->file_id, $product);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  UpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $product = Lessons::findOrFail($id);

        $resource_data = $request->onlyWith([
            'duration','resourceType','lessonName','lessonOwner','createdBy','modifiedBy','createdTime',
            'modifiedTime','lastActivity','resource','parentModuleId','parentModule','parentCourseId','parentCourse',
            'description','order', 'file_id'
        ]);
        
        $product->fill($resource_data);
        $product->save();
        
       return $this->AssignOrUpdateFile($request->file_id, $product);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        Lessons::findOrFail($id)->delete();

        return $this->ApiResponse(200, null);
    }

    public function CheckOrder(Request $request)
    {
        $value = $request->all();
        $table = 'lessons';
        $index = 'id';

        $status = Batch::update($table, $value, $index);
        
        if($status == true)
        {
            return $this->ApiResponse(200, 'Las lecciones han sido ordenadas');
        }else
        {
            return $this->ApiResponse(400, 'Ha ocurrido un error durante la operacion');
        }
    }

    public function AssignOrUpdateFile($file_id, $model)
    {
         $files = File::find($file_id);
        
         if (empty($files))
         {
            $model->save();
            return $this->ApiResponse(200, $model);
         }

         $resource_file = [
            'lesson_id' => $model['id'],
         ];

         $files->fill($resource_file);
         $files->save();

         $model['file_id'] = $files['id'];
         $model['type_resource'] = $files['type_resource'];
         $model['path_resource'] = $files['path_resource'];
         $model->save();

         return $this->ApiResponse(200, $model);
    }

}

