<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use Carbon\Carbon;
use App\Entities\User;
use App\Entities\Vendor;
use App\Entities\Contact;
use App\Entities\Advances;
use App\Entities\Products;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use App\Entities\SalesOrders;
use App\Entities\Registration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Transformers\UsersTransformer;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\Users\IndexRequest;
use App\Http\Requests\Users\StoreRequest;
use App\Http\Requests\Users\UpdateRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Users\InscriptionRequest;
use App\Http\Requests\Users\UpdateRequestPassword;
use App\Http\Requests\Users\StoreRequestForVendors;


class UsersController extends ApiController
{
    
    /**
     * @var Zoho
     */
    protected $zoho;

    /**
     * @var UsersTransformer
     */
    protected $userTransformer;

    /**
     * UsersController constructor.
     * @param UsersTransformer $userTransformer
     */
    public function __construct(UsersTransformer $userTransformer, Zoho $zoho)
    {
        $this->userTransformer = $userTransformer;
        $this->zoho = $zoho;
        $this->middleware('VerifyAuthToken')->only('update');

    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $users = $this->paginateResource($request, new User(), [
            'id', 'name', 'lastName', 'email', 'contactId', 'vendorId', 'level', 'password', 'status']);

        return $this->ApiResponse(200, $users);
        
    }

    /**
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($user)
    {
        $user = User::findOrFail($user);

        return $this->ApiResponse(200,
            $this->userTransformer->transform($user));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {

    }

    /**
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateZoho(UpdateRequest $request)
    {
        $user = User::findOrFail(Auth::user()->id);

        $resource_data = $request->onlyWith([
            'name', 'lastName', 'email', 'contactId', 'vendorId', 'level', 'password'
        ]);

        $user->fill($resource_data);
        $user->save();

        return $this->ApiResponse(200, null);

    }


    /**
     * @param $vendorId
     * @param array $set
     * @return mixed
     */
    private function changeEmailProducts($vendorId, $set = [])
    {
        $products = Products::where('vendorId', '=', $vendorId)->get()->toArray();
        $product = Product::findOrFail($products[0]->id);

        foreach ($products as $parser => $get)
        {
            $products[$parser]['instructorEmail'] = $set['Email'];
            $products[$parser]['vendorName'] = $set['First Name'] ." ". $set['Last Name'];

            $product->save();
            $this->zoho->update('Products', $get['id'], ['Email del instructor' =>  $set['Email']]);
        }

        return $products;

    }

    /**

     * @return \Illuminate\Http\JsonResponse
     */
    public function become()
    {
        $user = User::findOrFail(Auth::user()->id);
        $contact = Contact::findOrFail($user->contactId);

        $instructor = $user['name'] . " " . $user['lastName'];

        $vendor = new Vendor();

        $new_vendor = [
            'vendorOwner'    => 'The Biz Nation',
            'vendorName'     => $instructor,
            'nameInstructor' => $instructor,
            'email'          => $user->email,
        ];
       
        $vendor->fill($new_vendor);
        $vendor->save();

        $user->vendorId = $vendor->id;
        $user->level    = 'Instructor';
        $user->save();
        
        $vendor->save();

        $contact->vendorId = $vendor->id;
        $contact->type = 'Instructor';
        $contact->save();
        
        return $this->ApiResponse(200,
            $this->userTransformer->transform($user));    
    }


    /**
     * @param UpdateRequestPassword $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postReset(UpdateRequestPassword $request)
    {
        $user = User::findOrFail(Auth::user()->id);

        $credentials = $request->only(
            'password', 'password_confirmation'
        );

        if ($credentials['password'] === $credentials['password_confirmation'])
        {
            $credentials['password'] = bcrypt($credentials['password']);
            $user->fill($credentials);
            $user->save();

            return $this->ApiResponse(200, 'La contraseña ha sido cambiada exitosamente');

        }else
        {
            return $this->ApiResponse(400, 'Las contraseña no coinciden');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function forget(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $verify = User::where('email', $request->input('email'))->get(['id', 'contactId'])->toArray();
        #dd(!$verify);
        if (!empty($verify))
        {
            $user = User::findOrFail($verify[0]['id']);
            $user->tokenPassword = str_random(64);
            $user->dateExpired = Carbon::now();
            
            $token = $user->tokenPassword = str_random(64);
            $mail = $request->email;

            Mail::to($mail)->send(new ResetPassword($user));
            
            $user->save();

            return $this->ApiResponse(200, 'Ha sido enviado un correo de re-establecimiento de contraseña tu buzon');
        }
        else{

        }

        return $this->ApiResponse(400, ['error' => 'Invalid request']);

    }

    /**
     * @param bool $token
     * @param UpdateRequestPassword $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function forgetWithToken($token = false, UpdateRequestPassword $request)
    {
        if ($this->verifyToken($token))
        {
            return $this->postResetToken($request, $this->verifyToken($token));
        }

        return $this->ApiResponse(400, ['error' => 'Token expired']);
    }

    /**
     * @param $token
     * @return mixed
     */
    private function verifyToken($token)
    {
        $user = User::where('tokenPassword','=', $token)->get()->toArray();
        return  !empty($user) ? $user[0]['id'] : null;
    }


    /**
     * @param UpdateRequestPassword $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function postResetToken(UpdateRequestPassword $request, $id)
    {
        $user = User::where('email', '=', $request->email)->first();

        $credentials = [
            'password' => $request->password
        ];

        $credentials['password'] = bcrypt($credentials['password']);
        
        if($user['dateExpired'] >= Carbon::now()->addMinutes(30))
        {
            $user->update(['tokenPassword' => null]);
        }

        $user->fill($credentials);
        $user->save();

        return $this->ApiResponse(200, $this->userTransformer->transform($user->fresh()));
    }

    /**
     * @param $contactId
     * @param $vendorId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAccount($contactId = false, $vendorId = false)
    {
        User::findOrFail(Auth::user()->id)->delete();

        return $this->ApiResponse(200, null);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'email_confirmation' =>  'required|email'
        ]);

        $user = User::findOrFail(Auth::user()->id);
        $vendor = Vendor::where('email', '=', Auth::user()->email)->first();
        $contact = Contact::where('email', '=', Auth::user()->email)->first();

        $user->email =  $request->input('email');
        $vendor->email = $request->input('email');
        $contact->email = $request->input('email');

        $user->save();
        $vendor->save();
        $contact->save();

        return $this->ApiResponse(200, null);

    }

    public function CreateUsers(RegisterRequest $request)
    {
        $data = $request;
        $users = $this->CountClient();
        
		$resource_contact = Contact::select('id')
			->where('email', '=', $data->email)->first();
		$resource_vendor = Vendor::select('id')
			->where('email', '=', $data->email)->first();

		if ($data->level == 'Alumno') {
            return $message = $this->CreateStudent($data, $resource_contact);
        }elseif ($data->level == 'Instructor' || $data->level == 'Administrador' && ($users['instructor'] + $users['admin'] <= 2))
        {
            return $message = $this->CreateInstructorOrAdmin($data);
        }
        else{
            return $this->ApiResponse(400, 'Oops, este email ya se encuentra registrado o alcanzado el limite de usuarios permitidos de un mismo tipo');
        }
        
        
    }

    public function CreateStudent($data, $resource_contact) 
	{
		if (!empty($resource_contact)) {

			$new_user = [
				'email' => $data->email,
				'password' => bcrypt($data->password),
				'name' => $data->name,
				'lastName' => $data->lastName,
				'level' => $data->level,
				'contactId' => $resource_contact->id,
                'vendorId' => null,
                'status' => true
			];

			$user = new User();
			$user->fill($new_user);

			$user->save();

			return $this->ApiResponse(200, [
				'token' => $token,
				'refresh_ttl' => config('jwt.refresh_ttl'),
				'user' => $this->userTransformer->transform($user->fresh()),
			]);

		} elseif (empty($resource_contact)) {
			$contact = new Contact();

			$new_contact = [
				'name' => $data->name,
				'lastName' => $data->lastName,
				'contactOwner' => 'Elearning B&D',
				'type' => $data->level,
				'gender' => $data->gender,
				'dateOfBirth' => $data->dateOfBirth,
				'mailingCountry' => $data->mailingCountry,
				'phone' => $data->phone,
				'leadSource' => 'Registro Principal desde el Menu',
				'vendorId' => null,
			];

			$contact->fill($new_contact);
			$contact->save();

			$new_user = [
				'email' => $data->email,
				'password' => bcrypt($data->password),
				'name' => $data->name,
				'lastName' => $data->lastName,
				'level' => $data->level,
				'contactId' => $contact->id,
                'vendorId' => null,
                'status' => true
			];

			$user = new User();
			$user->fill($new_user);
			$user->save();

			return $this->ApiResponse(200, $user);

	} else {
			return $this->ApiResponse(409, 'Este email ya se encuentra registrado');
	}

}
    public function CreateInstructorOrAdmin($data) {

        $instructor = $data->name . ' ' . $data->lastName;

		$vendor = new Vendor();

		$new_vendor = [
				'vendorOwner' => 'Elearning B&D',
				'vendorName' => $instructor,
				'nameInstructor' => $instructor,
				'email' => $data->email,
				'age' => $data->age,
				'country' => $data->country,
				'gender' => $data->gender,
				'city' => $data->city,
				'website' => $data->website,
				'street' => $data->street,
				'state' => $data->state,
				'zipCode' => $data->zipCode,
				'description' => $data->description,
				'biography' => $data->biography,
				'littleDescription' => $data->littleDescription,
				'interest' => $data->interest,
				'socialOne' => $data->socialOne,
				'socialTwo' => $data->socialTwo,
				'socialThree' => $data->socialThree,
				'socialFour' => $data->socialFour,
				'socialFive' => $data->socialFive,
				'socialSix' => $data->socialSix,
				'vendorPicture' => $data->vendorPicture,
				'education' => $data->education,
				'phone' => $data->phone,
				'type' => $data->level,
			];

			$vendor->fill($new_vendor);
			$vendor->save();

			$contact = new Contact();
			$new_contact = [
				'name' => $data->name,
				'lastName' => $data->lastName,
				'contactOwner' => 'Elearning B&D',
				'type' => $data->level,
				'vendorName' => $instructor,
				'vendorId' => $vendor->id,
				'leadSource' => 'Registro Principal desde el Menu',
			];

			$contact->fill($new_contact);
			$contact->save();
			
			$new_user = [
				'email' => $data->email,
				'password' => bcrypt($data->password),
				'name' => $data->name,
				'lastName' => $data->lastName,
				'level' => $data->level,
				'vendorId' => $vendor->id,
				'level' => $data->level,
                'contactId' => $contact->id,
                'status' => true
			];

			$user = new User();
			$user->fill($new_user);
			$user->save();
					
			return $this->ApiResponse(200, $user);
		}
    
    public function CountClient()
    {
        return [
            'admin' => User::where('level', '=', 'Administrador')->count(),
            'instructor' => User::where('level', '=', 'Instructor')->count(),
        ];
    }

    public function DeleteUser(Request $request, $id)
    {
        $user = User::findOrFail($id);
        Products::where('vendorId', '=', $user->vendorId)->delete();
        Vendor::where('id', '=', $user->vendorId)->delete();
        Contact::where('id', '=', $user->contactId)->delete();
        Registration::where('userId', '=', $id)->delete();
        $user->delete();
        
        return $this->ApiResponse(200, 'El usuario ha sido eliminado satisfactoriamente');

    }

    public function UpdateUser(UpdateRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $courses = $this->CheckInscription($id);
        $users = $this->CountClient();

        $resource_data = $request->onlyWith([
            'name', 'lastName', 'email', 'contactId', 'vendorId', 'level', 'password', 'status'
        ]);
        
        $resource_data['password'] = bcrypt($request->password);

        if ($request->level == 'Instructor' || $request->level == 'Administrador' && ($users['instructor'] + $users['admin'] <= 2))
        {
            return $this->checkExists($user, $resource_data);

        }elseif($request->level == 'Alumno')
        {
            return $this->checkExists($user, $resource_data);

        }else
        {
            return $this->ApiResponse(400, 'Usted ha alcanzado el limite de Instructores de su licencia');
        }
    
        if ($request->status == 0 && $courses > 0)
        {
            return $this->ApiResponse(400, 'Oops, este no puede ser deshabilitado, posee subscripciones vigentes');

        }else
        {
            $user->fill($resource_data);
            $user->save();

            return $this->ApiResponse(200, $user);
        }

    }
    
    public function checkExists($user, $resource_data)
    {
        if (empty($user->vendorId))
        {
            $vendor = new Vendor();

		    $new_vendor = [
		    		'vendorOwner' => 'Elearning B&D',
		    		'vendorName' => $user->name.' '.$user->lastName,
		    		'nameInstructor' => $user->name.' '.$user->lastName,
		    		'email' => $user->email,
		    		'type' => $resource_data['level'],
		    	];

			    $vendor->fill($new_vendor);
                $vendor->save();
                
                $resource_data['vendorId'] = $vendor->id;
                $user->fill($resource_data);
                $user->save();

            return $this->ApiResponse(200, $user);

        }
        
        if(!empty($user->vendorId) && $resource_data['level'] == 'Alumno')
        {
            
            $vendor = Vendor::findOrFail($user->vendorId);
            $vendor->type = 'Alumno';
            $vendor->save();

            $user->fill($resource_data);
            $user->save();

            return $this->ApiResponse(200, $user);

        }
        
        if(!empty($user->vendorId) && $resource_data['level'] == 'Administrador' || $resource_data['level'] == 'Instructor')
        {
            $vendor = Vendor::findOrFail($user->vendorId);
            $vendor->type = 'Alumno';
            $vendor->save();
            
            $user->fill($resource_data);
            $user->save();

            return $this->ApiResponse(200, $user);
        }

        $user->fill($resource_data);
        $user->save();
    }

    public function CheckInscription($user)
    {
        return DB::table('registrations')
            ->where('userId', '=', $user)
            ->whereDate('ends_at', '>=', Carbon::now('utc')->toDateTimeString())
            ->where('status', '=', true)
            ->count();
    }
}
