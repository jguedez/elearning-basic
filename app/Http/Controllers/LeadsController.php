<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use App\Entities\Leads;
use Illuminate\Http\Request;
use App\Transformers\LeadsTransformer;
use App\Http\Requests\Leads\StoreRequest;
use App\Http\Requests\Leads\DestroyRequest;

class LeadsController extends ApiController
{

    /**
     * @var LeadsTransformer
     */
    protected $leadsTransformer;

    /**
     * @var Zoho
     */
    protected $zoho;

    /**
     * LeadsController constructor.
     * @param LeadsTransformer $leadsTransformer
     * @param Zoho $zoho
     */
    public function __construct(LeadsTransformer $leadsTransformer, Zoho $zoho)
    {
        $this->leadsTransformer = $leadsTransformer;
        $this->zoho = $zoho;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request, $id)
    {
        $leads = $this->paginateResource(null, new Leads(), [
            'id', 'name', 'leadOwner', 'lastName', 'email', 'leadSource', 'createdTime', 'modifiedTime', 'fullName',
            'description', 'country', 'lastActivity', 'key', 'Type']);

        return $this->ApiResponse(200, $leads);
    }

    /**
     * @param $lead
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, $id)
    {
        $lead = Leads::findOrFail($id);

        return $this->ApiResponse(200, $this->leadsTransformer->transform($lead));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        // Populating the user
        $resource_data = $request->onlyWith(['name', 'leadOwner', 'lastName', 'email', 'leadSource', 'createdTime', 'modifiedTime', 'fullName',
            'description', 'country', 'lastActivity', 'key', 'Type']);

        $lead = new Leads();
        $lead->fill($resource_data);
        $lead->save();

        return $this->ApiResponse(201, $lead[1]->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, $id)
    {
        Leads::findOrFail($id)->delete();
        
        return $this->ApiResponse(200, null);
    }



}
