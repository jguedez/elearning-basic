<?php
namespace App\Http\Controllers;

use URL;
use Session;
use Redirect;
use Carbon\Carbon;
use PayPal\Api\Item;

/** All Paypal Details class **/
use PayPal\Api\Payer;
use App\Entities\User;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\ItemList;
use App\Entities\Products;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use Illuminate\Http\Request;
use PayPal\Api\RedirectUrls;
use App\Entities\SalesOrders;
use App\Entities\Registration;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use PayPal\Auth\OAuthTokenCredential;
use App\Http\Requests\Stripe\StoreRequest;
use PayPal\Exception\PayPalConnectionException;
use CristianPontes\ZohoCRMClient\Exception\Exception;

class PaypalController extends ApiController
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    
    public function PaypalPay(StoreRequest $request)
    {
        $data = $request;
        $product = Products::where('id', '=', $data->productId)->first();
        $price = $product->unitPrice;
        
         if ($price == 0)
         {
            return $this->CourseFree($product, $price, $data);
        
         }elseif ($price > 0)
         {
            return $this->payWithpaypal($product, $price, $data);
        
         }else 
         {
             return $this->ApiResponse(400, 'Oops, ha ocurrido un error durante la operacion');
         }
    }

    public function payWithpaypal($product, $price, $data)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $item_1->setName($product->productName) /** item name **/
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($price); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($price);
    
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription($product->description);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl('https://elearning.nemobile.app/byd/api/execute/payment') /** Specify return URL **/
            ->setCancelUrl('https://elearning.nemobile.app/byd/api/cancel');

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
    
        try {
            $payment->create($this->_api_context);
            return response()->json(['id' => $payment->getId()]);

        } catch (Exception $e) {
             return $e->getMessage();
        }
    }

    public function getPaymentStatus(Request $request)
    {
        $product = $request->productId;
        $contact = $request->contactId;

        $paymentId = $request->paymentID;
        $payment = Payment::get($paymentId, $this->_api_context);

        $product = Products::where('id', '=', $request->productId)->first();
        $price = $product->unitPrice;

        $execution = new PaymentExecution();
        $execution->setPayerId($request->payerID);
        
        $transaction = new Transaction();
        $amount = new Amount();
    
        $amount->setCurrency('USD');
        $amount->setTotal($price);
        $transaction->setAmount($amount);
        
        $execution->addTransaction($transaction);
    
        try {
           $result = $payment->execute($execution, $this->_api_context);
    
            $this->InscriptionCourse($product, $contact);

        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode(); // Prints the Error Code
            echo $ex->getData(); // Prints the detailed error message 
            die($ex);
        } catch (Exception $ex) {
            die($ex);
        }
        
    }

    public function InscriptionCourse($product, $contact)
    {
        $user = User::where('contactId', '=', $contact)->first();

        $sale = new SalesOrders();
            $sales_order = [
                'soNumber'              =>  $product->id,
                'subject'               =>  'Inscripción automatica en el curso '.$product->productName,
                'contactId'             =>  $contact,
                'contactName'           =>  $user->name.' '.$user->lastName,
                'vendorId'              =>  $product->vendorId,
                'instructor'            =>  $product->vendorName,
                'owner'                 =>  'Elearning Nemobile',
                'status'                =>  'Creada',
                'subTotal'              =>  $product->unitPrice,
                'tax'                   =>  '0',
                'adjustment'            =>  '0',
                'grandTotal'            =>  $product->unitPrice,
                'bank'                  => 'Paypal',
                'codeAdvance'           =>  str_random(10),
                'typeAdvance'           => 'TDC'
            ];
    
            $sales_order['productDetails'] = [

                1 => [
            
                    '@type'         => 'product',
                    'Product Id'    =>  $product->id,
                    'Unit Price'    =>  $product->unitPrice,
                    'Quantity'      =>  '1',
                    'Total'         =>  '1',
                    'List Price'    =>  '0',
                    'Net Total'     =>  $product->unitPrice
                ]
            ];
    
            $sale->fill($sales_order);
            $sale->save();

            if(empty($request->duration)) { // if not send duration
                $duration = empty($product->duration_lic) ? Carbon::now('utc')->addYear(12) : Carbon::now('utc')->addDay($product->duration_lic);
            
            } else { // if send duration
                $duration = Carbon::now('utc')->addDay($request->duration);
            }

    
            $register = [
                'wfTrigger'       => 'true',
                'date'            => Carbon::now(),
                'inscriptionName' => $product->productName,
                'userId'          => $user->id,
                'user'            => $user->name.' '.$user->lastName,
                'parentCourseId'  => $product->id,
                'parentCourse'    => $product->productName,
                'chargeClientId'  => $sale->id,
                'owner'           => 'Elearning Nemobile',
                'createdBy'       => 'Elearning Nemobile',
                'createdTime'     => Carbon::now('utc'),
                'currency'        => 'USD',
                'start_at'        => Carbon::now('utc'),
                'ends_at'         => $duration,
                'duration_lic'    => $request->duration,
                'status'          => true,
            ];
    
            $registration = new Registration();
            $registration->fill($register);
            $registration->save();
    
            return $this->ApiResponse(200, 'El usuario ha sido inscrito satisfactoriamente');
    }

    public function CourseFree($product, $price, $data)
    {
        //Generate Sales Orders price 0.
        $sale = new SalesOrders();
        $sales_order = [
            'soNumber'              =>  $data->productId,
            'subject'               =>  'Inscripción automatica en el curso '.$product->productName,
            'contactId'             =>  Auth::user()->contactId,
            'contactName'           =>  Auth::user()->name.' '.Auth::user()->lastName,
            'vendorId'              =>  $product->vendorId,
            'instructor'            =>  $product->vendorName,
            'owner'                 =>  'Elearning Nemobile',
            'status'                =>  'Creada',
            'subTotal'              =>  '0',
            'tax'                   =>  '0',
            'adjustment'            =>  '0',
            'grandTotal'            =>  '0',
            'bank'                  => 'Elearning Nemobile',
            'codeAdvance'           => 'Free',
            'typeAdvance'           => 'Free'
        ];

        $sales_order['productDetails'] = [
            1 => [
        
                '@type'         => 'product',
                'Product Id'    =>  $data->productId,
                'Unit Price'    =>  '0',
                'Quantity'      =>  '0',
                'Total'         =>  '0',
                'List Price'    =>  '0',
                'Net Total'     =>  '0'
            ]
        ];

        $sale->fill($sales_order);
        $sale->save();

        $register = [
            'wfTrigger'       => 'true',
            'date'            => Carbon::now(),
            'inscriptionName' => $product->productName,
            'userId'          => Auth::user()->id,
            'user'            => Auth::user()->name.' '.Auth::user()->lastName,
            'parentCourseId'  => $product->id,
            'parentCourse'    => $product->productName,
            'chargeClientId'  => $sale->id,
            'owner'           => 'Elearning Nemobile',
            'createdBy'       => 'Elearning Nemobile',
            'createdTime'     => Carbon::now('utc'),
            'currency'        => 'USD',
            'start_at'        => Carbon::now('utc'),
            'ends_at'         => Carbon::now('utc')->addYear(12),
            'duration_lic'    => 0,
            'status'          => true,
        ];

        $registration = new Registration();
        $registration->fill($register);
        $registration->save();

    return $this->ApiResponse(200, 'El usuario ha sido inscrito satisfactoriamente');

    }

}