<?php

namespace App\Http\Controllers;

use App\Zoho\Zoho;
use App\Entities\Cases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Cases\ShowRequest;
use App\Http\Requests\Cases\IndexRequest;
use App\Http\Requests\Cases\StoreRequest;
use App\Http\Requests\Cases\UpdateRequest;
use App\Http\Requests\Cases\DestroyRequest;

class CasesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

         /**
     * @var CasessTransformer
     */
    protected $casesTransformer;

    /**
     * @var Zoho
     */
    protected $zoho;

    public function __construct(Zoho $zoho)
    {
        //$this->productsTransformer = $productsTransformer;
        $this->zoho = $zoho;
    }


    public function index(IndexRequest $request)
    {

        $cases = $this->paginateResource($request, new Cases(), [
            'id','addComment','subject','noOfComments','internalComments','email','createdBy','description',
            'status','createdTime','modifiedTime','reportedBy','modifiedBy','currency','caseReason','accountName','dealName'
            ,'productName','caseNumber','caseOrigin','priority','owner','relatedTo','solution','exchangeRate','phone','caseNumberText','type'
        ]);

        return $this->ApiResponse(200, $cases);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

        $resource_data= $request->onlyWith(['addComment','subject','noOfComments','internalComments','email','createdBy','description',
        'status','createdTime','modifiedTime','reportedBy','modifiedBy','currency','caseReason','accountName','dealName'
        ,'productName','caseNumber','caseOrigin','priority','owner','relatedTo','solution','exchangeRate','phone','caseNumberText','type']);

        $case = new Cases();
        $case->fill($resource_data);
        $case->relatedTo = Auth::user()->contactId;

        $case->save();

        return $this->ApiResponse(201, $response);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cases  $cases
     * @return \Illuminate\Http\Response
     */
    public function show(ShowRequest $request, $id)
    {
        $case = Cases::findOrFail($id);
    
        return $this->ApiResponse(200, $case);
 
    }

    public function getLastTest(){

        $data = Cases::where('relatedTo', Auth::user()->contactId)->orderBy('created_at','desc')->first()->toArray();
        
        return $this->ApiResponse(200, $data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cases  $cases
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request,$id)
    {
        $case = Cases::findOrFail($id);

        $resource_data= $request->onlyWith(['addComment','subject','noOfComments','internalComments','email','createdBy','description',
        'status','createdTime','modifiedTime','reportedBy','modifiedBy','currency','caseReason','accountName','dealName'
        ,'productName','caseNumber','caseOrigin','priority','owner','relatedTo','solution','exchangeRate','phone','caseNumberText','type']);

        $case->fill($resource_data);
        $case->save();

        return $this->ApiResponse(200, null);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cases  $cases
     * @return \Illuminate\Http\Response
     */
    public function destroy(DestroyRequest $request, $id)
    {
        Cases::findOrFail($id)->delete();

        return $this->ApiResponse(200, null);
    }
}
