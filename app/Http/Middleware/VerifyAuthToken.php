<?php

namespace App\Http\Middleware;

use App\Helpers\ApiHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class VerifyAuthToken
{
    /**
     * Handle an incoming request.
     * This middleware is used for validating the request
     * With the Authorization SHA logic
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try
        {
            if (! $user = JWTAuth::parseToken()->authenticate() )
            {
                return ApiHelpers::ApiResponse(200, null);
            }
        }
        catch (TokenExpiredException $e)
        {
            return ApiHelpers::ApiResponse(401, null);
        }
        catch (JWTException $e)
        {
            
            return ApiHelpers::ApiResponse(400, null);
        }

        // Login the user instance for global usage

        Auth::loginUsingId($user->id, false);

        return $next($request);
    }
}
