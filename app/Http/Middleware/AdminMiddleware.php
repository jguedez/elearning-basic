<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\ApiHelpers;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if (Auth()->check() && Auth::user()->level == 'Administrador')
        {
            return $next($request);
        }else
        {
            return $this->ApiResponse(403, null);
        }
        
    }

    protected function ApiResponse($code, $response)
    {
        return ApiHelpers::ApiResponse($code, $response);
    }
}
