<?php

namespace App\Http\Requests\Vendors;

use App\Entities\Vendor;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Vendor::class)->store($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'vendorName'        => 'required',
            'phone'             => 'sometimes|max:255',
            'email'             => 'sometimes|email|max:255|unique:vendors',
            'website'           => 'sometimes',
            'glAccount'         => 'sometimes',
            'category'          => 'sometimes',
            'vendorOwner'       => 'sometimes|max:60|min:6',
            'createdBy'         => 'sometimes',
            'modifiedBy'        => 'sometimes',
            'createdTime'       => 'sometimes',
            'modifiedTime'      => 'sometimes',
            'street'            => 'sometimes',
            'city'              => 'sometimes',
            'state'             => 'sometimes',
            'zipCode'           => 'sometimes',
            'country'           => 'sometimes',
            'description'       => 'sometimes',
            'type'              => 'sometimes',
            'biography'         => 'sometimes',
            'nameInstructor'    => 'sometimes',
            'littleDescription' => 'sometimes',
            'interest'          => 'sometimes',
            'age'               => 'sometimes',
            'gender'            => 'sometimes',
            'socialOne'         => 'sometimes',
            'socialTwo'         => 'sometimes',
            'socialThree'       => 'sometimes',
            'socialFour'        => 'sometimes',
            'socialFive'        => 'sometimes',
            'socialSix'         => 'sometimes',
            'vendorPicture'     => 'sometimes',
            'education'         => 'sometimes',
            'paypal'            => 'sometimes',
            'bank'              => 'sometimes',
            'birthday'          => 'sometimes',
            'experience'        => 'sometimes',
            'education'         => 'sometimes'
        ];

        return $rules;
    }
}
