<?php

namespace App\Http\Requests\Vendors;

use App\Entities\User;
use App\Entities\Vendor;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class ShowRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Vendor::class)->show($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
