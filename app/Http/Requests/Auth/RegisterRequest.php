<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;

class RegisterRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'email'     => 'required|email|unique:users|max:250',
            'password'  => 'required|min:6',
            'name'      => 'required|max:250',
            'lastName'  => 'required|max:250',
            'level'     => 'required',
            'age'       => 'sometimes',
            'country'   => 'sometimes',
            'gender'    => 'sometimes',
            'city'      => 'sometimes',
            'website'   => 'sometimes',
            'street'    => 'sometimes',
            'state'     => 'sometimes',
            'zipCode'   => 'sometimes',
            'description'       => 'sometimes',
            'biography'         => 'sometimes',
            'littleDescription' => 'sometimes',
            'interest'          => 'sometimes',
            'socialOne'         => 'sometimes',
            'socialTwo'         => 'sometimes',
            'socialThree'       => 'sometimes',
            'socialFour'        => 'sometimes',
            'socialFive'        => 'sometimes',
            'socialSix'         => 'sometimes',
            'vendorPicture'     => 'sometimes',
            'education'         => 'sometimes',
            'mailingCountry'    => 'sometimes',
            'birthday'          => 'sometimes',
        ];
    }
}