<?php

namespace App\Http\Requests\Registrations;

use App\Entities\Registration;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Registration::class)->store($level);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date'              => 'required|max:5000',
            'inscriptionName'   => 'required|max:5000',
            'owner'             => 'sometimes|nullable|max:22300',
            'createdBy'         => 'sometimes|nullable|max:22300',
            'createdTime'       => 'sometimes|nullable|max:22300',
            'modifiedTime'      => 'sometimes|nullable|max:22300',
            'lastActivity'      => 'sometimes|nullable|max:22300',
            'currency'          => 'sometimes|nullable|max:22300',
            'exchangeRate'      => 'sometimes|nullable|max:22300',
            'parentCourseId'    => 'sometimes|nullable|max:22300',
            'parentCourse'      => 'sometimes|nullable|max:22300',
            'userId'            => 'sometimes|nullable|max:22300',
            'user'              => 'sometimes|nullable|max:22300',
            'chargeClientId'    => 'sometimes|nullable|max:22300',
            'payVendorId'       => 'sometimes|nullable|max:22300',
            'start_at'          => 'sometimes|nullable',
            'ends_at'           => 'sometimes|nullable',
            'status'            => 'sometimes'
        ];

        return $rules;
    }
}