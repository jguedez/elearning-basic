<?php

namespace App\Http\Requests\Registrations;

use App\Entities\Registration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class InscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Registration::class)->store($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'     => 'required|email|max:250',
            'productId' => 'required',
            'method'    => 'required',
            'methodPay' => 'required',
            'amount'    => 'required',
            'duration'  => 'required', 
            'code'      => 'sometimes'
        ];
    }
}
