<?php

namespace App\Http\Requests\Registrations;

use App\Entities\Registration;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Registration::class)->view($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->indexRules();
    }
}
