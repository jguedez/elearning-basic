<?php

namespace App\Http\Requests\Modules;

use App\Entities\Modules;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ApiRequest;

class ShowRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Modules::class)->show($level);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
