<?php

namespace App\Http\Requests\Modules;

use App\Entities\Modules;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Modules::class)->store($level);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'moduleName'        => 'required|max:255',
            'moduleOwner'       => 'required|max:255',
            'order'             => 'sometimes|nullable|max:32000',
            'createdBy'         => 'sometimes|nullable|max:300',
            'modifiedBy'        => 'sometimes|nullable|max:300',
            'createdTime'       => 'sometimes|nullable|max:300',
            'modifiedTime'      => 'sometimes|nullable|max:300',
            'lastActivity'      => 'sometimes|nullable|max:300',
            'parentCourseId'    => 'sometimes|nullable|max:300',
            'parentCourse'      => 'sometimes|nullable|max:300',
            'productActive'     => 'sometimes|nullable|max:32000',
            'description'       => 'sometimes|nullable|max:32000',
            'currency'          => 'sometimes|nullable|max:32000',
            'exchangeRate'      => 'sometimes|nullable|max:32000'
        ];

        return $rules;
    }
}