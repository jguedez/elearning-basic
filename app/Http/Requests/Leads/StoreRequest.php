<?php

namespace App\Http\Requests\Leads;

use App\Entities\Leads;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;


class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Leads::class)->store($level);  
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'name'              => 'required|max:255',
            'leadOwner'         => 'required|max:255',
            'lastName'          => 'required|max:255',
            'email'             => 'required|email|max:255|unique:users',
            'leadSource'        => 'sometimes|max:255',
            'createdTime'       => 'sometimes|max:255',
            'modifiedTime'      => 'sometimes|max:255',
            'fullName'          => 'sometimes|max:255',
            'lastActivity'      => 'sometimes|max:255',
            'description'       => 'sometimes|max:700',
            'country'           => 'sometimes|max:255',
            'key'               => 'sometimes|max:255',
            'Type'              => 'sometimes|max:255',

        ];

        return $rules;
    }
}
