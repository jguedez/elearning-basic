<?php

namespace App\Http\Requests\Cases;

use App\Entities\Cases;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class ShowRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Cases::class)->show($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
