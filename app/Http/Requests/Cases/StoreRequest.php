<?php

namespace App\Http\Requests\Cases;

use App\Entities\Cases;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Cases::class)->store($level);
    }

    /**
     * Get the validation rules that apply to the request.
     
     * @return array
     */
    public function rules()
    {
        $rules = [
            'subject'               => 'required|max:32000',
            'status'                => 'required|max:32000',
            'caseOrigin'            => 'required|max:32000',
            'addComment'            => 'sometimes|nullable|max:32000',
            'noOfComments'          => 'sometimes|nullable|max:32000',
            'internalComments'      => 'sometimes|nullable|max:32000',
            'email'                 => 'sometimes|nullable|max:32000',
            'createdBy'             => 'sometimes|nullable|max:32000',
            'description'           => 'sometimes|nullable|max:32000',
            'createdTime'           => 'sometimes|nullable|max:32000',
            'modifiedTime'          => 'sometimes|nullable|max:32000',
            'reportedBy'            => 'sometimes|nullable|max:32000',
            'modifiedBy'            => 'sometimes|nullable|max:32000',
            'currency'              => 'sometimes|nullable|max:32000',
            'caseReason'            => 'sometimes|nullable|max:32000',
            'accountName'           => 'sometimes|nullable|max:32000',
            'dealName'              => 'sometimes|nullable|max:32000',
            'productName'           => 'sometimes|nullable|max:32000',
            'caseNumber'            => 'sometimes|nullable|max:32000',
            'priority'              => 'sometimes|nullable|max:32000',
            'owner'                 => 'sometimes|nullable|max:32000',
            'relatedTo'             => 'sometimes|nullable|max:32000',
            'solution'              => 'sometimes|nullable|max:32000',
            'exchangeRate'          => 'sometimes|nullable|max:32000',
            'phone'                 => 'sometimes|nullable|max:32000',
            'caseNumberText'        => 'sometimes|nullable|max:32000',
            'type'                  => 'sometimes|nullable|max:32000',
            
        ];

        return $rules;
    }
}