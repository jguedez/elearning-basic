<?php

namespace App\Http\Requests\SalesOrders;

use App\Entities\SalesOrders;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(SalesOrders::class)->update($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'soNumber'          => 'required|max:255',
            'subject'           => 'required|max:255',
            'status'            => 'required|max:255',
            'owner'             => 'required|max:255',
            'createdBy'         => 'sometimes|nullable|max:32000',
            'modifiedBy'        => 'sometimes|nullable|max:32000',
            'createdTime'       => 'sometimes|nullable|max:32000',
            'modifiedTime'      => 'sometimes|nullable|max:32000',
            'subTotal'          => 'sometimes|nullable|max:32000',
            'tax'               => 'sometimes|nullable|max:32000',
            'adjustment'        => 'sometimes|nullable|max:32000',
            'grandTotal'        => 'sometimes|nullable|max:32000',
            'productDetails'    => 'sometimes|nullable',
            'description'       => 'sometimes|nullable|max:32000',
            'discount'          => 'sometimes|nullable|max:32000',
            'currency'          => 'sometimes|nullable|max:32000',
            'exchangeRate'      => 'sometimes|nullable|max:32000',
            'advance'           => 'sometimes|nullable|max:32000',
            'bank'              => 'sometimes|nullable|max:32000',
            'codeAdvance'       => 'sometimes|nullable|max:32000',
            'typeAdvance'       => 'sometimes|nullable|max:32000',
            'dateAdvance'       => 'sometimes|nullable|max:32000',
            'mountAdvance'      => 'sometimes|nullable|max:32000',
            'debtAdvance'       => 'sometimes|nullable|max:32000',
            'percentageOfProfit'       => 'sometimes|nullable|max:32000',
            'purchaseOrders'        => 'sometimes|nullable|max:32000'
        ];

        return $rules;
    }
}