<?php

namespace App\Http\Requests\PurchaseOrders;

use App\Entities\PurchaseOrders;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class IndexRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(PurchaseOrders::class)->view($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->indexRules();
    }
}
