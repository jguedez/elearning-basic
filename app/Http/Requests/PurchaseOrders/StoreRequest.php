<?php

namespace App\Http\Requests\PurchaseOrders;

use App\Entities\PurchaseOrders;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(PurchaseOrders::class)->store($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id'                => 'sometimes|max:255',
            'subject'           => 'sometimes|max:255',
            'vendorId'          => 'sometimes|max:255',
            'vendorName'        => 'sometimes|nullable|max:32000',
            'contactId'         => 'sometimes|nullable|max:32000',
            'contactName'       => 'sometimes|nullable|max:32000',
            'poDate'            => 'sometimes|nullable|max:32000',
            'dueDate'           => 'sometimes|nullable|max:32000',
            'Carrier'           => 'sometimes|nullable|max:32000',
            'exciseDuty'        => 'sometimes|nullable|max:32000',
            'salesCommission'   => 'sometimes|nullable|max:32000',
            'status'            => 'sometimes|nullable|max:32000',
            'owner'             => 'sometimes|nullable|max:32000',
            'createdBy'         => 'sometimes|nullable|max:32000',
            'modifiedBy'        => 'sometimes|nullable|max:32000',
            'createdTime'       => 'sometimes|nullable|max:32000',
            'modifiedTime'      => 'sometimes|nullable|max:32000',
            'subTotal'          => 'sometimes|nullable|max:32000',
            'tax'               => 'sometimes|nullable|max:32000',
            'adjustment'        => 'sometimes|nullable|max:32000',
            'grandTotal'        => 'sometimes|nullable|max:32000',
            'billingStreet'     => 'sometimes|nullable|max:32000',
            'billingCity'       => 'sometimes|nullable|max:32000',
            'billingState'      => 'sometimes|nullable|max:32000',
            'billingCountry'    => 'sometimes|nullable|max:32000',
            'productDetails'    => 'sometimes|nullable|max:32000',
            'discount'          => 'sometimes|nullable|max:32000',
            'currency'          => 'sometimes|nullable|max:32000',
            'exchangeRate'      => 'sometimes|nullable|max:32000',
            'bank'              => 'sometimes|nullable|max:32000',
            'advance'          =>  'sometimes|nullable|max:32000',
            'dateAdvance'      =>  'sometimes|nullable|max:32000',
        ];

        return $rules;
    }
}