<?php

namespace App\Http\Requests\Stripe;

use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'token'          => 'sometimes|max:255',
            'productId'      => 'required',
            'lessonCourseId' => 'sometimes|max:225',
            'lessonCourse'   => 'sometimes|max:255',
            'moduleCourseId' => 'sometimes|max:255',
            'contactId'      => 'sometimes',
            'name'           => 'sometimes',
            'lastName'       => 'sometimes'
        ];
        return $rules;
    }
}
