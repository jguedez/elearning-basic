<?php

namespace App\Http\Requests\Contacts;

use App\Entities\Contact;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Contact::class)->update($level);  
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $contact_id = $this->route()->getParameter('contact');

        $rules = [

            'contactOwner'      => 'sometimes',
            'leadSource'        => 'sometimes|max:255',
            'name'              => 'sometimes',
            'lastName'          => 'sometimes',
            'accountId'         => 'sometimes',
            'accountName'       => 'sometimes',
            'email'             => 'sometimes|email|max:255|unique:contacts',$contact_id,
            'phone'             => 'sometimes',
            'createdBy'         => 'sometimes',
            'modifiedBy'        => 'sometimes',
            'createdTime'       => 'sometimes',
            'modifiedTime'      => 'sometimes',
            'fullName'          => 'sometimes',
            'mailingCountry'    => 'sometimes',
            'emailOptOut'       => 'sometimes',
            'lastActivity'      => 'sometimes',
            'tag'               => 'sometimes',
            'gender'            => 'sometimes',
            'dateOfBirth'       => 'sometimes',
            'type'              => 'sometimes|in:Alumno, Instructor, Administrador',
            'vendorId'          => 'sometimes',
            'vendorName'        => 'sometimes',
            'birthday'          => 'sometimes',
            'file_id'           => 'sometimes',
            'photo_resource'    => 'sometimes',     
        ];
        return $rules;
    }
}
