<?php

namespace App\Http\Requests\Contacts;

use App\Entities\Contact;
use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Contact::class)->store($level);  
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'contactOwner'      => 'sometimes',
            'leadSource'        => 'sometimes|max:255',
            'name'              => 'sometimes',
            'lastName'          => 'sometimes',
            'accountId'         => 'sometimes',
            'accountName'       => 'sometimes',
            'email'             => 'sometimes|email|max:255|unique:contacts',
            'phone'             => 'sometimes',
            'createdBy'         => 'sometimes',
            'modifiedBy'        => 'sometimes',
            'createdTime'       => 'sometimes',
            'modifiedTime'      => 'sometimes',
            'fullName'          => 'sometimes',
            'mailingCountry'    => 'sometimes',
            'emailOptOut'       => 'sometimes',
            'lastActivity'      => 'sometimes',
            'tag'               => 'sometimes',
            'gender'            => 'sometimes',
            'dateOfBirth'       => 'sometimes',
            'type'              => 'required',
            'vendorId'          => 'sometimes',
            'vendorName'        => 'sometimes',
            'birthday'          => 'sometimes',
            'file_id'           => 'sometimes', 
            'photo_resource'    => 'sometimes',
        ];

        return $rules;
    }
}
