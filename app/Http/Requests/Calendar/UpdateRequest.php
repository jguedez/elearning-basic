<?php

namespace App\Http\Requests\Calendar;

use App\Entities\Calendar;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $calendar_id = $this->route()->getParameter('calendar');

        $rules = [

            'crmId'                 => 'sometimes',
            'contactId'             => 'sometimes',
            'crmContactId'          => 'sometimes',
            'vendorId'              => 'sometimes',
            'crmVendorId'           => 'sometimes',
            'startDate'             => 'sometimes',
            'endDate'               => 'sometimes',
            'type'                  => 'sometimes',
            'frequency'             => 'sometimes',
            'rule'                  => 'sometimes',

        ];
        return $rules;
    }
}
