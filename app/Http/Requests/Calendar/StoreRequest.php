<?php

namespace App\Http\Requests\Calendar;

use App\Entities\Contact;
use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'contactOwner'      => 'sometimes',
            'leadSource'        => 'sometimes|max:255',
            'name'              => 'sometimes',
            'lastName'          => 'sometimes',
            'accountId'         => 'sometimes',
            'accountName'       => 'sometimes',
            'email'             => 'sometimes|email|max:255|unique:contacts',
            'phone'             => 'sometimes',
            'createdBy'         => 'sometimes',
            'modifiedBy'        => 'sometimes',
            'createdTime'       => 'sometimes',
            'modifiedTime'      => 'sometimes',
            'fullName'          => 'sometimes',
            'mailingCountry'    => 'sometimes',
            'emailOptOut'       => 'sometimes',
            'lastActivity'      => 'sometimes',
            'tag'               => 'sometimes',
            'gender'            => 'sometimes',
            'dateOfBirth'       => 'sometimes',
            'type'              => 'required',
            'vendorId'          => 'sometimes',
            'vendorName'        => 'sometimes'

        ];

        return $rules;
    }
}
