<?php

namespace App\Http\Requests\Lessons;

use App\Entities\Lessons;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Lessons::class)->update($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'duration'          => 'sometimes|nullable|max:300',
            'resourceType'      => 'sometimes|nullable|max:300',
            'lessonName'        => 'required|max:255',
            'lessonOwner'       => 'sometimes|max:255',
            'createdBy'         => 'sometimes|nullable|max:300',
            'modifiedBy'        => 'sometimes|nullable|max:300',
            'createdTime'       => 'sometimes|nullable|max:300',
            'modifiedTime'      => 'sometimes|nullable|max:300',
            'lastActivity'      => 'sometimes|nullable|max:300',
            'resource'          => 'sometimes|nullable|max:300',
            'parentModuleId'    => 'sometimes|nullable|max:300',
            'parentModule'      => 'sometimes|nullable|max:300',
            'parentCourseId'    => 'sometimes|nullable|max:300',
            'parentCourse'      => 'sometimes|nullable|max:300',
            'description'       => 'sometimes|nullable|max:3000',
            'order'             => 'sometimes|nullable|max:3000',
            'file_id'           => 'sometimes'
        ];

        return $rules;
    }
}