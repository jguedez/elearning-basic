<?php

namespace App\Http\Requests\Live;

use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required',
            'start_at'       => 'required', 
            'ends_at'        => 'required',
            'rezisable'      => 'sometimes', 
            'product_id'     => 'sometimes',
            'lesson_id'      => 'sometimes',
            'user_id'        => 'sometimes',
            'duration_hours' => 'sometimes'
        ];
    }
}
