<?php

namespace App\Http\Requests\Live;

use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required',
            'start_at'       => 'required', 
            'ends_at'        => 'draggble',
            'rezisable'      => 'sometimes', 
            'product_id'     => 'required',
            'lesson_id'      => 'sometimes',
            'user_id'        => 'required',
            'duration_hours' => 'required'
        ];
    }
}
