<?php

namespace App\Http\Requests\Products;

use App\Entities\Products;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Products::class)->store($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'productOwner'      => 'required|max:32000',
            'productName'       => 'required|max:32000',
            'productCode'       => 'required|max:32000',
            'order'             => 'sometimes|nullable',
            'statusApproved'    => 'sometimes|nullable',
            'courseProgress'    => 'sometimes|nullable',
            'productActive'     => 'sometimes|nullable|max:32000',
            'productCategory'   => 'sometimes|nullable|max:32000',
            'createdBy'         => 'sometimes|nullable|max:32000',
            'modifiedBy'        => 'sometimes|nullable|max:32000',
            'createdTime'       => 'sometimes|nullable|max:32000',
            'modifiedTime'      => 'sometimes|nullable|max:32000',
            'unitPrice'         => 'sometimes|nullable|max:32000',
            'commissionRate'    => 'sometimes|nullable|max:32000',
            'usageUnit'         => 'sometimes|nullable|max:32000',
            'qtyOrdered'        => 'sometimes|nullable|max:32000',
            'qtyInStock'        => 'sometimes|nullable|max:32000',
            'reorderLevel'      => 'sometimes|nullable|max:32000',
            'qtyInDemand'       => 'sometimes|nullable|max:32000',
            'description'       => 'sometimes|nullable|max:32000',
            'taxable'           => 'sometimes|nullable|max:32000',
            'previous'          => 'sometimes|nullable',
            'vendorId'          => 'required|numeric',
            'vendorName'        => 'sometimes|max:225',
            'link'              => 'sometimes|nullable|max:32000',
            'image'             => 'sometimes|nullable|max:32000',
            'level'             => 'sometimes|nullable|max:32000',
            'video'             => 'sometimes|nullable|max:32000',
            'subCategory'       => 'sometimes|nullable|max:32000',
            'category'          => 'sometimes|nullable|max:32000',
            'audience'          => 'sometimes|nullable|max:2000',
            'subTitle'          => 'sometimes|nullable|max:32000',
            'duration'          => 'sometimes|nullable|max:32000',
            'location'          => 'sometimes|nullable|max:32000',
            'language'          => 'sometimes|nullable|max:32000',
            'document'          => 'sometimes|nullable|max:99999',
            'startCourse'       => 'sometimes|nullable|max:32000',
            'descriptionCourse' => 'sometimes|nullable|max:32000',
            'objectivesCourse'  => 'sometimes|nullable|max:32000',
            'littleDescription' => 'sometimes|nullable|max:32000',
            'instructorEmail'   => 'sometimes|nullable|max:32000',
            'instructorWeb'     => 'sometimes|nullable|max:32000',
            'instructorName'    => 'sometimes|nullable|max:32000',
            'socialOne'         => 'sometimes|nullable|max:32000',
            'biographyInstructor' => 'sometimes|nullable|max:5000',
            'socialTwo'         => 'sometimes|nullable|max:32000',
            'phoneInstructor'   => 'sometimes|nullable|max:32000',
            'socialFour'        => 'sometimes|nullable|max:32000',
            'socialThree'       => 'sometimes|nullable|max:32000',
            'photoInstructor'   => 'sometimes|nullable|max:32000',
            'education'         => 'sometimes|nullable|max:32000',
            'deltaIncome'       => 'sometimes|nullable|max:32000',
            'cover'             => 'sometimes|nullable|max:32000',
            'linkVideo'         => 'sometimes|nullable|max:32000',
            'signedUp'          => 'sometimes|nullable|max:32000',
            'duration_lic'      => 'required',
            'aditionalInstructors' => 'sometimes|nullable|max:32000',
            'file_id'           => 'sometimes', 
            'photo_cover'       => 'sometimes',
            'live_hours'        => 'sometimes'
        ];

        return $rules;
    }
}