<?php

namespace App\Http\Requests;

use App\Entities\User;
use App\Helpers\ApiHelpers;
use App\Helpers\FieldsHelpers;
use Illuminate\Foundation\Http\FormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class ApiRequest extends FormRequest
{

    /**
     * Each resource request should define this property
     * @var $resource string
     */
    protected $resource;

    /**
     * This method is used for handling the parameters validation errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function forbiddenResponse()
    {
        return ApiHelpers::ApiResponse(403, null);
    }

    /**
     * This method is used for handling the parameters validation errors
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(array $errors)
    {
        return ApiHelpers::ApiResponse(409, ['errors' => $errors]);
    }

    /**
     * Wrapper for the 'only' method, but excluding the null values
     * @param array $keys
     * @return array
     */
    public function onlyWith(array $keys)
    {
        $inputs = $this->only($keys);
        return $this->removeNullValues($inputs);
    }

    /**
     *
     * @param array $inputs
     * @return array
     */
    private function removeNullValues(array $inputs)
    {
        foreach ($inputs as $key => $value)
        {
            if (is_array($value))
            {
                $inputs[$key] = $this->removeNullValues($inputs[$key]);
            }

            if (empty($inputs[$key]) && !is_string($inputs[$key]))
            {
                unset($inputs[$key]);
            }
        }

        return $inputs;
    }

    /**
     * Basic rules for resource index request
     * @return array
     */
    protected function indexRules()
    {
        return [
            'updated_time'       => 'sometimes|date|date_format:Y-m-d H:i:s',
            'pagination_size'    => 'sometimes|integer|max:500',
            'sort'               => 'sometimes|in:desc,asc',
            'criteria'           => 'sometimes|array',
            'criteria.*.field'   => 'required_with:criteria',
            'criteria.*.operator'=>
                'required_with:criteria,in:equals,not_equals,empty,not_empty,contains,not_contains,starts_with,ends_with,greater_than,less_than,date_equals',
        ];
    }

    /**
     * @param $resource_class
     * @param $record_id
     * @return mixed
     */
    protected function findRecordInRequest($resource_class, $record_id)
    {
        return call_user_func_array([$resource_class, "findOrFail"], [$record_id]);
    }

}