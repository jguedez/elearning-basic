<?php

namespace App\Http\Requests\Files;

use App\Http\Requests\ApiRequest;

class UploadFileRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'folder_name'   => 'sometimes',
            'file_path'     => 'required',
            'type_resource' => 'required|in:img,pdf,doc,pptx'
        ];
    }
}
