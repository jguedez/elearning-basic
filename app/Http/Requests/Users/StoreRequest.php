<?php

namespace App\Http\Requests\Users;

use App\Entities\User;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(User::class)->store($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'name'            => 'required|max:255',
            'lasName'         => 'required|max:255',
            'email'           => 'required|email|max:255|unique:users',
            'contactId'       => 'sometimes',
            'vendorId'        => 'sometimes',
            'level'           => 'required',
            'password'        => 'required|max:60|min:6'

        ];

        return $rules;
    }
}
