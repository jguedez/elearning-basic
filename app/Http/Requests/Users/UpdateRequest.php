<?php

namespace App\Http\Requests\Users;

use App\Entities\User;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(User::class)->update($level);

    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $user_id = $this->route()->getParameter('user');

        $rules = [
            'name'       => 'sometimes|max:255',
            'lastName'   => 'sometimes|max:255',
            'email'      => 'sometimes|email|max:255',
            'contactId'  => 'sometimes',
            'vendorId'   => 'sometimes',
            'level'      => 'sometimes',
            'password'   => 'sometimes|max:60|min:6',
            'status'     => 'sometimes|boolean'
        ];
        return $rules;
    }



}
