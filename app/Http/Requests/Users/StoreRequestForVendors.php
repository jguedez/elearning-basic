<?php

namespace App\Http\Requests\Users;

use App\Entities\User;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequestForVendors extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'       => 'sometimes|max:255',
            'lastName'   => 'sometimes|max:255',
            'email'      => 'sometimes|email|max:255|unique:users,email,'. Auth::user()->id,
            'contactId'  => 'sometimes',
            'vendorId'   => 'sometimes',
            'level'      => 'required',
            'password'   => 'sometimes|max:60|min:6'
        ];

        return $rules;
    }
}
