<?php

namespace App\Http\Requests\Advances;

use App\Entities\Advances;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Advances::class)->store($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'advanceName'       => 'required|nullable|max:32000',
            'name'              => 'required|nullable|max:32000',
            'owner'             => 'required|nullable|max:32000',
            'createdBy'         => 'sometimes|nullable|max:32000',
            'modifiedBy'        => 'sometimes|nullable|max:32000',
            'createdTime'       => 'sometimes|nullable|max:32000',
            'modifiedTime'      => 'sometimes|nullable|max:32000',
            'lessonCourseId'    => 'required|nullable|max:32000',
            'lessonCourse'      => 'sometimes|nullable|max:32000',
            'courseId'          => 'required|nullable|max:32000',
            'course'            => 'sometimes|nullable|max:32000',
            'moduleCourseId'    => 'required|nullable|max:32000',
            'moduleCourse'      => 'sometimes|nullable|max:32000',
            'secondId'          => 'sometimes|nullable|max:32000',
            'contactId'         => 'sometimes|nullable|max:32000',
            'contact'           => 'sometimes|nullable|max:32000',

        ];

        return $rules;
    }
}