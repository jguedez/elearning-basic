<?php

namespace App\Http\Requests\Advances;

use App\Entities\Advances;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Advances::class)->update($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id'                => 'sometimes|max:32000',
            'advanceName'       => 'sometimes|nullable|max:32000',
            'name'              => 'sometimes|nullable|max:32000',
            'owner'             => 'sometimes|nullable|max:32000',
            'createdBy'         => 'sometimes|nullable|max:32000',
            'modifiedBy'        => 'sometimes|nullable|max:32000',
            'createdTime'       => 'sometimes|nullable|max:32000',
            'modifiedTime'      => 'sometimes|nullable|max:32000',
            'lessonCourseId'    => 'sometimes|nullable|max:32000',
            'lessonCourse'      => 'sometimes|nullable|max:32000',
            'courseId'          => 'sometimes|nullable|max:32000',
            'course'            => 'sometimes|nullable|max:32000',
            'moduleCourseId'    => 'sometimes|nullable|max:32000',
            'moduleCourse'      => 'sometimes|nullable|max:32000',
            'secondId'          => 'sometimes|nullable|max:32000',
            'contactId'         => 'sometimes|nullable|max:32000',
            'contact'           => 'sometimes|nullable|max:32000',
        ];

        return $rules;
    }
}