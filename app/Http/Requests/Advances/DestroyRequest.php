<?php

namespace App\Http\Requests\Advances;

use App\Entities\Advances;
use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class DestroyRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        $level = Auth::user()->level;
        return policy(Advances::class)->delete($level);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}