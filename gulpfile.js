const elixir = require('laravel-elixir');

elixir.config.assetsPath = 'public';
elixir.config.css.folder = '';
elixir.config.js.folder = '';


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


