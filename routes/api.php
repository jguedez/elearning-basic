<?php

use App\Entities\Cases;
use App\Helpers\ApiHelpers;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('event-live', 'LiveControlller', ['except' => ['create', 'edit']]);
//Auth

Route::get('testing', 'EventsController@test');

//Vimeo

Route::get('getVideo','VimeoController@getVideo');
Route::get('getLessonsWithoutDuration','VimeoController@getLessonsWithoutDuration');


Route::post('auth/login', 'AuthController@login');
Route::post('auth/register', 'AuthController@register');

//Route::get('auth/getUser', 'AuthController@getUser');

Route::get('advanceUser/{CourseId}/{contactId}' ,'AdvancesController@CountTotalAuth');
Route::get('progressUser/{CourseId}/{advancetotal}' ,'AdvancesController@AdvancesProgress');


//Products
Route::get('getProducts','ProductsController@alternativesProducts');
Route::resource('products', 'ProductsController', ['except' => ['create', 'edit']]);

//Passwords
Route::post('forget-password','UsersController@forget');
Route::get('forget-password-token/{token}','UsersController@forgetWithToken');

Route::resource('salesOrders', 'SalesOrdersController', ['except' => ['create', 'edit']]);

Route::group(['middleware' => "VerifyAuthToken"], function() {

    //Calendario

    Route::resource('calendar','CalendarController');
    Route::get('instructors','VendorsController@InstructorsList');
    Route::get('instructors/{id}','VendorsController@show');
    Route::get('instructorsClasses','LiveControlller@nextInstructorClasses');

    //stripe
    Route::post('stripe', 'StripeController@payment');
    Route::post('paypal', 'PaypalController@PaypalPay');
    Route::post('execute/payment', 'PaypalController@getPaymentStatus');

    //File microservice
    Route::post('folder', 'FilesController@Folders');
    Route::post('file/upload', 'FilesController@UploadFiles');
    Route::post('file/delete', 'FilesController@DeleteFile');
    Route::post('video/upload', 'VimeoController@VimeoFile');
    Route::post('video/url', 'VimeoController@UriVimeo');
    
    //Cases
    Route::resource('cases', 'CasesController', ['except' => ['create', 'edit']]);
    Route::get('getLastTest','CasesController@getLastTest');
    //Purchase Orders
    Route::get('GetPurchasesOrdersAuth','PurchaseOrdersController@GetPushesOrdersAuth');
    //Products Auth
    Route::get('getProductsAuth','ProductsController@getProductsAuth');
    Route::get('getProductAuthById/{product}','ProductsController@getProductAuthById');
    Route::get('gettingParameters','ProductsController@gettingParameters');

    //Sales Orders
    Route::get('getSalesOrders','SalesOrdersController@getSalesOrders');
    Route::get('Mycourses','SalesOrdersController@MyCoursesAuth');
    Route::get('getSalesOrdersAuth','SalesOrdersController@getSalesOrdersAuth');

    Route::get('incomeForTotal/{value?}', 'SalesOrdersController@incomeForTotal');
    Route::get('incomeForPercent/{value?}', 'SalesOrdersController@incomeForPercent');
    Route::get('gettingSaleParameters','SalesOrdersController@gettingSalesParameters');

    Route::resource('advances', 'AdvancesController', ['except' => ['create', 'edit']]);
    Route::get('advancesWithParemeter/{course}','AdvancesController@indexWithParameter');
    Route::get('indexAuth','AdvancesController@indexAuth');
    Route::get('lastlesson','AdvancesController@GetLastLesson');

    //Update for Zoho
    Route::post('updateZoho', 'UsersController@updateZoho');

    //Like Instructor
    Route::get('becomeAnInstructor', 'UsersController@become');

    //Delete Account
    Route::get('deleteAccount/{contactId}/{vendorId}','UsersController@deleteAccount');

    //Reset Password
    Route::post('reset-password','UsersController@postReset');
    
    //Change Email for Zoho
    Route::get('changeEmail', 'UsersController@changeEmail');

    //Zoho Modules
    Route::resource('contacts', 'ContactsController', ['except' => ['create', 'edit']]);
    Route::resource('leads','LeadsController', ['except' => ['create', 'edit']]);
    Route::resource('modules', 'ModulesController', ['except' => ['create', 'edit']]);
    Route::resource('lessons', 'LessonsController');
    Route::post('reorder', 'LessonsController@CheckOrder');

    Route::resource('registrations', 'RegistrationController', ['except' => ['create', 'edit']]);
    Route::resource('vendors', 'VendorsController', ['except' => ['create', 'edit']]);
    Route::get('test/{id}', 'VendorsController@test');
    Route::get('gettingDashboard', 'RegistrationController@GetInscriptions');

    Route::group(['middleware' => 'admin'], function () {
        Route::get('course-status', 'ProductsController@ListToCourse');
        Route::get('search', 'ProductsController@searchCourse');
        Route::get('details', 'VendorsController@AdvancesByStudent');
        Route::get('users', 'UsersController@index');
        Route::post('user/new', 'UsersController@CreateUsers');
        Route::delete('user/delete/{id}', 'UsersController@DeleteUser');
        Route::put('user/update/{id}', 'UsersController@UpdateUser');
        Route::get('no-enrolled/{userid}', 'RegistrationController@CourseNotEnrolledByUser');
        Route::get('course/enrolled/{courseid}', 'RegistrationController@CourseEnrolledByCourse');
        Route::put('registrations/{id}', 'RegistrationController@update');
        Route::get('enrolled/{userid}', 'RegistrationController@CourseEnrolledByUser');
        Route::post('inscription', 'RegistrationController@InscriptionCourse');
    });

    //Product Auth
    Route::get('getCourseAuthById/{product}','ProductsController@show_product_auth');

    Route::resource('purchaseOrders', 'PurchaseOrdersController', ['except' => ['create', 'edit']]);
});


Route::post('refresh', 'AuthController@refresh');


Route::get('fromZoho/{module}', function($module = 'Products')
{
    $array   = [];
    $zoho    = new \CristianPontes\ZohoCRMClient\ZohoCRMClient($module,'fe4d0ce38c31c7d47eee02e4b9731b20');
    $records = $zoho->getRecords()->request();

    foreach ($records as $record)
    {
       $array[] = $record;
    }

    return response()->json(['response' => $array]);

});

// Zoho Sync//
Route::get('zoho/sync/{service}', function ($service) {

    $zohoSync = new \App\Zoho\ZohoSync($service);
    $zohoSync->run();

    return \App\Helpers\ApiHelpers::ApiResponse(200, true);

})->where(['service' => 'Cases|Products|Contacts|Leads|CustomModule2|CustomModule3|CustomModule4|Vendors|SalesOrders|PurchaseOrders|CustomModule1']);


// Zoho Delete
Route::post('zoho/delete/{service}/', function ($service, \Illuminate\Http\Request $request) {

    $zohoSync = new \App\Zoho\ZohoSync($service);
    $zohoSync->delete($request->input('id'));

    //return $request->input('id');
    return \App\Helpers\ApiHelpers::ApiResponse(200, true);

})->where(['service' => 'Products|Contacts|Leads|CustomModule2|CustomModule3|CustomModule4|Vendors|SalesOrders|PurchaseOrders|CustomModule1']);

