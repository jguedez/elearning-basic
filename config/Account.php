<?php

$env = env('APP_ENV') ? env('APP_ENV') : 'production';
$debug = $env != 'production';

try {
    $dockerSecrets = new DockerSecrets\Reader\SecretsReader('/run/secrets');
    
    $key_secret = $dockerSecrets->read('KEY_ACCOUNT');
    $subscription = $dockerSecrets->read('SUBSCRIPTION_ID');

} catch (DockerSecrets\Exception\SecretDirNotExistException $e) {

    if ($env == 'production')
    {
        throw $e;

    }else{
        $key_secret = getenv('KEY_ACCOUNT') ? getenv('KEY_ACCOUNT') : 'ba7eed20-4f4c-11e9-84bd-ff0c642af743';
        $subscription = getenv('SUBSCRIPTION_ID') ? getenv('SUBSCRIPTION_ID') : 'ySt4CgYolpMsSNbkwQwu';
    }
}

return [

    /*
    |--------------------------------------------------------------------------
    | Key security Account Microservice 
    |--------------------------------------------------------------------------
    |
    */

    'secret_key' => $key_secret,
    'id_subscription' => $subscription
];