<?php 

use DockerSecrets\Reader\SecretsReader;
use DockerSecrets\Exception\SecretDirNotExistException;

$env = getenv('APP_ENV') ? getenv('APP_ENV') : 'production';

try {
    $dockerSecrets = new DockerSecrets\Reader\SecretsReader('/run/secrets');
    
    $client = $dockerSecrets->read('PAYPAL_CLIENT_ID');
    $secret = $dockerSecrets->read('PAYPAL_SECRET');
    $mode = $dockerSecrets->read('PAYPAL_MODE');

} catch (DockerSecrets\Exception\SecretDirNotExistException $e) {

    if ($env == 'production')
    {
        throw $e;
    }else{
        
        $client = getenv('PAYPAL_CLIENT_ID') ? getenv('PAYPAL_CLIENT_ID') : 'Ac8AynzvX8rHwIVzR4G3WypomeB812Sfw1jna4uJSai2HGOQpne5pMFuou58sFi9dKe4H7n-UiZBMM9Y';
        $secret = getenv('PAYPAL_SECRET') ? getenv('PAYPAL_SECRET') : 'EO9_-4NdxCpwkbdeHHp5nXZ7N8EvFSLUq1aGV-gATTrVcwx6UQRwJyADturTsRBHGijddOPUYQtAYKpm';;
        $mode = getenv('PAYPAL_MODE') ? getenv('PAYPAL_MODE') : 'sandbox';
    }
}

return [ 
    'client_id' => $client,
    'secret' => $secret,
    'settings' => array(
        'mode' => $mode,
        'http.ConnectionTimeOut' => 50,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
    ),
];