<?php

$env = getenv('APP_ENV') ? getenv('APP_ENV') : 'production';

try {
    $dockerSecrets = new DockerSecrets\Reader\SecretsReader('/run/secrets');
    
    $host = $dockerSecrets->read('DB_HOST');
    $port = $dockerSecrets->read('DB_PORT');
    $database = $dockerSecrets->read('DB_DATABASE');
    $username = $dockerSecrets->read('DB_USERNAME');
    $password = $dockerSecrets->read('DB_PASSWORD');

} catch (DockerSecrets\Exception\SecretDirNotExistException $e) {

    if ($env == 'production')
    {
        throw $e;
    }else{
        $host = getenv('DB_HOST') ? getenv('DB_HOST') : 'database';
        $port = getenv('DB_PORT') ? getenv('DB_PORT') : '3306';;
        $database = getenv('DB_DATABASE') ? getenv('DB_DATABASE') : 'homestead';
        $username = getenv('DB_USERNAME') ? getenv('DB_USERNAME') : 'root';
        $password = getenv('DB_PASSWORD') ? getenv('DB_PASSWORD') : 'homestead';
    }
}

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_OBJ,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => 'mysql',

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => getenv('DB_DATABASE') ? getenv('DB_DATABASE') : database_path('database.sqlite'),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => $host,
            'port' => $port,
            'database' => $database,
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        
        'pgsql' => [
            'driver' => 'pgsql',
            'host' => getenv('DB_HOST', 'localhost'),
            'port' => getenv('DB_PORT', '5432'),
            'database' => getenv('DB_DATABASE', 'forge'),
            'username' => getenv('DB_USERNAME', 'forge'),
            'password' => getenv('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host' => getenv('REDIS_HOST', 'localhost'),
            'password' => getenv('REDIS_PASSWORD', null),
            'port' => getenv('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
